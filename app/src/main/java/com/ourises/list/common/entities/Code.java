package com.ourises.list.common.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Code {
    OTHER(0),
    USER_NOT_FOUND(403);

    private final int id;

    Code(int id) {
        this.id = id;
    }

    @JsonCreator
    public static Code fromId(int id) {
        for (Code templateId: Code.values()) {
            if (templateId.getId() == id) {
                return templateId;
            }
        }
        return OTHER;
    }

    @JsonValue
    public int getId() {
        return this.id;
    }
}
