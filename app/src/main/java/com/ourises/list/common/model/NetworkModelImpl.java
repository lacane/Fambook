package com.ourises.list.common.model;

import android.util.Log;

import com.ourises.list.common.entities.ErrorModel;
import com.ourises.list.common.model.socket.SocketModel;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.observables.ConnectableObservable;
import rx.schedulers.Schedulers;
import rx.subjects.ReplaySubject;

public class NetworkModelImpl implements NetworkModel {
    private final SocketModel socketModel;
    private final StorageModel storageModel;

    private boolean offlineMode;
    private Subscriber<? super Boolean> subscriberOfflineMode;

    private Subscription timeoutSubscription;
    private Subscription connectErrorSubscription;
    private Subscription disconnectSubscription;
    private Subscription connectSubscription;

    private boolean connected = false;

    private ConnectableObservable<Boolean> offlineModeObservable;

    public NetworkModelImpl(SocketModel socketModel, StorageModel storageModel) {
        this.socketModel = socketModel;
        this.storageModel = storageModel;

        setOfflineMode(true);
    }

    @Override
    public void connect() {
        connected = true;
        socketModel.connect();

        timeoutSubscription = socketModel.onConnectTimeoutSubscribe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    setOfflineMode(true);
                    Log.e("NetworkModel", "onConnectTimeoutSubscribe " + o.toString());
                });

        connectErrorSubscription = socketModel.onConnectErrorSubscribe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    setOfflineMode(true);
                    Log.e("NetworkModel", "onConnectErrorSubscribe " + o.toString());
                });

        disconnectSubscription = socketModel.onDisconnectSubscribe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    setOfflineMode(true);
                    Log.d("NetworkModel", "onDisconnectSubscribe " + o.toString());
                });

        connectSubscription = socketModel.onConnectSubscribe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    setOfflineMode(false);
                    Log.d("NetworkModel", "onConnectSubscribe " + o.toString());
                });
    }

    @Override
    public void disconnect() {
        socketModel.disconnect();

        if (connectSubscription != null) {
            connectSubscription.unsubscribe();
        }
        if (disconnectSubscription != null) {
            disconnectSubscription.unsubscribe();
        }
        if (connectErrorSubscription != null) {
            connectErrorSubscription.unsubscribe();
        }
        if (timeoutSubscription != null) {
            timeoutSubscription.unsubscribe();
        }
    }

    private void setOfflineMode(boolean offlineMode) {
        this.offlineMode = offlineMode;
        sendOfflineStatus();
    }

    private void sendOfflineStatus() {
        if (subscriberOfflineMode != null) {
            subscriberOfflineMode.onNext(offlineMode);
        }
    }

    @Override
    public Observable<Boolean> onOfflineModeSubscribe() {
        return getObservable();
    }

    private Observable<Boolean> getObservable() {
        if (offlineModeObservable == null) {
            offlineModeObservable = ReplaySubject.create(new Observable.OnSubscribe<Boolean>() {
                @Override
                public void call(Subscriber<? super Boolean> subscriber) {
                    subscriberOfflineMode = subscriber;
                    subscriberOfflineMode.onNext(offlineMode);
                }
            }).publish();

            offlineModeObservable.connect();
        }

        return offlineModeObservable;
    }

    @Override
    public boolean checkForNetwork() {
        boolean result = connected;
        if (!connected) {
            connect();
        }

        return result;
    }

    @Override
    public Observable<Boolean> isOfflineMode() {
        return Observable.just(offlineMode);
    }

    @Override
    public Observable<ErrorModel> onErrorModelSubscribe() {
        return socketModel.onErrorModelSubscribe();
    }
}
