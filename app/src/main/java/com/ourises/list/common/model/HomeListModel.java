package com.ourises.list.common.model;


import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.entities.ResponseModelId;
import com.ourises.list.navigation.changes.model.ChangesModel;
import com.ourises.list.navigation.homeDescription.model.SynchronizedListModel;
import com.ourises.list.navigation.homeDescription.model.UpdateAction;

import java.util.List;

import rx.Observable;

public interface HomeListModel {
    void setListModels(List<ListModel> listModels);

    Observable<List<ListModel>> getListModels();

    Observable<ListModel> synchronizeListModel(ListModel listModel);

    Observable<ListModel> getListModel(String itemId);

    Observable<ListModel> onUpdateModelSubscribe();

    Observable<ResponseModelId> onDeleteModelSubscribe();

    Observable<Object> removeListModel(ListModel listModel);

    Observable<Object> updateListModel(ListModel listModel);

    ListModel addListModel(ListModel listModel);

    Observable<Object> removeListItemModel(ListItemModel listItemModel);

    void updateListItemsInStorage(String parentId, List<ListItemModel> listItemModels);

    void updateListModelInStorage(ListModel listModel);

    Observable<ListItemModel> synchronizeListItemModel(ListItemModel itemModel);

    Observable<Object> updateListItemModel(ListItemModel itemModel);

    Observable<List<ListModel>> getListModelsFromStorage();

    Observable<Object> shareToFriend(String userId, String listId);

    Observable<Boolean> synchronizeListModels();

    Observable<SynchronizedListModel> onSynchronizedListModelSubscribe();

    Observable<String> onUpdateStorageListModelSubscribe();

    Observable<UpdateAction> onUpdateActionSubscribe();

    void setHistoryMode(boolean historyMode);

    Observable<List<ChangesModel>> getChanges(String itemId);
}
