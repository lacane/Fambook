package com.ourises.list.common.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.ourises.list.common.model.HomeListModel;
import com.ourises.list.common.model.NetworkModel;
import com.ourises.list.common.model.UserModel;
import com.ourises.list.di.HasComponent;
import com.ourises.list.di.components.ApplicationComponent;

import javax.inject.Inject;

import rx.Subscription;

public class SynchronizingService extends Service {

    @Inject
    NetworkModel networkModel;

    @Inject
    UserModel userModel;

    @Inject
    HomeListModel homeListModel;

    private ApplicationComponent component;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        if (component == null) {
            component = ((HasComponent<ApplicationComponent>) getApplication()).getComponent();
        }
        component.inject(this);
    }

    private Subscription isOfflineModeSubscription;
    private Subscription onOfflineModeSubscribeSubscription;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        isOfflineModeSubscription = networkModel.isOfflineMode()
                .subscribe(offlineMode -> {
                    if (!offlineMode) {
                        disconnect();
                    }
                });

        onOfflineModeSubscribeSubscription = networkModel.onOfflineModeSubscribe()
                .subscribe(offlineMode -> {
                    if (!offlineMode) {
                        synchronize();
                    }
                });

        return START_STICKY;
    }

    private void synchronize() {
        userModel.getTokenSession()
                .flatMap(token -> userModel.login(token))
                .flatMap(offlineMode -> homeListModel.synchronizeListModels())
                .flatMap(offlineMode -> homeListModel.getListModels())
                .subscribe(listModels -> {
                    homeListModel.setListModels(listModels);
                    disconnect();
                }, throwable -> disconnect());

    }

    private void disconnect() {
        if (isOfflineModeSubscription != null) {
            isOfflineModeSubscription.unsubscribe();
        }
        if (onOfflineModeSubscribeSubscription != null) {
            onOfflineModeSubscribeSubscription.unsubscribe();
        }
        networkModel.disconnect();
        stopSelf();
    }

    @Override
    public void onDestroy() {
        isOfflineModeSubscription.unsubscribe();
        onOfflineModeSubscribeSubscription.unsubscribe();
        super.onDestroy();
    }
}
