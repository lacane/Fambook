package com.ourises.list.common.model;

import com.google.firebase.iid.FirebaseInstanceId;
import com.ourises.list.common.entities.User;
import com.ourises.list.common.model.socket.SocketModel;
import com.ourises.list.navigation.auth.model.AuthTokenModel;
import com.ourises.list.navigation.profile.model.ProfileStatus;

import rx.Observable;
import rx.Subscriber;
import rx.observables.ConnectableObservable;
import rx.subjects.ReplaySubject;

public class UserModelImpl implements UserModel {
    private final StorageModel storageModel;
    private final SocketModel socketModel;
    private final NetworkModel networkModel;
    private User user;

    private Subscriber<? super User> userSubscriber;
    private ConnectableObservable<User> userObservable;

    public UserModelImpl(NetworkModel networkModel, SocketModel socketModel, StorageModel storageModel) {
        this.storageModel = storageModel;
        this.socketModel = socketModel;
        this.networkModel = networkModel;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
        storageModel.putUser(user);
        sendUserStatus();
    }

    private void sendUserStatus() {
        if (userSubscriber != null) {
            userSubscriber.onNext(user);
        }
    }

    @Override
    public Observable<User> getUserReadyObservable() {
        if (userObservable == null) {
            userObservable = ReplaySubject.create(new Observable.OnSubscribe<User>() {
                @Override
                public void call(Subscriber<? super User> subscriber) {
                    userSubscriber = subscriber;
                }
            }).publish();

            userObservable.connect();
        }

        return userObservable;
    }

    @Override
    public Observable<User> getUser() {
        if (user != null) {
            return Observable.just(user);
        } else {
            return storageModel.getUser();
        }
    }

    @Override
    public Observable<String> getTokenSession() {
        return storageModel.getTokenSession();
    }

    @Override
    public Observable<User> login(String token) {
        String fcmToken = FirebaseInstanceId.getInstance().getToken();
        networkModel.checkForNetwork();
        return socketModel.login(AuthTokenModel.create(token, fcmToken)).doOnNext(user1 -> this.user = user1);
    }

    @Override
    public Observable<User> onAuthSubscribe() {
        return socketModel.onAuthSubscribe();
    }

    @Override
    public Observable<Object> logout() {
        return socketModel.logout();
    }

    @Override
    public Observable<ProfileStatus> getListStatus() {
        return socketModel.getProfileStatus();
    }
}
