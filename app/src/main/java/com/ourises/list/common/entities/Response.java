package com.ourises.list.common.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Response {
    @JsonProperty("status")
    Integer code;

    @JsonProperty("message")
    String details;

    @JsonProperty("successfully")
    Boolean successfully;

    public Integer getCode() {
        return code;
    }
    public String getDetails() {
        return details;
    }
    public Boolean getSuccessfully() {
        return successfully;
    }
}
