package com.ourises.list.common.model;

import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.entities.User;

import java.util.List;

import rx.Observable;

public interface StorageModel {
    Observable<Boolean> putTokenSession(String token);

    Observable<String> getTokenSession();

    Observable<User> getUser();

    Observable<Boolean> putUser(User user);

    void putListModels(List<ListModel> listModels, boolean historyMode);

    Observable<List<ListModel>> getListModels(boolean historyMode);

    Observable<Boolean> getNotificationsEnableState();

    void updateNotificationState(boolean state);

    Observable<Object> logout();

    void putListModelForUpdating(ListModel listModel);

    Observable<List<ListModel>> getListModelsForUpdating();

    void removeItemsForSynchronizing();

    void putListModelForDeleting(ListModel listModel);

    Observable<List<ListModel>> getListModelsForDeleting();

    void putListItemModelForUpdating(ListItemModel itemModel);

    Observable<List<ListItemModel>> getListItemModelForUpdating();

    void putListItemModelForDeleting(ListItemModel itemModel);

    Observable<List<ListItemModel>> getListItemModelForDeleting();

    void putListItemModelForCreating(ListItemModel itemModel);

    Observable<List<ListItemModel>> getListItemModelForCreating();
}
