package com.ourises.list.common.entities;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;

import java.io.Serializable;
import java.util.List;

@AutoValue
@JsonDeserialize(builder = AutoValue_User.Builder.class)
public abstract class User implements Serializable {

    public static Builder builder() {
        return new AutoValue_User.Builder();
    }

    @JsonProperty("id")
    public abstract String id();

    @JsonProperty("name")
    public abstract String name();

    @Nullable
    @JsonProperty("photo")
    public abstract String photo();

    @Nullable
    @JsonProperty("friends")
    public abstract List<User> friends();

    @AutoValue.Builder
    public abstract static class Builder {
        @JsonProperty("id")
        public abstract Builder id(String id);

        @JsonProperty("name")
        public abstract Builder name(String name);

        @Nullable
        @JsonProperty("photo")
        public abstract Builder photo(String photo);

        @Nullable
        @JsonProperty("friends")
        public abstract Builder friends(List<User> friends);

        public abstract User build();
    }
}
