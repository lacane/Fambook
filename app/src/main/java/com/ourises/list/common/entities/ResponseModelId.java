package com.ourises.list.common.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_ResponseModelId.Builder.class)
public abstract class ResponseModelId {

    @JsonProperty("id")
    public abstract String id();

    @AutoValue.Builder
    public abstract static class Builder {

        @JsonProperty("id")
        public abstract Builder id(String id);

        public abstract ResponseModelId build();
    }
}
