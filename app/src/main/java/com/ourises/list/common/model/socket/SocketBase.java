package com.ourises.list.common.model.socket;

import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ourises.list.common.entities.ErrorModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.socket.client.Socket;
import rx.Observable;
import rx.Subscriber;
import rx.observables.ConnectableObservable;
import rx.subjects.ReplaySubject;

class SocketBase {
    Socket socket;
    private ObjectMapper mapper;

    private ConnectableObservable<ErrorModel> errorModelObservable;
    private Subscriber<? super ErrorModel> errorModelSubscriber;

    protected void setSocket(Socket socket, ObjectMapper mapper) {
        this.socket = socket;
        this.mapper = mapper;
    }

    <T> Observable<T> onObservable(String event, Class<T> tClass) {
        return ReplaySubject.create(subscriber -> socket.on(event, args -> {
            print(event + " event", args);
            T model = getModel(args.clone()[0], tClass);
            try {
                subscriber.onNext(model);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }));
    }

    <T> Observable<List<T>> onObservableList(String event, Class<T> tClass) {
        return ReplaySubject.create(subscriber -> socket.on(event, args -> {
            print(event + " event", args);
            try {
                List<T> items = getModels(tClass, args);
                subscriber.onNext(items);
            } catch (JSONException e) {
                subscriber.onError(e);
            }
        }));
    }

    Observable<Object> onObservable(String event) {
        return ReplaySubject.create(subscriber -> {
            socket.on(event, args -> {
                print(event + " event", args);
                subscriber.onNext(args);
            });
        });
    }

    void offSocket(String event) {
        socket.off(event);
    }

    <T> Observable<T> send(String event, Object o, Class<T> tClass) {
        return Observable.create(subscriber -> send(event, o, subscriber, tClass));
    }

    <T> void send(String event, Object o, Subscriber<? super T> subscriber, Class<T> tClass) {
        try {
            socket.emit(event, new Object[]{mapper.writeValueAsString(o)}, args -> {
                print(event + " callback", args);
                if (args.clone().length > 0) {
                    T model = getModel(args.clone()[0], tClass);
                    subscriber.onNext(model);
                }
                subscriber.onCompleted();
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            subscriber.onError(e);
            subscriber.onCompleted();
        }
    }

    <T> Observable<List<T>> sendList(String event, Class<T> tClass) {
        return sendList(event, null, tClass);
    }

    <T> Observable<List<T>> sendList(String event, Object o, Class<T> tClass) {
        return Observable.create(subscriber -> sendList(event, o, subscriber, tClass));
    }

    <T> void sendList(String event, Object o, Subscriber<? super List<T>> subscriber, Class<T> tClass) {
        try {
            Object model = o != null ? mapper.writeValueAsString(o) : null;
            socket.emit(event, new Object[]{model}, args -> {
                print(event + " callback", args);
                try {
                    List<T> items = getModels(tClass, args);
                    subscriber.onNext(items);
                    subscriber.onCompleted();
                } catch (JSONException e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                    subscriber.onCompleted();
                }
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            subscriber.onError(e);
            subscriber.onCompleted();
        }
    }

    private <T> List<T> getModels(Class<T> tClass, Object... object) throws JSONException {
        List<T> items = new ArrayList<T>();
        String objects = (String) object.clone()[0];
        checkError(objects);
        JSONArray array = new JSONArray(objects);
        for (int index = 0; index < array.length(); index++) {
            T model = getModel(array.getJSONObject(index), tClass);
            items.add(model);
        }

        return items;
    }

    private <T> T getModel(Object object, Class<T> tClass) {
        if (object == null) return null;
        try {
            checkError(object);
            return mapper.readValue(object.toString(), tClass);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void print(String event, Object... args) {
        if (args == null) {
            Log.d("SocketModel", event);
        } else {
            Log.d("SocketModel", event + " " + Arrays.toString(args));
        }
    }

    private void checkError(Object object) {
        if (object == null) return;

        try {
            ErrorModel errorModel = mapper.readValue(object.toString(), ErrorModel.class);
            if (errorModel != null && errorModelSubscriber != null) {
                errorModelSubscriber.onNext(errorModel);
            }
        } catch (IOException ignored) {
        }
    }

    protected Observable<ErrorModel> errorModelObservable() {
        if (errorModelObservable == null) {
            errorModelObservable = ReplaySubject.create((Observable.OnSubscribe<ErrorModel>)
                    subscriber -> errorModelSubscriber = subscriber).publish();

            errorModelObservable.connect();
        }

        return errorModelObservable;
    }
}
