package com.ourises.list.common.model;

import android.content.Context;
import android.os.Build;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import rx.Observable;

public class SettingsModelImpl implements SettingsModel {
    private final StorageModel storageModel;
    private final UserModel userModel;

    public SettingsModelImpl(StorageModel storageModel, UserModel userModel) {
        this.storageModel = storageModel;
        this.userModel = userModel;
    }

    @Override
    public Observable<Boolean> getNotificationsEnableState() {
        return storageModel.getNotificationsEnableState();
    }

    @Override
    public void updateNotificationState(boolean state) {
        storageModel.updateNotificationState(state);
    }

    @Override
    public Observable<Object> logout(Context context) {
        userModel.logout();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncManager= CookieSyncManager.createInstance(context);
            cookieSyncManager.startSync();
            CookieManager cookieManager= CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncManager.stopSync();
            cookieSyncManager.sync();
        }
        return storageModel.logout();
    }
}
