package com.ourises.list.common.widget.util;


import android.animation.Animator;
import android.animation.ValueAnimator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.RelativeLayout;

public class AnimUtils {
    public static final int DURATION = 400;

    public static void showFade(View view) {
        if (view != null && view.getVisibility() != View.VISIBLE) {
            AlphaAnimation anim = new AlphaAnimation(0, 1);
            anim.setDuration(DURATION);
            view.startAnimation(anim);
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void goneFade(View view) {
        if (view != null && view.getVisibility() != View.GONE) {
            AlphaAnimation anim = new AlphaAnimation(1, 0);
            anim.setDuration(DURATION);
            view.startAnimation(anim);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(View.GONE);
                }
            });
        }
    }

    public static void hideFade(View view) {
        if (view != null && view.getVisibility() != View.INVISIBLE) {
            AlphaAnimation anim = new AlphaAnimation(1, 0);
            anim.setDuration(DURATION);
            view.startAnimation(anim);
            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(View.INVISIBLE);
                }
            });
        }
    }

    public static void goneBottomSlide(View view) {
        view.animate()
                .setDuration(DURATION)
                .setInterpolator(new LinearOutSlowInInterpolator())
                .translationY(view.getMeasuredHeight())
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .start();
    }

    public static void showBottomSlide(View view) {
        view.setVisibility(View.VISIBLE);
        view.animate()
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .setDuration(DURATION)
                .setInterpolator(new LinearOutSlowInInterpolator())
                .translationY(0)
                .start();
    }

    public static void goneRightSlide(View view) {
        view.animate()
                .setDuration(DURATION)
                .setInterpolator(new LinearOutSlowInInterpolator())
                .translationX(view.getMeasuredWidth())
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .start();
    }

    public static void showRightSlide(View view) {
        view.setVisibility(View.VISIBLE);
        view.animate()
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .setDuration(DURATION)
                .setInterpolator(new LinearOutSlowInInterpolator())
                .translationX(0)
                .start();
    }

    public static void changeMarginRight(int marginRight, View view) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
        ValueAnimator animator = ValueAnimator.ofInt(params.rightMargin, marginRight);
        animator.addUpdateListener(valueAnimator -> {
            params.rightMargin = (Integer) valueAnimator.getAnimatedValue();
            view.setLayoutParams(params);
        });
        animator.setDuration(DURATION);
        animator.setInterpolator(new LinearOutSlowInInterpolator());
        animator.start();

    }
}
