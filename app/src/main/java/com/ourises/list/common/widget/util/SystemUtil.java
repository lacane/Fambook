package com.ourises.list.common.widget.util;


import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SystemUtil {
    public static void hideKeyboard(Context context, View currentFocusView) {
        if (currentFocusView != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(currentFocusView.getWindowToken(), 0);
        }
    }

    public static void showKeyboard(Context context, EditText editText) {
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    public static String getCurrentFullDateToString() {
        SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy 'T' HH:mm:ss.SSS", Locale.ENGLISH);
        return format.format(new Date(System.currentTimeMillis()));
    }
}
