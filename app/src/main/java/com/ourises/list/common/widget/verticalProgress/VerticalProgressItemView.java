package com.ourises.list.common.widget.verticalProgress;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.ourises.list.R;

public class VerticalProgressItemView extends TextView {

    private Paint circleFillPaint = new Paint();
    private Paint circleStrokePaint = new Paint();

    private float circleRadius;
    private Boolean completingItem;

    private int gray = ContextCompat.getColor(getContext(), R.color.gray_selected_item);

    public VerticalProgressItemView(Context context) {
        super(context);

        circleStrokePaint.setStyle(Paint.Style.STROKE);
        circleStrokePaint.setStrokeWidth(4);
        circleStrokePaint.setColor(Color.GRAY);


        circleFillPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int cy = getMeasuredHeight() / 2;

        if (completingItem) {
            circleFillPaint.setColor(ContextCompat.getColor(getContext(), R.color.color_check_box));
            canvas.drawCircle(circleRadius + 2, cy, circleRadius, circleFillPaint);
            setPaintFlags(getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            circleFillPaint.setColor(Color.WHITE);
            canvas.drawCircle(circleRadius + 2, cy, circleRadius, circleFillPaint);
            canvas.drawCircle(circleRadius + 2, cy, circleRadius, circleStrokePaint);
        }

        setPadding((int) (50 + circleRadius), 0, 0, 0);
        super.onDraw(canvas);
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldheight) {
        super.onSizeChanged(width, height, oldWidth, oldheight);
    }

    public void setCompletingItem(Boolean completingItem) {
        this.completingItem = completingItem;
    }

    public void setCircleRadius(float circleRadius) {
        this.circleRadius = circleRadius;
    }
}
