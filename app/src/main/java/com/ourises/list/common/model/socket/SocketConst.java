package com.ourises.list.common.model.socket;

public class SocketConst {
    private static String CURRENT_VERSION = "/api/v1/";
    static String LOGIN = CURRENT_VERSION + "login";
    static String CREATE_LIST = CURRENT_VERSION + "create-note";
    static String DELETE_LIST = CURRENT_VERSION + "delete-note";
    static String DELETE_LIST_EVENT = "delete-note";
    static String GET_LIST_MODELS = CURRENT_VERSION + "get-notes";
    static String SHARE_LIST = CURRENT_VERSION + "share-note";
    static String GET_ARCHIVED_LIST = CURRENT_VERSION + "get-archived-notes";
    static String ADD_ITEM = CURRENT_VERSION + "add-item";
    static String DELETE_ITEM = CURRENT_VERSION + "delete-item";
    static String UPDATE_LIST = CURRENT_VERSION + "update-note";
    static String UPDATE_LIST_EVENT = "update-note";
    static String UPDATE_ITEM = CURRENT_VERSION + "update-item";
    static String PROFILE_STATUS = CURRENT_VERSION + "get-notes-status";
    static String GET_CHANGES = CURRENT_VERSION + "get-note-changes";
}
