package com.ourises.list.common.entities;

import android.support.annotation.Nullable;

import com.alapshin.genericrecyclerview.Item;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;
import com.ourises.list.navigation.changes.model.ChangesModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@AutoValue
@JsonDeserialize(builder = AutoValue_ListModel.Builder.class)
@JsonIgnoreProperties(ignoreUnknown = true, value = {"selected", "clicked"})
public abstract class ListModel implements Item, Serializable {

    @Override
    public int type() {
        return 0;
    }

    @Override
    public int id() {
        return 0;
    }

    @Nullable
    @JsonProperty("name")
    public abstract String name();

    @Nullable
    @JsonProperty("items")
    public abstract List<ListItemModel> items();

    @Nullable
    @JsonProperty("id")
    public abstract String itemId();

    @Nullable
    @JsonProperty("created_by")
    public abstract String createdUserId();

    @Nullable
    @JsonProperty("archived")
    public abstract Boolean archived();

    @Nullable
    @JsonProperty("created_at")
    public abstract String createDate();

    @Nullable
    @JsonProperty("changes")
    public abstract List<ChangesModel> changes();

    @Nullable
    @JsonProperty("users")
    public abstract List<User> users();

    @Nullable
    @JsonProperty("editMode")
    public abstract Boolean editMode();

    @Nullable
    @JsonProperty("synchronize")
    public abstract Boolean synchronize();

    @Nullable
    public abstract Boolean selected();

    @Nullable
    public abstract Boolean clicked();

    public abstract Builder toBuilder();

    public static ListModel create(String name) {
        return builder()
                .name(name)
                .selected(false)
                .clicked(false)
                .archived(false)
                .items(null)
                .synchronize(false)
                .editMode(false)
                .itemId(UUID.randomUUID().toString())
                .build();
    }

    public static Builder builder() {
        return new AutoValue_ListModel.Builder().editMode(false).synchronize(false);
    }

    public ListModel clearForRemoveRequest() {
        return new AutoValue_ListModel.Builder()
                .itemId(itemId())
                .build();
    }

    public ListModel clearForUpdateRequest() {
        return new AutoValue_ListModel.Builder()
                .itemId(itemId())
                .archived(archived())
                .name(name())
                .build();
    }

    public ListModel clearForCreateRequest() {
        return new AutoValue_ListModel.Builder()
                .items(getItems(true))
                .archived(archived())
                .name(name())
                .build();
    }

    public List<ListItemModel> getItems() {
        return getItems(false);
    }

    private List<ListItemModel> getItems(boolean request) {
        if (items() == null) return new ArrayList<>();
        if (!request) return items();

        List<ListItemModel> requestItems = new ArrayList<>();
        for (ListItemModel itemModel : items()) {
            itemModel = itemModel.clearForRequest().toBuilder().parentId(null).itemId(null).build();
            requestItems.add(itemModel);
        }

        return requestItems;
    }

    public List<User> getUsers() {
        return users() == null ? new ArrayList<>() : users();
    }

    public ListModel doSynchronize() {
        List<ListItemModel> itemModels = getItems();

        for (int indexItem = 0; indexItem < itemModels.size(); indexItem++) {
            itemModels.set(indexItem, itemModels.get(indexItem).doSynchronize());
        }

        return toBuilder().synchronize(true).items(itemModels).build();
    }

    public Boolean isEditMode() {
        return editMode() == null ? false : editMode();
    }

    public Boolean isSelected() {
        return selected() == null ? false : selected();
    }

    public boolean isClicked() {
        return clicked() == null ? false : clicked();
    }

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder selected(Boolean selected);

        public abstract Builder clicked(Boolean clicked);

        @JsonProperty("users")
        public abstract Builder users(List<User> users);

        @JsonProperty("archived")
        public abstract Builder archived(Boolean archived);

        @JsonProperty("name")
        public abstract Builder name(String name);

        @JsonProperty("items")
        public abstract Builder items(List<ListItemModel> items);

        @JsonProperty("id")
        public abstract Builder itemId(String itemId);

        @JsonProperty("created_at")
        public abstract Builder createDate(String createDate);

        @JsonProperty("changes")
        public abstract Builder changes(List<ChangesModel> changes);

        @JsonProperty("editMode")
        public abstract Builder editMode(Boolean editMode);

        @JsonProperty("synchronize")
        public abstract Builder synchronize(Boolean synchronize);

        @Nullable
        @JsonProperty("created_by")
        public abstract Builder createdUserId(String createdUserId);

        public abstract ListModel build();
    }
}
