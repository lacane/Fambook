package com.ourises.list.common.model;

import com.ourises.list.common.entities.ErrorModel;

import rx.Observable;

public interface NetworkModel {
    void connect();

    void disconnect();

    Observable<Boolean> onOfflineModeSubscribe();

    boolean checkForNetwork();

    Observable<Boolean> isOfflineMode();

    Observable<ErrorModel> onErrorModelSubscribe();
}
