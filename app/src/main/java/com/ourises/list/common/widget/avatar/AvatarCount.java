package com.ourises.list.common.widget.avatar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ourises.list.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AvatarCount extends FrameLayout {

    @BindView(R.id.count_avatar_text) TextView avatarText;

    public AvatarCount(Context context) {
        super(context);
        LayoutInflater.from(getContext()).inflate(R.layout.user_avatar_count, this);
        ButterKnife.bind(this);
    }

    @SuppressLint("SetTextI18n")
    public void setCount(int count) {
        if (count > 99) {
            avatarText.setText("99+");
        } else {
            avatarText.setText(String.valueOf(count));
        }
    }
}
