package com.ourises.list.common.model.socket;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ourises.list.common.entities.ErrorModel;
import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.entities.ResponseModelId;
import com.ourises.list.common.entities.User;
import com.ourises.list.navigation.auth.model.AuthTokenModel;
import com.ourises.list.navigation.changes.model.ChangesModel;
import com.ourises.list.navigation.changes.model.ChangesModelForRequest;
import com.ourises.list.navigation.profile.model.ProfileStatus;
import com.ourises.list.navigation.share.model.ShareItemModel;

import java.util.List;

import io.socket.client.Socket;
import rx.Observable;

public class SocketModelImpl extends SocketBase implements SocketModel {

    private Observable<Object> observableDisconnect;
    private Observable<Object> observableConnectError;
    private Observable<Object> observableConnectTimeout;
    private Observable<User> observableConnectAuth;
    private Observable<Object> observableConnect;
    private Observable<ListModel> observableUpdateModel;
    private Observable<ResponseModelId> observableDeleteModel;

    private String[] events = new String[] {
            SocketConst.CREATE_LIST,
            SocketConst.DELETE_LIST,
            SocketConst.GET_LIST_MODELS,
            SocketConst.SHARE_LIST,
            SocketConst.UPDATE_LIST,
            SocketConst.UPDATE_ITEM,
            SocketConst.DELETE_ITEM,
            SocketConst.ADD_ITEM,
            SocketConst.GET_ARCHIVED_LIST,
            SocketConst.LOGIN,
            SocketConst.UPDATE_LIST_EVENT,
            SocketConst.DELETE_LIST_EVENT,
            SocketConst.GET_CHANGES,
            SocketConst.PROFILE_STATUS,
            Socket.EVENT_CONNECT,
            Socket.EVENT_CONNECT_ERROR,
            Socket.EVENT_CONNECT_TIMEOUT,
            Socket.EVENT_DISCONNECT,
    };

    public SocketModelImpl(Socket socket, ObjectMapper objectMapper) {
        setSocket(socket, objectMapper);
    }

    @Override
    public void connect() {
        observableConnectAuth = onObservable(SocketConst.LOGIN, User.class);

        observableConnect = onObservable(Socket.EVENT_CONNECT);
        observableConnectError = onObservable(Socket.EVENT_CONNECT_ERROR);
        observableConnectTimeout = onObservable(Socket.EVENT_CONNECT_TIMEOUT);
        observableDisconnect = onObservable(Socket.EVENT_DISCONNECT);

        observableUpdateModel = onObservable(SocketConst.UPDATE_LIST_EVENT, ListModel.class);
        observableDeleteModel = onObservable(SocketConst.DELETE_LIST_EVENT, ResponseModelId.class);

        socket.connect();
    }

    @Override
    public Observable<User> onAuthSubscribe() {
        return observableConnectAuth;
    }

    @Override
    public Observable<Object> onConnectSubscribe() {
        return observableConnect;
    }

    @Override
    public Observable<Object> onDisconnectSubscribe() {
        return observableDisconnect;
    }

    @Override
    public Observable<Object> onConnectErrorSubscribe() {
        return observableConnectError;
    }

    @Override
    public Observable<Object> onConnectTimeoutSubscribe() {
        return observableConnectTimeout;
    }

    @Override
    public Observable<ListModel> onUpdateModelSubscribe() {
        return observableUpdateModel;
    }

    @Override
    public Observable<ErrorModel> onErrorModelSubscribe() {
        return errorModelObservable();
    }

    @Override
    public Observable<ResponseModelId> onDeleteModelSubscribe() {
        return observableDeleteModel;
    }

    @Override
    public Observable<Object> logout() {
        return null;
    }

    @Override
    public Observable<ProfileStatus> getProfileStatus() {
        return send(SocketConst.PROFILE_STATUS, null, ProfileStatus.class);
    }

    @Override
    public Observable<List<ChangesModel>> getChanges(ChangesModelForRequest itemId) {
        return sendList(SocketConst.GET_CHANGES, itemId, ChangesModel.class);
    }

    @Override
    public Observable<User> login(AuthTokenModel token) {
        return send(SocketConst.LOGIN, token, User.class);
    }

    @Override
    public void disconnect() {
        socket.disconnect();

        for (String event : events) {
            offSocket(event);
        }
    }

    @Override
    public Observable<List<ListModel>> getListModels(boolean history) {
        if (!history) {
            return sendList(SocketConst.GET_LIST_MODELS, ListModel.class);
        } else {
            return sendList(SocketConst.GET_ARCHIVED_LIST, ListModel.class);
        }
    }

    @Override
    public Observable<ListModel> createListModel(ListModel item) {
        return send(SocketConst.CREATE_LIST, item, ListModel.class);
    }

    @Override
    public Observable<ListItemModel> addListItemModel(ListItemModel item) {
        return send(SocketConst.ADD_ITEM, item, ListItemModel.class);
    }

    @Override
    public Observable<Object> removeListItemModel(ListItemModel listItemModel) {
        return send(SocketConst.DELETE_ITEM, listItemModel, Object.class);
    }

    @Override
    public Observable<Object> removeListModel(ListModel listModel) {
        return send(SocketConst.DELETE_LIST, listModel, Object.class);
    }

    @Override
    public Observable<Object> updateListItemModel(ListItemModel newItem) {
        return send(SocketConst.UPDATE_ITEM, newItem, Object.class);
    }

    @Override
    public Observable<Object> updateListModel(ListModel listModel) {
        return send(SocketConst.UPDATE_LIST, listModel, Object.class);
    }

    @Override
    public Observable<Object> shareToFriend(ShareItemModel shareItemModel) {
        return send(SocketConst.SHARE_LIST, shareItemModel, Object.class);
    }
}
