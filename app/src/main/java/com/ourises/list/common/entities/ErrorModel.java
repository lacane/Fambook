package com.ourises.list.common.entities;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_ErrorModel.Builder.class)
public abstract class ErrorModel {

    @Nullable
    @JsonProperty("error")
    public abstract Code code();

    @Nullable
    @JsonProperty("message")
    public abstract String message();

    @AutoValue.Builder
    public abstract static class Builder {
        @Nullable
        @JsonProperty("error")
        public abstract Builder code(Code code);

        @Nullable
        @JsonProperty("message")
        public abstract Builder message(String message);

        public abstract ErrorModel build();
    }
}
