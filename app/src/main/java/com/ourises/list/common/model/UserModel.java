package com.ourises.list.common.model;

import com.ourises.list.common.entities.User;
import com.ourises.list.navigation.profile.model.ProfileStatus;

import rx.Observable;

public interface UserModel {
    void setUser(User user);

    Observable<User> getUser();

    Observable<String> getTokenSession();

    Observable<User> login(String token);

    Observable<User> onAuthSubscribe();

    Observable<Object> logout();

    Observable<ProfileStatus> getListStatus();

    Observable<User> getUserReadyObservable();
}
