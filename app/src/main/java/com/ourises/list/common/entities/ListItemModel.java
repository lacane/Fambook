package com.ourises.list.common.entities;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;

import java.io.Serializable;
import java.util.UUID;

@AutoValue
@JsonDeserialize(builder = AutoValue_ListItemModel.Builder.class)
@JsonIgnoreProperties(ignoreUnknown = true, value = {"selected"})
public abstract class ListItemModel implements HomeDescriptionCommonItem, Serializable {
    public static final int LIST_MODEL_TYPE = 4;

    @Override
    public int id() {
        return 0;
    }

    @Override
    public int type() {
        return LIST_MODEL_TYPE;
    }

    @Nullable
    @JsonProperty("note_id")
    public abstract String parentId();

    @Nullable
    @JsonProperty("id")
    public abstract String itemId();

    @Nullable
    @JsonProperty("name")
    public abstract String name();

    @Nullable
    @JsonProperty("status")
    public abstract Boolean status();

    @Nullable
    @JsonProperty("editMode")
    public abstract Boolean editMode();

    @Nullable
    @JsonProperty("synchronize")
    public abstract Boolean synchronize();

    @Nullable
    public abstract Boolean selected();

    public Boolean isSelected() {
        return selected() == null ? false : selected();
    }

    public static Builder builder() {
        return new AutoValue_ListItemModel.Builder().editMode(false).synchronize(false);
    }

    public Boolean isEditMode() {
        return editMode() == null ? false : editMode();
    }

    public static ListItemModel create(String name, String parentId) {
        return new AutoValue_ListItemModel.Builder()
                .status(false)
                .parentId(parentId)
                .name(name)
                .editMode(false)
                .itemId(UUID.randomUUID().toString())
                .synchronize(false)
                .selected(false)
                .build();
    }

    public ListItemModel clearForRequest() {
        return new AutoValue_ListItemModel.Builder()
                .itemId(itemId())
                .parentId(parentId())
                .name(name())
                .status(status())
                .synchronize(null)
                .build();
    }

    public ListItemModel clearForRemoveRequest() {
        return new AutoValue_ListItemModel.Builder()
                .parentId(parentId())
                .itemId(itemId())
                .build();
    }

    public abstract Builder toBuilder();

    public ListItemModel doSynchronize() {
        return toBuilder().synchronize(true).build();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        @JsonProperty("note_id")
        public abstract Builder parentId(String parentId);

        @JsonProperty("status")
        public abstract Builder status(Boolean status);

        @JsonProperty("id")
        public abstract Builder itemId(String itemId);

        @JsonProperty("name")
        public abstract Builder name(String name);

        @JsonProperty("editMode")
        public abstract Builder editMode(Boolean editMode);

        @JsonProperty("synchronize")
        public abstract Builder synchronize(Boolean synchronize);

        public abstract Builder selected(Boolean selected);

        public abstract ListItemModel build();
    }
}