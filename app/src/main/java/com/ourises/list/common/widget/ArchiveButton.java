package com.ourises.list.common.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ourises.list.R;
import com.ourises.list.common.widget.util.AnimUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArchiveButton extends RelativeLayout {

    @BindView(R.id.archive_button_title) TextView titleView;
    @BindView(R.id.archive_button_image) TintableImageView iconView;
    @BindView(R.id.archive_button_root) ViewGroup rootView;

    private String title;
    private Drawable icon;
    private boolean archived = false;
    private boolean showed = false;

    public ArchiveButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ArchiveButton);
        title = typedArray.getString(R.styleable.ArchiveButton_archiveTitle);
        icon = typedArray.getDrawable(R.styleable.ArchiveButton_archiveIcon);
        typedArray.recycle();

        LayoutInflater.from(getContext()).inflate(R.layout.archive_button, this);
        ButterKnife.bind(this);

        if (title != null) {
            titleView.setText(title);
        }

        if (icon != null) {
            iconView.setImageDrawable(icon);
        }

        setVisibility(INVISIBLE);
        hide();
    }

    public void setTitle(String title) {
        titleView.setText(title);
    }

    public void setIcon(@DrawableRes int resId) {
        iconView.setImageResource(resId);
    }

    public void hide() {
        if (getVisibility() != GONE && !archived) {
            showed = false;
            AnimUtils.goneBottomSlide(this);
        }
    }

    public void show() {
        if (getVisibility() != VISIBLE) {
            showed = true;
            AnimUtils.showBottomSlide(this);
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        rootView.setOnClickListener(onClickListener);
    }

    public void setArchivedState(boolean archived) {
        this.archived = archived;
        if (archived) {
            show();
            titleView.setText(getContext().getString(R.string.unarchive));
            iconView.setImageResource(R.drawable.ic_unarchive_container);
        } else {
            titleView.setText(getContext().getString(R.string.add_to_archive));
            iconView.setImageResource(R.drawable.ic_archive_container);
        }
    }

    public void changeStateForKeyboard(boolean showedKeyboard) {
        if (showed && showedKeyboard) {
            setVisibility(GONE);
        } else if (showed) {
            postDelayed(() -> setVisibility(VISIBLE), 100);
        }
    }
}
