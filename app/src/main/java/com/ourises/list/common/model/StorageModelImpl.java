package com.ourises.list.common.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.entities.User;
import com.orhanobut.hawk.Hawk;
import com.ourises.list.common.widget.Indexed;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class StorageModelImpl implements StorageModel {
    private static final String SESSION = "session";
    private static final String USER = "user";
    private static final String LIST_MODELS = "list_models";
    private static final String LIST_MODELS_HISTORY = "list_models_history";

    private static final String LIST_MODELS_FOR_UPDATING = "list_models_for_updating";
    private static final String LIST_MODELS_FOR_DELETING = "list_models_for_deleting";

    private static final String LIST_ITEM_MODELS_FOR_UPDATING = "list_item_models_for_updating";
    private static final String LIST_ITEM_MODELS_FOR_DELETING = "list_item_models_for_deleting";
    private static final String LIST_ITEM_MODELS_FOR_CREATING = "list_item_models_for_creating";

    public static final String SETTINGS_NOTIFICATIONS = "settings_notifications";

    private final ObjectMapper objectMapper;

    public StorageModelImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public Observable<Boolean> putTokenSession(String token) {
        return Observable.just(Hawk.put(SESSION, token));
    }

    @Override
    public Observable<String> getTokenSession() {
        return Observable.just(Hawk.get(SESSION));
    }

    @Override
    public Observable<User> getUser() {
        return Observable.just(Hawk.get(USER));
    }

    @Override
    public Observable<Boolean> putUser(User user) {
        return Observable.just(Hawk.put(USER, user));
    }

    @Override
    public void putListModels(List<ListModel> listModels, boolean historyMode) {
        Observable.from(listModels)
                .map(model -> {
                    try {
                        return objectMapper.writeValueAsString(model);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .filter(stringModel -> stringModel != null)
                .toList()
                .subscribe(stringListModels -> {
                    Hawk.put(historyMode ? LIST_MODELS_HISTORY : LIST_MODELS, stringListModels);
                });
    }

    @Override
    public Observable<List<ListModel>> getListModels(boolean historyMode) {
        List<String> stringListModels = Hawk.get(historyMode ? LIST_MODELS_HISTORY : LIST_MODELS, new ArrayList<>());
        return Observable.from(stringListModels)
                .map(stringModel -> {
                    try {
                        return objectMapper.readValue(stringModel, ListModel.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .filter(stringModel -> stringModel != null)
                .toList();
    }

    @Override
    public Observable<Boolean> getNotificationsEnableState() {
        return Observable.just(Hawk.get(SETTINGS_NOTIFICATIONS, true));
    }

    @Override
    public void updateNotificationState(boolean state) {
        Hawk.put(SETTINGS_NOTIFICATIONS, state);
    }

    @Override
    public Observable<Object> logout() {
        return Observable.just(Hawk.deleteAll());
    }

    private void putListModelForSynchronize(ListModel listModel, String event) {
        getListModelsForSynchronize(event)
                .subscribe(listModels -> {
                    Observable.from(listModels)
                            .zipWith(Observable.range(0, listModels.size()), Indexed::new)
                            .filter(indexed -> indexed.value.itemId().equals(listModel.itemId()))
                            .toList()
                            .flatMap(indexedList -> {
                                if (indexedList.size() == 0) {
                                    listModels.add(0, listModel);
                                } else {
                                    listModels.set(indexedList.get(0).index, listModel);
                                }
                                return Observable.from(listModels);
                            })
                            .map(listModel1 -> {
                                try {
                                    return objectMapper.writeValueAsString(listModel1);
                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                    return null;
                                }
                            })
                            .filter(stringModel -> stringModel != null)
                            .toList()
                            .subscribe(stringListModels -> {
                                Hawk.put(event, stringListModels);
                            });
                });
    }

    private Observable<List<ListModel>> getListModelsForSynchronize(String event) {
        List<String> stringListModels = Hawk.get(event, new ArrayList<>());
        return Observable.from(stringListModels)
                .map(stringModel -> {
                    try {
                        return objectMapper.readValue(stringModel, ListModel.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .filter(stringModel -> stringModel != null)
                .toList();
    }

    @Override
    public void putListModelForUpdating(ListModel listModel) {
        putListModelForSynchronize(listModel, LIST_MODELS_FOR_UPDATING);
    }

    @Override
    public Observable<List<ListModel>> getListModelsForUpdating() {
        return getListModelsForSynchronize(LIST_MODELS_FOR_UPDATING);
    }

    @Override
    public void removeItemsForSynchronizing() {
        Hawk.delete(LIST_ITEM_MODELS_FOR_CREATING);
        Hawk.delete(LIST_MODELS_FOR_UPDATING);
        Hawk.delete(LIST_MODELS_FOR_DELETING);
        Hawk.delete(LIST_ITEM_MODELS_FOR_UPDATING);
        Hawk.delete(LIST_ITEM_MODELS_FOR_DELETING);
    }

    @Override
    public void putListModelForDeleting(ListModel listModel) {
        putListModelForSynchronize(listModel, LIST_MODELS_FOR_DELETING);
    }

    @Override
    public Observable<List<ListModel>> getListModelsForDeleting() {
        return getListModelsForSynchronize(LIST_MODELS_FOR_DELETING);
    }

    private void putListItemModelForSynchronize(ListItemModel listItemModel, String event) {
        getListItemModelsForSynchronize(event)
                .subscribe(listItemModels -> {
                    Observable.from(listItemModels)
                            .zipWith(Observable.range(0, listItemModels.size()), Indexed::new)
                            .filter(indexed -> indexed.value.itemId().equals(listItemModel.itemId()))
                            .toList()
                            .flatMap(indexedList -> {
                                if (indexedList.size() == 0) {
                                    listItemModels.add(0, listItemModel);
                                } else {
                                    listItemModels.set(indexedList.get(0).index, listItemModel);
                                }
                                return Observable.from(listItemModels);
                            })
                            .map(listModel1 -> {
                                try {
                                    return objectMapper.writeValueAsString(listModel1);
                                } catch (JsonProcessingException e) {
                                    e.printStackTrace();
                                    return null;
                                }
                            })
                            .filter(stringModel -> stringModel != null)
                            .toList()
                            .subscribe(stringListModels -> {
                                Hawk.put(event, stringListModels);
                            });
                });
    }

    private Observable<List<ListItemModel>> getListItemModelsForSynchronize(String event) {
        List<String> stringListModels = Hawk.get(event, new ArrayList<>());
        return Observable.from(stringListModels)
                .map(stringModel -> {
                    try {
                        return objectMapper.readValue(stringModel, ListItemModel.class);
                    } catch (IOException e) {
                        e.printStackTrace();
                        return null;
                    }
                })
                .filter(stringModel -> stringModel != null)
                .toList();
    }

    @Override
    public void putListItemModelForUpdating(ListItemModel itemModel) {
        putListItemModelForSynchronize(itemModel, LIST_ITEM_MODELS_FOR_UPDATING);
    }

    @Override
    public Observable<List<ListItemModel>> getListItemModelForUpdating() {
        return getListItemModelsForSynchronize(LIST_ITEM_MODELS_FOR_UPDATING);
    }

    @Override
    public void putListItemModelForDeleting(ListItemModel itemModel) {
        putListItemModelForSynchronize(itemModel, LIST_ITEM_MODELS_FOR_DELETING);
    }

    @Override
    public Observable<List<ListItemModel>> getListItemModelForDeleting() {
        return getListItemModelsForSynchronize(LIST_ITEM_MODELS_FOR_DELETING);
    }

    @Override
    public void putListItemModelForCreating(ListItemModel itemModel) {
        putListItemModelForSynchronize(itemModel, LIST_ITEM_MODELS_FOR_CREATING);
    }

    @Override
    public Observable<List<ListItemModel>> getListItemModelForCreating() {
        return getListItemModelsForSynchronize(LIST_ITEM_MODELS_FOR_CREATING);
    }
}
