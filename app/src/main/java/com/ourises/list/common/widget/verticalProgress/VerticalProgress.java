package com.ourises.list.common.widget.verticalProgress;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ourises.list.R;
import com.ourises.list.common.entities.ListItemModel;

import java.util.List;

public class VerticalProgress extends LinearLayout {

    private Paint linePaint = new Paint();

    private float circleRadius;
    private boolean isSelected = false;
    private int marginTop;
    private LinearLayout containerView;

    public VerticalProgress(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
        setWillNotDraw(false);

        linePaint.setStyle(Paint.Style.STROKE);
        linePaint.setColor(Color.GRAY);
        linePaint.setStrokeWidth(4);

        circleRadius = getResources().getDimensionPixelOffset(R.dimen._4sdp);
        marginTop = getResources().getDimensionPixelOffset(R.dimen._10sdp);
    }

    public void setTexts(List<ListItemModel> items, boolean isSelected, boolean expanded) {
        this.isSelected = isSelected;

        containerView = new LinearLayout(getContext());
        containerView.setOrientation(VERTICAL);
        for (int index = 0; index < items.size(); index++) {
            ListItemModel item = items.get(index);
            VerticalProgressItemView itemView = new VerticalProgressItemView(getContext());
            itemView.setCompletingItem(item.status());
            itemView.setCircleRadius(circleRadius);
            itemView.setText(item.name());
            LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.topMargin = marginTop;
            itemView.setLayoutParams(params);

            containerView.addView(itemView);
        }

        removeAllViews();
        addView(containerView);
    }

    public void clear() {
        removeAllViews();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (!isSelected) {
            super.onSizeChanged(w, h, oldw, oldh);
        } else {
            super.onSizeChanged(oldw, oldh, oldw, oldh);
        }
    }

    @Override
    public void draw(Canvas canvas) {
        // draw progress vertical line
        if (containerView != null && containerView.getChildCount() > 1) {
            View lastChild = containerView.getChildAt(containerView.getChildCount() - 1);
            float yEndLocation = lastChild.getY();
            float heightOfItem = lastChild.getMeasuredHeight();
            yEndLocation += heightOfItem / 2;

            View firstChild = containerView.getChildAt(0);
            float yStartLocation = firstChild.getY();
            heightOfItem = firstChild.getMeasuredHeight();
            yStartLocation += heightOfItem / 2;

            canvas.drawLine(circleRadius + 1, yStartLocation, circleRadius + 1, yEndLocation, linePaint);
        }

        super.draw(canvas);
    }
}
