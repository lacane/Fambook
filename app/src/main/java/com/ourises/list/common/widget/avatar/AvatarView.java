package com.ourises.list.common.widget.avatar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.ourises.list.R;
import com.ourises.list.common.widget.CircleTransform;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AvatarView extends FrameLayout {

    public enum Type {
        CREATOR,
        MEMBER,
    }

    @BindView(R.id.all_avatar_avatar) AvatarImage avatar;
    @BindView(R.id.all_avatar_shadow) ShadowLayout shadowLayout;

    public AvatarView(Context context) {
        super(context);
        init();
    }

    public AvatarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.user_avatar_view, this);
        ButterKnife.bind(this);
    }

    public void setType(Type type) {
        avatar.setType(type);
    }

    public void setYShadow(int yShadow) {
        shadowLayout.setYShadow(yShadow);
    }

    public void setShadowRadius(int radius) {
        shadowLayout.setShadowRadius(radius);
    }

    public void setAvatar(String imageUrl) {
        if (imageUrl != null) {
            Picasso.with(getContext())
                    .load(imageUrl)
                    .transform(new CircleTransform())
                    .into(avatar);
        } else {
            Picasso.with(getContext()).load(R.drawable.placeholder).transform(new CircleTransform()).into(avatar);
        }
    }
}
