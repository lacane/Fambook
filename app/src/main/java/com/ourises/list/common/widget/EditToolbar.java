package com.ourises.list.common.widget;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ourises.list.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditToolbar extends FrameLayout {

    @BindView(R.id.edit_toolbar_root)
    ViewGroup rootView;
    @BindView(R.id.edit_toolbar_text)
    TextView textView;
    @BindView(R.id.edit_toolbar_star)
    ImageView starButton;
    @BindView(R.id.edit_toolbar_delete)
    ImageView deleteButton;

    private boolean editMode = false;
    private OnChangeStateListener changeStateListener;

    public EditToolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditToolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_edit_toolbar, this);
        ButterKnife.bind(this);
        rootView.setVisibility(GONE);
    }

    public void show() {
        if (!editMode) {
            changeState(true);
            rootView.setVisibility(VISIBLE);
        }
    }

    public void hide() {
        if (editMode) {
            changeState(false);
            rootView.setVisibility(GONE);
        }
    }

    private void changeState(boolean editMode) {
        this.editMode = editMode;

        if (changeStateListener != null) {
            changeStateListener.onChangeState(editMode);
        }
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setCount(int count) {
        textView.setText(String.valueOf(count));
    }

    public void setDeleteListener(OnClickListener listener) {
        deleteButton.setOnClickListener(listener);
        deleteButton.setVisibility(VISIBLE);
    }

    public void setStarListener(OnClickListener listener) {
        starButton.setOnClickListener(listener);
        starButton.setVisibility(VISIBLE);
    }

    public void setOnChangeStateListener(OnChangeStateListener changeStateListener) {
        this.changeStateListener = changeStateListener;
    }

    public void setBackListener(OnClickListener listener) {
        textView.setOnClickListener(listener);
    }

    public interface OnChangeStateListener {
        void onChangeState(boolean editMode);
    }
}
