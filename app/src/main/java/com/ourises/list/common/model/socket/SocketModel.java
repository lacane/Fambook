package com.ourises.list.common.model.socket;

import com.ourises.list.common.entities.ErrorModel;
import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.entities.ResponseModelId;
import com.ourises.list.common.entities.User;
import com.ourises.list.navigation.auth.model.AuthTokenModel;
import com.ourises.list.navigation.changes.model.ChangesModel;
import com.ourises.list.navigation.changes.model.ChangesModelForRequest;
import com.ourises.list.navigation.profile.model.ProfileStatus;
import com.ourises.list.navigation.share.model.ShareItemModel;

import java.util.List;

import rx.Observable;

public interface SocketModel {
    void connect();

    Observable<User> onAuthSubscribe();

    Observable<Object> onConnectSubscribe();

    Observable<Object> onDisconnectSubscribe();

    Observable<Object> onConnectErrorSubscribe();

    Observable<Object> onConnectTimeoutSubscribe();

    Observable<ListModel> onUpdateModelSubscribe();

    Observable<ErrorModel> onErrorModelSubscribe();

    Observable<User> login(AuthTokenModel token);

    void disconnect();

    Observable<List<ListModel>> getListModels(boolean history);

    Observable<ListModel> createListModel(ListModel item);

    Observable<ListItemModel> addListItemModel(ListItemModel item);

    Observable<Object> removeListItemModel(ListItemModel listItemModel);

    Observable<Object> removeListModel(ListModel listModel);

    Observable<Object> updateListItemModel(ListItemModel newItem);

    Observable<Object> updateListModel(ListModel listModel);

    Observable<Object> shareToFriend(ShareItemModel shareItemModel);

    Observable<ResponseModelId> onDeleteModelSubscribe();

    Observable<Object> logout();

    Observable<ProfileStatus> getProfileStatus();

    Observable<List<ChangesModel>> getChanges(ChangesModelForRequest itemId);
}
