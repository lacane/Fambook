package com.ourises.list.common.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.ourises.list.R;
import com.ourises.list.common.widget.util.AttrUtil;


/**
 * {@link android.widget.ImageView} with support for drawable tinting
 * <a href="http://stackoverflow.com/a/18724834">StackOverflow question</a>
 * <a href="https://code.google.com/p/android/issues/detail?id=18220">Google issue</a>
 */
public class TintableImageView extends AppCompatImageView {
    private ColorStateList tint;
    private PorterDuff.Mode tinMode;

    public TintableImageView(Context context) {
        this(context, null);
    }

    public TintableImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TintableImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.TintableImageView,
                defStyle, 0);
        if (ta.hasValue(R.styleable.TintableImageView_tint)) {
            tint = ta.getColorStateList(R.styleable.TintableImageView_tint);
        }
        tinMode = AttrUtil.parseTintMode(ta.getInt(R.styleable.TintableImageView_tintMode, -1),
                PorterDuff.Mode.SRC_IN);
        ta.recycle();
        updateTintColor();
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (tint != null) {
            updateTintColor();
        }
    }

    public void setSupportImageTintList(ColorStateList tint) {
        this.tint = tint;
        if (tint == null) {
            setColorFilter(null);
        } else {
            setColorFilter(tint.getColorForState(getDrawableState(), 0), tinMode);
        }
    }

    private void updateTintColor() {
        if (tint == null) {
            setColorFilter(null);
        } else {
            setColorFilter(tint.getColorForState(getDrawableState(), 0), tinMode);
        }
    }
}