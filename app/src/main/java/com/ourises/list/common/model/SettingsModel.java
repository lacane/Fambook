package com.ourises.list.common.model;

import android.content.Context;

import rx.Observable;

public interface SettingsModel {
    Observable<Boolean> getNotificationsEnableState();

    void updateNotificationState(boolean state);

    Observable<Object> logout(Context context);
}
