package com.ourises.list.common.widget.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
    public static String getDateForList(String source) {
        return new SimpleDateFormat("MMMM, dd", Locale.getDefault()).format(getDate(source));
    }

    public static String getDateForChangesList(String source) {
        return new SimpleDateFormat("HH:mm MMMM, dd", Locale.getDefault()).format(getDate(source));
    }

    public static Date getDate(String source) {
        if (source == null) return new Date();

        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());
        try {
            Date date = sourceFormat.parse(source);
            return date;
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        return new Date();
    }
}
