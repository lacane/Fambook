package com.ourises.list.common.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import rx.Observable;

public class OkHttpModelImpl implements OkHttpModel {
    private final OkHttpClient client;

    public OkHttpModelImpl(OkHttpClient client) {
        this.client = client;
    }

    @Override
    public Observable<String> getAccessToken(String clientId, String redirectUri, String clientSecret, String code) {
        String url = "https://graph.facebook.com/v2.9/oauth/access_token?"
                + "client_id=%s"
                + "&redirect_uri=%s"
                + "&client_secret=%s"
                + "&code=%s";
        url = String.format(url, clientId, redirectUri, clientSecret, code);
        Request request = new Request.Builder().url(url).build();

        return Observable.create(subscriber -> {
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    subscriber.onError(e);
                }

                @Override
                public void onResponse(Call call, okhttp3.Response response) throws IOException {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String token = jsonObject.getString("access_token");
                        subscriber.onNext(token);
                        subscriber.onCompleted();
                    } catch (JSONException e) {
                        subscriber.onError(e);
                    }
                }
            });
        });
    }
}
