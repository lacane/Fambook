package com.ourises.list.common.widget.avatar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.ourises.list.R;

public class AvatarImage extends AppCompatImageView {
    private Paint paintBorder = new Paint();
    private int color;

    public AvatarImage(Context context, AttributeSet attrs) {
        super(context, attrs);

        color = ContextCompat.getColor(getContext(), android.R.color.transparent);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        float r = getMeasuredHeight() / 2f;

        int strokeWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2,
                getResources().getDisplayMetrics());

        paintBorder.setColor(color);
        paintBorder.setStyle(Paint.Style.STROKE);
        paintBorder.setAntiAlias(true);
        paintBorder.setStrokeWidth(strokeWidth);
        canvas.drawCircle(r, r, r - getResources().getDimension(R.dimen._1sdp) / 3, paintBorder);
    }

    public void setType(AvatarView.Type type) {
        switch (type) {
            case CREATOR:
                color = ContextCompat.getColor(getContext(), R.color.color_avatar_border);
                break;
            case MEMBER:
                color = ContextCompat.getColor(getContext(), android.R.color.transparent);
            default:
                break;
        }
    }
}
