package com.ourises.list.common.model;

import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.entities.ResponseModelId;
import com.ourises.list.common.model.socket.SocketModel;
import com.ourises.list.common.widget.Indexed;
import com.ourises.list.navigation.changes.model.ChangesModel;
import com.ourises.list.navigation.changes.model.ChangesModelForRequest;
import com.ourises.list.navigation.homeDescription.model.SynchronizedListModel;
import com.ourises.list.navigation.homeDescription.model.UpdateAction;
import com.ourises.list.navigation.share.model.ShareItemModel;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.observables.ConnectableObservable;
import rx.schedulers.Schedulers;
import rx.subjects.ReplaySubject;

public class HomeListModelImpl implements HomeListModel {
    private final StorageModel storageModel;
    private final SocketModel socketModel;
    private final NetworkModel networkModel;
    private List<ListModel> listModels;

    private boolean historyMode = false;

    private Subscriber<? super SynchronizedListModel> synchronizedListModelSubscriber;
    private ConnectableObservable<SynchronizedListModel> synchronizedListModelObservable;
    private List<SynchronizedListModel> synchronizedListModels = new ArrayList<>();

    private Subscriber<? super String> updateStorageListModelSubscriber;
    private ConnectableObservable<String> updateStorageListModelObservable;

    private Subscriber<? super UpdateAction> updateActionSubscriber;
    private ConnectableObservable<UpdateAction> updateActionObservable;

    public HomeListModelImpl(StorageModel storageModel, SocketModel socketModel, NetworkModel networkModel) {
        this.storageModel = storageModel;
        this.socketModel = socketModel;
        this.networkModel = networkModel;
    }

    @Override
    public void setListModels(List<ListModel> listModels) {
        if (listModels != null) {
            this.listModels = listModels;
            storageModel.putListModels(listModels, historyMode);
            sendSynchronizedListModels();
        }
    }

    @Override
    public Observable<List<ListModel>> getListModels() {
        return socketModel.getListModels(historyMode)
                .flatMap(listModels1 -> Observable.just(doSynchronize(listModels1)));
    }

    @Override
    public Observable<List<ListModel>> getListModelsFromStorage() {
        return storageModel.getListModels(historyMode);
    }

    @Override
    public Observable<Object> shareToFriend(String userId, String listId) {
        return socketModel.shareToFriend(ShareItemModel.create(listId, userId)).doOnCompleted(() ->
                sendUpdateAction(new UpdateAction(UpdateAction.ActionType.UPDATE, listId)));
    }

    @Override
    public Observable<Boolean> synchronizeListModels() {
        return storageModel.getListModelsForUpdating()
                .flatMap(Observable::from)
                .flatMap(this::sendListModelToServer)
                .toList()
                .flatMap(list -> storageModel.getListModelsForDeleting())
                .flatMap(Observable::from)
                .flatMap(this::removeListModelFromServer)
                .toList()
                .flatMap(list -> storageModel.getListModels(false))
                .flatMap(Observable::from)
                .filter(listModel -> !listModel.synchronize())
                .flatMap(this::synchronizeListModel)
                .toList()
                .flatMap(list -> storageModel.getListModels(true))
                .flatMap(Observable::from)
                .filter(listModel -> !listModel.synchronize())
                .flatMap(this::synchronizeListModel)
                .toList()
                .flatMap(list -> storageModel.getListItemModelForCreating())
                .flatMap(Observable::from)
                .flatMap(listItemModel -> createListItemModelToServer(listItemModel, true))
                .toList()
                .flatMap(list -> storageModel.getListItemModelForDeleting())
                .flatMap(Observable::from)
                .flatMap(this::removeListItemModelToServer)
                .toList()
                .flatMap(list -> storageModel.getListItemModelForUpdating())
                .flatMap(Observable::from)
                .flatMap(this::updateListItemModelToServer)
                .toList()
                .flatMap(list -> {
                    storageModel.removeItemsForSynchronizing();
                    return networkModel.isOfflineMode();
                });
    }

    @Override
    public Observable<SynchronizedListModel> onSynchronizedListModelSubscribe() {
        if (synchronizedListModelObservable == null) {
            synchronizedListModelObservable = ReplaySubject.create(
                    (Observable.OnSubscribe<SynchronizedListModel>) subscriber ->
                            synchronizedListModelSubscriber = subscriber).publish();

            synchronizedListModelObservable.connect();
        }

        return synchronizedListModelObservable;
    }

    @Override
    public Observable<String> onUpdateStorageListModelSubscribe() {
        if (updateStorageListModelObservable == null) {
            updateStorageListModelObservable = ReplaySubject.create(
                    (Observable.OnSubscribe<String>) subscriber ->
                            updateStorageListModelSubscriber = subscriber).publish();

            updateStorageListModelObservable.connect();
        }

        return updateStorageListModelObservable;
    }

    @Override
    public Observable<UpdateAction> onUpdateActionSubscribe() {
        if (updateActionObservable == null) {
            updateActionObservable = ReplaySubject.create(
                    (Observable.OnSubscribe<UpdateAction>) subscriber ->
                            updateActionSubscriber = subscriber).publish();

            updateActionObservable.connect();
        }

        return updateActionObservable;
    }

    private void sendUpdateAction(UpdateAction updateAction) {
        if (updateActionSubscriber != null) {
            updateActionSubscriber.onNext(updateAction);
        }
    }

    private void sendSynchronizedListModels() {
        if (synchronizedListModelSubscriber != null) {
            for (SynchronizedListModel synchronizedListModel : synchronizedListModels) {
                synchronizedListModelSubscriber.onNext(synchronizedListModel);
            }
        }
        synchronizedListModels.clear();
    }

    private void sendUpdatedListModel(String itemId) {
        if (updateStorageListModelSubscriber != null) {
            updateStorageListModelSubscriber.onNext(itemId);
        }
    }

    private void addSynchronizedModelId(String oldListModel, String newListModel) {
        if (oldListModel != null && newListModel != null) {
            synchronizedListModels.add(
                    SynchronizedListModel.create(oldListModel, newListModel)
            );
        }
    }

    @Override
    public void setHistoryMode(boolean historyMode) {
        this.historyMode = historyMode;
    }

    @Override
    public Observable<List<ChangesModel>> getChanges(String itemId) {
        return socketModel.getChanges(ChangesModelForRequest.create(itemId));
    }

    private List<ListModel> doSynchronize(List<ListModel> list) {
        for (int indexList = 0; indexList < list.size(); indexList++) {
            ListModel listModel = list.get(indexList);
            list.set(indexList, listModel.doSynchronize());
        }

        return list;
    }

    @Override
    public Observable<ListModel> synchronizeListModel(ListModel listModel) {
        if (!listModel.synchronize()) {
            return socketModel.createListModel(listModel.clearForCreateRequest())
                    .map(listModel1 -> listModel1.toBuilder().synchronize(true).build())
                    .doOnNext(listModel1 -> addSynchronizedModelId(listModel.itemId(), listModel1.itemId()));
        } else {
            return Observable.from(listModel.getItems())
                    .flatMap(this::synchronizeListItemModel)
                    .toList()
                    .map(listItemModels -> listModel.toBuilder().items(listItemModels).build());
        }
    }

    @Override
    public Observable<ListModel> getListModel(String itemId) {
        return Observable.from(listModels)
                .filter(listModel -> itemId.equals(listModel.itemId()));
    }

    @Override
    public Observable<ListItemModel> synchronizeListItemModel(ListItemModel itemModel) {
        if (!itemModel.synchronize()) {
            networkModel.isOfflineMode()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(offlineMode -> {
                        if (offlineMode) {
                            storageModel.putListItemModelForCreating(itemModel);
                        }
                    });
            return createListItemModelToServer(itemModel, false);
        } else {
            return Observable.just(itemModel);
        }
    }

    private Observable<ListItemModel> createListItemModelToServer(ListItemModel itemModel, boolean synchronizing) {
        return socketModel.addListItemModel(itemModel.clearForRequest().toBuilder().itemId(null).build())
                .doOnNext(listItemModel -> {
                    if (synchronizing) {
                        addSynchronizedModelId(itemModel.parentId(), itemModel.parentId());
                    }
                });
    }

    @Override
    public Observable<Object> updateListItemModel(ListItemModel itemModel) {
        networkModel.isOfflineMode()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(offlineMode -> {
                    if (offlineMode) {
                        storageModel.putListItemModelForUpdating(itemModel);
                    }
                });
        return updateListItemModelToServer(itemModel);
    }

    private Observable<Object> updateListItemModelToServer(ListItemModel itemModel) {
        return socketModel.updateListItemModel(itemModel.clearForRequest());
    }

    @Override
    public Observable<ListModel> onUpdateModelSubscribe() {
        return socketModel.onUpdateModelSubscribe();
    }

    @Override
    public Observable<ResponseModelId> onDeleteModelSubscribe() {
        return socketModel.onDeleteModelSubscribe();
    }

    @Override
    public Observable<Object> removeListModel(ListModel listModel) {
        networkModel.isOfflineMode()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(offlineMode -> {
                    if (offlineMode) {
                        storageModel.putListModelForDeleting(listModel);
                    }
                });
        sendUpdateAction(new UpdateAction(UpdateAction.ActionType.DELETED, listModel.itemId()));
        return removeListModelFromServer(listModel);
    }

    private Observable<Object> removeListModelFromServer(ListModel listModel) {
        return socketModel.removeListModel(listModel.clearForRemoveRequest());
    }

    @Override
    public Observable<Object> updateListModel(ListModel listModel) {
        networkModel.isOfflineMode()
                .subscribeOn(Schedulers.io())
                .subscribe(offlineMode -> {
                    if (offlineMode) {
                        storageModel.putListModelForUpdating(listModel);
                    }
                });

        Observable.from(listModels)
                .zipWith(Observable.range(0, listModels.size()), Indexed::new)
                .filter(indexed -> listModel.itemId().equals(indexed.value.itemId()))
                .subscribe(indexed -> {
                    listModels.set(indexed.index, listModel);
                    setListModels(listModels);
                }, Throwable::printStackTrace);

        updateListModelInStorage(listModel);

        if (historyMode != listModel.archived()) {
            sendUpdateAction(new UpdateAction(UpdateAction.ActionType.ARCHIVED_STATE, listModel.itemId()));
        }

        return sendListModelToServer(listModel);
    }

    private Observable<Object> sendListModelToServer(ListModel listModel) {
        return socketModel.updateListModel(listModel.clearForUpdateRequest());
    }

    private void changeArchivedStatusModel(ListModel listModel) {
        storageModel.getListModels(historyMode)
                .subscribe(listModels1 -> changeArchiveStatusFromCurrentList(listModels1, listModel.itemId()));

        storageModel.getListModels(!historyMode)
                .subscribe(listModels1 -> {
                    listModels1.add(listModel);
                    storageModel.putListModels(listModels1, !historyMode);
                });
    }

    private void changeArchiveStatusFromCurrentList(List<ListModel> listModels, String itemId) {
        this.listModels = listModels;

        Observable.from(listModels)
                .zipWith(Observable.range(0, listModels.size()), Indexed::new)
                .filter(indexed -> itemId.equals(indexed.value.itemId()))
                .subscribe(indexed -> {
                    listModels.remove(indexed.index);
                    setListModels(listModels);
                });
    }

    @Override
    public ListModel addListModel(ListModel listModel) {
        networkModel.isOfflineMode()
                .subscribeOn(Schedulers.io())
                .subscribe(offlineMode -> {
                    if (offlineMode) {
                        storageModel.putListModelForUpdating(listModel);
                    }
                });

        if (listModels == null) {
            listModels = new ArrayList<>();
        }
        listModels = new ArrayList<>(listModels);
        listModels.add(listModel);
        setListModels(listModels);
        return listModels.get(listModels.size() - 1);
    }

    @Override
    public Observable<Object> removeListItemModel(ListItemModel listItemModel) {
        networkModel.isOfflineMode()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(offlineMode -> {
                    if (offlineMode) {
                        storageModel.putListItemModelForDeleting(listItemModel);
                    }
                });
        return removeListItemModelToServer(listItemModel);
    }

    private Observable<Object> removeListItemModelToServer(ListItemModel listItemModel) {
        return socketModel.removeListItemModel(listItemModel.clearForRemoveRequest());
    }

    @Override
    public void updateListItemsInStorage(String parentId, List<ListItemModel> listItemModels) {
        Observable.from(listModels)
                .zipWith(Observable.range(0, listModels.size()), Indexed::new)
                .filter(indexed -> parentId.equals(indexed.value.itemId()))
                .subscribe(indexed -> {
                    ListModel listModel = listModels.get(indexed.index);
                    listModel = listModel.toBuilder().items(listItemModels).build();
                    listModels.set(indexed.index, listModel);
                    setListModels(listModels);
                    sendUpdatedListModel(parentId);
                });
    }

    @Override
    public void updateListModelInStorage(ListModel listModel) {
        Observable.from(listModels)
                .zipWith(Observable.range(0, listModels.size()), Indexed::new)
                .filter(indexed -> listModel.itemId().equals(indexed.value.itemId()))
                .subscribe(indexed -> {
                    listModels.set(indexed.index, listModel);
                    setListModels(listModels);
                    sendUpdatedListModel(listModel.itemId());
                });
    }
}
