package com.ourises.list.common.widget.util;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;

import com.ourises.list.R;

import java.util.Locale;

public class InfoDialog {

    private static ProgressDialog progressDialog;

    private static DialogInterface.OnClickListener defaultListener = (dialogInterface, i) -> {
    };

    public static void stopProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public static void showProgress(Context context, String title) {
        stopProgress();

        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(true);
        progressDialog.setMessage(context.getString(R.string.loading));
        progressDialog.setTitle(title);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    public static void show(Context context, String title, String message) {
        show(context, title, message, null, null, null, null);
    }

    public static void show(Context context, String title, String message,
                            DialogInterface.OnClickListener positiveListener) {
        show(context, title, message, context.getString(R.string.ok), null, positiveListener, null);
    }

    public static void show(Context context, String title, String message,
                            String positiveButton, String negativeButton,
                            DialogInterface.OnClickListener positiveListener,
                            DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);

        adb.setTitle(title)
                .setCancelable(false)
                .setMessage(message);

        if (positiveButton != null) {
            if (positiveListener != null)
                adb.setPositiveButton(positiveButton, positiveListener);
            else
                adb.setPositiveButton(positiveButton, defaultListener);
        } else
            adb.setPositiveButton(context.getString(R.string.ok), defaultListener);

        if (negativeButton != null) {
            if (negativeListener != null)
                adb.setNegativeButton(negativeButton, negativeListener);
            else
                adb.setNegativeButton(negativeButton, defaultListener);
        }

        adb.show();
    }

    public static void showSnackbar(CoordinatorLayout rootView, String text) {
        Snackbar snackbar = Snackbar.make(rootView, text.toUpperCase(Locale.getDefault()), 1500);
        snackbar.show();
    }
}