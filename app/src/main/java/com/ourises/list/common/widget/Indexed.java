package com.ourises.list.common.widget;

public class Indexed<T> {
    public int index;
    public final T value;
    public Indexed(T value, int index) {
        this.index = index;
        this.value = value;
    }

    public Indexed setIndex(int index) {
        this.index = index;
        return this;
    }
    @Override
    public String toString() {
        return index + ") " + value;
    }
}