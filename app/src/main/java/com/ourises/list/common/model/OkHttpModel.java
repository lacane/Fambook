package com.ourises.list.common.model;

import rx.Observable;

public interface OkHttpModel {
    Observable<String> getAccessToken(String clientId, String redirectUri, String clientSecret, String code);
}
