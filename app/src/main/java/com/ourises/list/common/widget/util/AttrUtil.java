package com.ourises.list.common.widget.util;

import android.graphics.PorterDuff;
import android.os.Build;

public class AttrUtil {
    public static PorterDuff.Mode parseTintMode(int value, PorterDuff.Mode defaultMode) {
        switch (value) {
            case 3:
                return PorterDuff.Mode.SRC_OVER;
            case 5:
                return PorterDuff.Mode.SRC_IN;
            case 9:
                return PorterDuff.Mode.SRC_ATOP;
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            case 16:
                return Build.VERSION.SDK_INT >= 11
                        ? PorterDuff.Mode.valueOf("ADD")
                        : defaultMode;
            default:
                return defaultMode;
        }
    }
}