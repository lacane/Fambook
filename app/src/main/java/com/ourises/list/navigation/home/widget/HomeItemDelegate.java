package com.ourises.list.navigation.home.widget;


import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.alapshin.genericrecyclerview.ViewHolderDelegate;
import com.ourises.list.common.entities.ListModel;

public class HomeItemDelegate implements ViewHolderDelegate<ListModel, ListItemViewHolder> {

    @Override
    public boolean isForViewType(@NonNull ListModel listModel) {
        return true;
    }

    @NonNull
    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        return new ListItemViewHolder(new ListItemView(viewGroup.getContext()));
    }

    @Override
    public void onBindViewHolder(@NonNull ListItemViewHolder listItemViewHolder, ListModel listModel) {
        listItemViewHolder.onBindViewHolder(listModel);
    }

    @Override
    public Class<ListItemViewHolder> getViewHolderType() {
        return ListItemViewHolder.class;
    }
}
