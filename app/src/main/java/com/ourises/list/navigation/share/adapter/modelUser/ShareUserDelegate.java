package com.ourises.list.navigation.share.adapter.modelUser;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.alapshin.genericrecyclerview.ViewHolderDelegate;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItem;
import com.ourises.list.navigation.share.adapter.common.ShareCommonViewHolder;

public class ShareUserDelegate implements ViewHolderDelegate<ShareCommonItem, ShareCommonViewHolder> {
    @Override
    public boolean isForViewType(@NonNull ShareCommonItem item) {
        return item.type() == ShareUserItem.USER_TYPE;
    }

    @NonNull
    @Override
    public ShareCommonViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        return new ShareUserViewHolder(new ShareUserItemView(viewGroup.getContext()));
    }

    @Override
    public void onBindViewHolder(@NonNull ShareCommonViewHolder holder, ShareCommonItem item) {
        holder.onBindViewHolder(item);
    }

    @Override
    public Class<ShareCommonViewHolder> getViewHolderType() {
        return ShareCommonViewHolder.class;
    }
}
