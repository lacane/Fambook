package com.ourises.list.navigation.homeDescription.adapter.modelTitle;

import android.content.Context;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.ourises.list.R;
import com.ourises.list.common.widget.EditTextImeAction;
import com.ourises.list.common.widget.util.SystemUtil;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItemView;

import butterknife.BindView;

public class HomeDescriptionTitleItemView extends HomeDescriptionCommonItemView {

    @BindView(R.id.home_description_item_title_text) TextView titleTextView;
    @BindView(R.id.home_description_item_title_edit) EditTextImeAction titleEditView;

    @Override
    protected int getLayoutRes() {
        return R.layout.home_description_item_title;
    }

    public HomeDescriptionTitleItemView(Context context) {
        super(context);

        titleEditView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                String text = titleEditView.getText().toString();
                titleTextView.setText(text);
                onChangeItemContentListener.onClickOnChangeTitle(text);

                postDelayed(() -> {
                    titleTextView.setVisibility(VISIBLE);
                    titleEditView.setVisibility(INVISIBLE);
                    titleEditView.setEnabled(false);
                }, 100);
                return false;
            }

            return true;
        });

        titleEditView.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                titleTextView.setVisibility(VISIBLE);
                titleEditView.setVisibility(INVISIBLE);
            }
        });

        titleTextView.setOnClickListener(v -> {
            titleTextView.setVisibility(INVISIBLE);
            titleEditView.setVisibility(VISIBLE);

            titleEditView.setEnabled(true);
            titleEditView.selectAll();
            titleEditView.post(() -> SystemUtil.showKeyboard(getContext(), titleEditView));
        });
    }

    public void onBind(HomeDescriptionTitleItem item) {

        titleEditView.setText(item.title());
        titleTextView.setText(item.title());

        titleEditView.setVisibility(INVISIBLE);
        titleTextView.setVisibility(VISIBLE);
    }
}
