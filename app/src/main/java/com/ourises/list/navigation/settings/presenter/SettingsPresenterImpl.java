package com.ourises.list.navigation.settings.presenter;

import android.content.Context;

import com.alapshin.arctor.presenter.PresenterBundle;
import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.ourises.list.common.model.SettingsModel;
import com.ourises.list.navigation.settings.view.SettingsView;

import javax.annotation.Nullable;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SettingsPresenterImpl extends RxPresenter<SettingsView> implements SettingsPresenter {
    private final SettingsModel settingsModel;

    public SettingsPresenterImpl(SettingsModel settingsModel) {
        this.settingsModel = settingsModel;
    }

    @Override
    public void onCreate(@Nullable PresenterBundle bundle) {
        super.onCreate(bundle);
        Subscription notificationSubscription = settingsModel.getNotificationsEnableState()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(enableNotification -> {
                    getView().setNotificationState(enableNotification);
                });
        addSubscription(notificationSubscription);
    }

    @Override
    public void updateNotificationState(boolean checked) {
        settingsModel.updateNotificationState(checked);
    }

    @Override
    public void logout(Context context) {
        Subscription dataSubscription = settingsModel.logout(context)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                    getView().onLogoutComplete();
                });
        addSubscription(dataSubscription);
    }
}
