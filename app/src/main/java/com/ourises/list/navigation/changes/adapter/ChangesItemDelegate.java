package com.ourises.list.navigation.changes.adapter;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.alapshin.genericrecyclerview.ViewHolderDelegate;
import com.ourises.list.navigation.changes.model.ChangesModel;

public class ChangesItemDelegate implements ViewHolderDelegate<ChangesModel, ChangesViewHolder> {
    @Override
    public boolean isForViewType(@NonNull ChangesModel changesModel) {
        return true;
    }

    @NonNull
    @Override
    public ChangesViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        return new ChangesViewHolder(new ChangesItemView(viewGroup.getContext()));
    }

    @Override
    public void onBindViewHolder(@NonNull ChangesViewHolder changesViewHolder, ChangesModel changesModel) {
        changesViewHolder.onBindViewHolder(changesModel);
    }

    @Override
    public Class<ChangesViewHolder> getViewHolderType() {
        return ChangesViewHolder.class;
    }
}
