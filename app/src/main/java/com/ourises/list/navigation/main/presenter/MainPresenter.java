package com.ourises.list.navigation.main.presenter;

import com.alapshin.arctor.presenter.Presenter;
import com.ourises.list.navigation.main.view.MainView;

public interface MainPresenter extends Presenter<MainView> {
    void getUser();

    void connect();

    void login();
}
