package com.ourises.list.navigation.home.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ourises.list.R;
import com.ourises.list.common.entities.User;
import com.ourises.list.common.widget.Indexed;
import com.ourises.list.common.widget.avatar.AvatarCount;
import com.ourises.list.common.widget.avatar.AvatarView;
import com.ourises.list.navigation.homeDescription.widget.ShareButton;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class UsersView extends RelativeLayout {

    @BindView(R.id.home_users_view_container) LinearLayout usersContainer;
    private int leftMarginMember;

    private int countOfViews = 0;
    private int size;
    private boolean needAddToShareButton = false;
    private OnClickListener shareClickListener;

    public UsersView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(getContext()).inflate(R.layout.home_users_view, this);
        ButterKnife.bind(this);
    }

    public void setUsers(@Nonnull List<User> users, String createdUserId) {
        List<User> usersList = new ArrayList<>();

        for (User user : users) {
            if (user.id().equals(createdUserId)) {
                usersList.add(0, user);
            } else {
                usersList.add(user);
            }
        }

        post(() -> setUsers(usersList));
    }

    private void setUsers(List<User> users) {
        usersContainer.removeAllViews();

        int countOtherUsers = users.size() - countOfViews;
        int endValue = countOtherUsers > 0 ? countOfViews - 1 : users.size();
        for (int index = 0; index < endValue; index++) {
            if (index == 0 && needAddToShareButton) {
                addShareButton();
            }

            User user = users.get(index);
            AvatarView avatarView = new AvatarView(getContext());
            settingAvatarView(index, avatarView);
            avatarView.setAvatar(user.photo());
            usersContainer.addView(avatarView);
        }

        if (countOtherUsers > 0) {
            AvatarCount avatarCount = new AvatarCount(getContext());
            avatarCount.setCount(countOtherUsers);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, size);
            params.leftMargin = leftMarginMember;
            avatarCount.setLayoutParams(params);
            usersContainer.addView(avatarCount);
        }
    }

    private void addShareButton() {
        ShareButton shareButton = new ShareButton(getContext());
        shareButton.setOnClickListener(shareClickListener);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, size);
        shareButton.setLayoutParams(params);
        usersContainer.addView(shareButton);
    }

    private void settingAvatarView(int index, AvatarView view) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, size);
        if (index == 0) {
            view.setType(AvatarView.Type.CREATOR);
            if (needAddToShareButton) {
                params.leftMargin = leftMarginMember;
            }
        } else {
            view.setType(AvatarView.Type.MEMBER);
            params.leftMargin = leftMarginMember;
        }
        view.setLayoutParams(params);
    }

    public void withShareButton() {
        this.needAddToShareButton = true;
    }

    public void setShareClickListener(OnClickListener clickListener) {
        this.shareClickListener = clickListener;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        size = h;
        determineCountOfViews(w, h);
    }

    private void determineCountOfViews(int width, int height) {
        countOfViews = width / height;
        if (countOfViews > 1) {
            leftMarginMember = width - height * countOfViews;
            leftMarginMember = leftMarginMember / (countOfViews - 1);
        }

        if (needAddToShareButton) {
            countOfViews--;
        }
    }
}
