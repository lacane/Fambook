package com.ourises.list.navigation.profile.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_ProfileStatus.Builder.class)
public abstract class ProfileStatus {

    @JsonProperty("in_progress")
    public abstract Integer inProgress();

    @JsonProperty("archived")
    public abstract Integer archived();

    @AutoValue.Builder
    public abstract static class Builder {
        @JsonProperty("in_progress")
        public abstract Builder inProgress(Integer inProgress);

        @JsonProperty("archived")
        public abstract Builder archived(Integer archived);

        public abstract ProfileStatus build();
    }
}
