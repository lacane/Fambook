package com.ourises.list.navigation.homeDescription.adapter.modelUsers;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.alapshin.genericrecyclerview.ViewHolderDelegate;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonViewHolder;

public class HomeDescriptionUsersDelegate
        implements ViewHolderDelegate<HomeDescriptionCommonItem, HomeDescriptionCommonViewHolder> {
    @Override
    public boolean isForViewType(@NonNull HomeDescriptionCommonItem homeDescriptionCommonItem) {
        return homeDescriptionCommonItem.type() == HomeDescriptionUsersItem.USERS_MODEL_TYPE;
    }

    @NonNull
    @Override
    public HomeDescriptionCommonViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        HomeDescriptionUsersItemView itemView = new HomeDescriptionUsersItemView(viewGroup.getContext());
        return new HomeDescriptionUsersViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeDescriptionCommonViewHolder holder, HomeDescriptionCommonItem item) {
        holder.onBindViewHolder(item);
    }

    @Override
    public Class<HomeDescriptionCommonViewHolder> getViewHolderType() {
        return HomeDescriptionCommonViewHolder.class;
    }
}
