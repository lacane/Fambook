package com.ourises.list.navigation.homeDescription.adapter.modelList;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.alapshin.genericrecyclerview.ViewHolderDelegate;
import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonViewHolder;

public class HomeDescriptionListDelegate
        implements ViewHolderDelegate<HomeDescriptionCommonItem, HomeDescriptionCommonViewHolder> {
    @Override
    public boolean isForViewType(@NonNull HomeDescriptionCommonItem homeDescriptionCommonItem) {
        return homeDescriptionCommonItem.type() == ListItemModel.LIST_MODEL_TYPE;
    }

    @NonNull
    @Override
    public HomeDescriptionCommonViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        HomeDescriptionListItemView itemView = new HomeDescriptionListItemView(viewGroup.getContext());
        return new HomeDescriptionListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeDescriptionCommonViewHolder holder, HomeDescriptionCommonItem item) {
        holder.onBindViewHolder(item);
    }

    @Override
    public Class<HomeDescriptionCommonViewHolder> getViewHolderType() {
        return HomeDescriptionCommonViewHolder.class;
    }
}
