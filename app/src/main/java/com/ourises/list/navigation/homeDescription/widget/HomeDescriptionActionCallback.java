package com.ourises.list.navigation.homeDescription.widget;

import android.os.Build;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import com.ourises.list.R;

public class HomeDescriptionActionCallback implements ActionMode.Callback {

    private OnActionListener onActionListener;

    public HomeDescriptionActionCallback(OnActionListener onActionListener) {
        this.onActionListener = onActionListener;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.menu_home_description, menu);
        return true;
    }
 
    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        if (Build.VERSION.SDK_INT < 11) {
            MenuItemCompat.setShowAsAction(menu.findItem(R.id.menu_home_description_select), MenuItemCompat.SHOW_AS_ACTION_NEVER);
            MenuItemCompat.setShowAsAction(menu.findItem(R.id.menu_home_description_delete), MenuItemCompat.SHOW_AS_ACTION_NEVER);
        } else {
            menu.findItem(R.id.menu_home_description_select).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            menu.findItem(R.id.menu_home_description_delete).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
 
        return true;
    }
 
    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home_description_select:
                onActionListener.onClickSelectAction();
                break;
            case R.id.menu_home_description_delete:
                onActionListener.onClickDeleteAction();
                break;
            default:
                break;
        }
        return false;
    }
 
 
    @Override
    public void onDestroyActionMode(ActionMode mode) {
        onActionListener.onDestroyAction();
    }

    public interface OnActionListener {
        void onDestroyAction();

        void onClickDeleteAction();

        void onClickSelectAction();
    }
}