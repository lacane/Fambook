package com.ourises.list.navigation.share.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.alapshin.genericrecyclerview.DefaultItemProvider;
import com.alapshin.genericrecyclerview.ItemProvider;
import com.alapshin.genericrecyclerview.ViewHolderDelegateManager;
import com.fambook.base.mvp.view.BaseMvpFragment;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.ourises.list.R;
import com.ourises.list.common.widget.Indexed;
import com.ourises.list.common.widget.recyclerView.DefaultAnimator;
import com.ourises.list.common.widget.util.InfoDialog;
import com.ourises.list.di.HasComponent;
import com.ourises.list.di.components.ApplicationComponent;
import com.ourises.list.di.components.ShareComponent;
import com.ourises.list.navigation.share.adapter.ShareAdapter;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItem;
import com.ourises.list.navigation.share.adapter.common.ShareCommonViewHolder;
import com.ourises.list.navigation.share.adapter.modelHeader.ShareHeaderDelegate;
import com.ourises.list.navigation.share.adapter.modelText.ShareTextDelegate;
import com.ourises.list.navigation.share.adapter.modelUser.ShareUserDelegate;
import com.ourises.list.navigation.share.adapter.modelUser.ShareUserItem;
import com.ourises.list.navigation.share.presenter.SharePresenter;

import java.util.List;

import butterknife.BindView;
import rx.Observable;

@FragmentWithArgs
public class ShareFragment extends BaseMvpFragment<ShareView, SharePresenter> implements ShareView {
    @BindView(R.id.share_recycler) RecyclerView recyclerView;
    @BindView(R.id.share_toolbar) Toolbar toolbar;

    @Arg String listId;

    private ShareAdapter adapter = new ShareAdapter();
    private LinearLayoutManager layoutManager;
    private ItemProvider<ShareCommonItem> itemProvider = new DefaultItemProvider<>();
    private ViewHolderDelegateManager<ShareCommonItem, ShareCommonViewHolder> delegateManager =
            new ViewHolderDelegateManager<>();

    private ShareComponent component;

    private OnHideListener onHideListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        boolean isTablet = getResources().getBoolean(R.bool.isTablet);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        getActivity().setTitle(getString(R.string.share_list));

        if (!isTablet) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_container);
        } else {
            toolbar.setNavigationIcon(R.drawable.ic_clear_container);
        }
        toolbar.setNavigationOnClickListener(v -> {
            close(false);
        });

        adapter.setDelegateManager(delegateManager);
        delegateManager.addDelegate(new ShareHeaderDelegate());
        delegateManager.addDelegate(new ShareTextDelegate());
        delegateManager.addDelegate(new ShareUserDelegate());

        itemProvider.setAdapter(adapter);
        adapter.setItemProvider(itemProvider);
        adapter.setOnShareItemListener((user, share) -> {
            Observable.from(itemProvider.getItems())
                    .zipWith(Observable.range(0, itemProvider.getItemCount()), Indexed::new)
                    .filter(indexed -> indexed.value.type() == ShareUserItem.USER_TYPE)
                    .filter(indexed -> user.id().equals(((ShareUserItem)indexed.value).user().id()))
                    .first()
                    .subscribe(indexed -> {
                        ShareUserItem userItem = ((ShareUserItem)indexed.value)
                                .toBuilder()
                                .shared(share)
                                .build();

                        itemProvider.setItem(indexed.index, userItem);

                        getPresenter().prepareSharingFriends(user, share);
                    });
        });

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultAnimator());

        getPresenter().loadData(getActivity(), listId);
    }

    @Override
    public void onLoadedItems(List<ShareCommonItem> items) {
        itemProvider.setItems(items);
    }

    @Override
    public void onError(Throwable throwable) {
        InfoDialog.stopProgress();
        InfoDialog.show(getActivity(), getString(R.string.error), getString(R.string.connecting_error));
        if (throwable != null) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void onCompleteSharing() {
        InfoDialog.stopProgress();
        close(true);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.share_fragment;
    }

    @Override
    protected void injectDependencies() {
        if (component == null) {
            component = ((HasComponent<ApplicationComponent>) getActivity().getApplication())
                    .getComponent().shareComponent();
        }
        component.inject(this);
    }

    private void close(boolean result) {
        if (onHideListener != null) {
            onHideListener.onHide(result);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_share, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_share_done:
                InfoDialog.showProgress(getActivity(), getString(R.string.sharing_list));
                getPresenter().share(listId);
                break;
            default:
                break;
        }
        return true;
    }

    public void setOnHideListener(OnHideListener onHideListener) {
        this.onHideListener = onHideListener;
    }

    public interface OnHideListener {
        void onHide(boolean okResult);
    }
}
