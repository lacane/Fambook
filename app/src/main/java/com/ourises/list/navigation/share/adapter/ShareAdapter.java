package com.ourises.list.navigation.share.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.alapshin.genericrecyclerview.ItemProvider;
import com.alapshin.genericrecyclerview.ViewHolderDelegateManager;
import com.ourises.list.common.entities.User;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItem;
import com.ourises.list.navigation.share.adapter.common.ShareCommonViewHolder;

public class ShareAdapter extends RecyclerView.Adapter<ShareCommonViewHolder> {

    private ViewHolderDelegateManager<ShareCommonItem, ShareCommonViewHolder> delegateManager;
    private ItemProvider<ShareCommonItem> itemProvider;
    private OnShareItemListener onShareItemListener;

    @Override
    public ShareCommonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return delegateManager.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(ShareCommonViewHolder holder, int position) {
        holder.setShareItemListener(onShareItemListener);
        delegateManager.onBindViewHolder(holder, itemProvider.getItem(position));
    }

    @Override
    public int getItemViewType(int position) {
        return delegateManager.getItemViewType(itemProvider.getItem(position));
    }

    @Override
    public int getItemCount() {
        return itemProvider.getItemCount();
    }

    public void setDelegateManager(ViewHolderDelegateManager<ShareCommonItem, ShareCommonViewHolder> delegateManager) {
        this.delegateManager = delegateManager;
    }

    public void setItemProvider(ItemProvider<ShareCommonItem> itemProvider) {
        this.itemProvider = itemProvider;
    }

    public void setOnShareItemListener(OnShareItemListener onShareItemListener) {
        this.onShareItemListener = onShareItemListener;
    }

    public interface OnShareItemListener {
        void onShare(User user, boolean share);
    }
}
