package com.ourises.list.navigation.homeDescription.adapter.modelList;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ourises.list.R;
import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.common.widget.CircleCheckBox;
import com.ourises.list.common.widget.EditTextImeAction;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItemView;

import butterknife.BindView;

public class HomeDescriptionListItemView extends HomeDescriptionCommonItemView {

    @BindView(R.id.home_description_item_list_container) FrameLayout containerView;
    @BindView(R.id.home_description_item_list_icon) CircleCheckBox circleCheckBox;
    @BindView(R.id.home_description_item_list_text) EditTextImeAction editText;
    @BindView(R.id.home_description_item_list_text_click) TextView textViewClick;

    private boolean removed = false;
    private ListItemModel item;
    private CircleCheckBox.OnCheckedChangeListener checkedListener = (checkBox, isChecked) -> {
        circleCheckBox.postDelayed(() -> {
            onChangeItemContentListener.onClickOnCheckedItem(item.itemId(), isChecked);
        }, 150);
    };

    @Override
    protected int getLayoutRes() {
        return R.layout.home_description_item_list;
    }

    public HomeDescriptionListItemView(Context context) {
        super(context);

        editText.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL) {
                if (editText.getText().toString().isEmpty() && !removed) {
                    removed = true;
                    onChangeItemContentListener.onClickOnRemoveItem(item.itemId());
                }
            }
            return false;
        });

        editText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                onChangeItemContentListener.onClickOnUpdateItem(item.itemId(), editText.getText().toString());

                return false;
            }

            return true;
        });

        editText.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                textViewClick.setVisibility(VISIBLE);
                editText.setVisibility(INVISIBLE);
            }
        });

        textViewClick.setOnClickListener(v -> {
            textViewClick.setVisibility(INVISIBLE);
            editText.setVisibility(VISIBLE);
            onChangeItemContentListener.onClickOnItem(item.itemId(), editText);
        });

        textViewClick.setOnLongClickListener(v -> {
            onChangeItemContentListener.onClickOnSelectItem(item.itemId());
            return true;
        });
    }

    public void bind(ListItemModel item) {
        circleCheckBox.setOnCheckedChangeListener(null);
        this.item = item;
        removed = false;

        if (item.status() != null) {
            circleCheckBox.setChecked(item.status());
        }

        editText.setVisibility(INVISIBLE);
        textViewClick.setVisibility(VISIBLE);

        editText.setText(item.name());
        textViewClick.setText(item.name());

        circleCheckBox.setOnCheckedChangeListener(checkedListener);

        circleCheckBox.setVisibility(item.isEditMode() ? INVISIBLE : VISIBLE);
        setSelectedMode();
    }

    private void setSelectedMode() {
        containerView.setBackgroundColor(isSelected() ?
                ContextCompat.getColor(getContext(), R.color.gray_selected_item) : Color.TRANSPARENT);
    }
}
