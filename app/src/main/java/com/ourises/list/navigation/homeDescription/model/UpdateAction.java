package com.ourises.list.navigation.homeDescription.model;


public class UpdateAction {
    public enum ActionType {
        DELETED,
        ARCHIVED_STATE,
        UPDATE
    }

    private ActionType actionType;
    private String itemId;

    public UpdateAction(ActionType actionType, String itemId) {
        this.actionType = actionType;
        this.itemId = itemId;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public String getItemId() {
        return itemId;
    }
}
