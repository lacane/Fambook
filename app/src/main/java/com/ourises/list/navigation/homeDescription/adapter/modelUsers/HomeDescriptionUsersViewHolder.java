package com.ourises.list.navigation.homeDescription.adapter.modelUsers;

import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItemView;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonViewHolder;

public class HomeDescriptionUsersViewHolder extends HomeDescriptionCommonViewHolder {
    public HomeDescriptionUsersViewHolder(HomeDescriptionCommonItemView itemView) {
        super(itemView);
    }

    @Override
    public void onBindViewHolder(HomeDescriptionCommonItem item) {
        super.onBindViewHolder(item);

        HomeDescriptionUsersItem usersItem = (HomeDescriptionUsersItem) item;
        HomeDescriptionUsersItemView usersItemView = (HomeDescriptionUsersItemView) itemView;
        usersItemView.bind(usersItem);
    }
}
