package com.ourises.list.navigation.share.adapter.modelUser;

import com.ourises.list.navigation.share.adapter.common.ShareCommonItem;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItemView;
import com.ourises.list.navigation.share.adapter.common.ShareCommonViewHolder;

public class ShareUserViewHolder extends ShareCommonViewHolder {
    public ShareUserViewHolder(ShareCommonItemView itemView) {
        super(itemView);
    }

    @Override
    public void onBindViewHolder(ShareCommonItem item) {
        super.onBindViewHolder(item);

        ShareUserItem userItem = (ShareUserItem) item;
        ShareUserItemView userItemView = (ShareUserItemView) itemView;
        userItemView.onBind(userItem);
    }
}
