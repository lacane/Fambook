package com.ourises.list.navigation.share.adapter.modelHeader;

import android.content.Context;
import android.widget.TextView;

import com.ourises.list.R;
import com.ourises.list.common.widget.avatar.AvatarView;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItemView;

import butterknife.BindView;

public class ShareHeaderItemView extends ShareCommonItemView {

    @BindView(R.id.share_item_header_title) TextView titleView;
    @BindView(R.id.share_item_header_avatar) AvatarView avatarView;
    @BindView(R.id.share_item_header_name) TextView usernameView;

    public ShareHeaderItemView(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.share_item_header;
    }

    public void onBind(ShareHeaderItem item) {
        titleView.setText(item.listName());

        avatarView.setAvatar(item.creatorUser().photo());
        avatarView.setType(AvatarView.Type.CREATOR);
    }
}
