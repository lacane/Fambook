package com.ourises.list.navigation.auth.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_AuthTokenModel.Builder.class)
public abstract class AuthTokenModel {
    @JsonProperty("access_token")
    public abstract String accessToken();

    @JsonProperty("device")
    public abstract DeviceModel deviceModel();

    public static AuthTokenModel create(String accessToken, String fcmToken) {
        if (fcmToken == null) fcmToken = "";
        return builder()
                .accessToken(accessToken)
                .deviceModel(DeviceModel.create(fcmToken))
                .build();
    }

    public static Builder builder() {
        return new AutoValue_AuthTokenModel.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        @JsonProperty("access_token")
        public abstract Builder accessToken(String accessToken);

        @JsonProperty("device")
        public abstract Builder deviceModel(DeviceModel deviceModel);

        public abstract AuthTokenModel build();
    }
}
