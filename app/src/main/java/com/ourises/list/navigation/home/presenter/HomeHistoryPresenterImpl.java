package com.ourises.list.navigation.home.presenter;

import android.content.Context;

import com.alapshin.arctor.presenter.PresenterBundle;
import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.ourises.list.R;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.model.HomeListModel;
import com.ourises.list.common.model.NetworkModel;
import com.ourises.list.common.model.UserModel;
import com.ourises.list.common.widget.Indexed;
import com.ourises.list.navigation.home.view.HomeHistoryView;

import java.util.List;

import javax.annotation.Nullable;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class HomeHistoryPresenterImpl extends RxPresenter<HomeHistoryView> implements HomeHistoryPresenter {
    private final HomeListModel homeListModel;
    private final NetworkModel networkModel;
    private final UserModel userModel;

    private boolean historyMode = true;

    public HomeHistoryPresenterImpl(HomeListModel homeListModel, NetworkModel networkModel, UserModel userModel) {
        this.homeListModel = homeListModel;
        this.networkModel = networkModel;
        this.userModel = userModel;
    }

    @Override
    public void onCreate(@Nullable PresenterBundle bundle) {
        super.onCreate(bundle);
    }

    @Override
    public void checkForEditMode(List<ListModel> items) {
        int selectedCount = 0;
        for (int index = 0; index < items.size(); index++) {
            if (items.get(index).isSelected()) {
                selectedCount++;
            }
        }

        getView().setEditMode(selectedCount > 0, selectedCount);
    }

    @Override
    public void createListModel(Context context) {
        String placeholder = context.getString(R.string.list_create_placeholder);
        Subscription dataSubscription = userModel.getUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(user -> ListModel.create(placeholder))
                .subscribe(listModel -> {
                    getView().addItem(homeListModel.addListModel(listModel));
                    synchronizeItem(listModel);
                }, throwable -> getView().onError(throwable));
        addSubscription(dataSubscription);
    }

    @Override
    public void synchronizeItem(ListModel listModel) {
        Subscription dataSubscription = homeListModel.synchronizeListModel(listModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(newListModel -> {
                    getView().synchronizeListModel(newListModel, listModel.itemId());
                }, throwable -> getView().onError(throwable));
        addSubscription(dataSubscription);
    }

    @Override
    public void getListData(boolean historyMode) {
        this.historyMode = historyMode;
        homeListModel.setHistoryMode(historyMode);

        Subscription dataSubscription = networkModel.isOfflineMode()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(offlineMode -> {
                    if (offlineMode) return Observable.just(true);
                    else return homeListModel.synchronizeListModels();
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(offlineMode -> {
                    if (offlineMode) return homeListModel.getListModelsFromStorage();
                    else return homeListModel.getListModels();
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(Observable::from)
                .filter(listModel -> listModel.archived() == historyMode)
                .toList()
                .subscribe(listModels -> {
                    homeListModel.setListModels(listModels);
                    getView().onLoadedLists(listModels);
                }, throwable -> getView().onError(throwable));
        addSubscription(dataSubscription);
    }

    @Override
    public void prepareCallbacks() {
        networkModel.checkForNetwork();

        Subscription deleteSubscription = homeListModel.onDeleteModelSubscribe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(modelId -> getView().removeListModel(modelId.id()),
                        throwable -> getView().onError(throwable));
        addSubscription(deleteSubscription);

        Subscription updateSubscription = homeListModel.onUpdateModelSubscribe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listModel -> getView().onUpdateListModel(listModel.doSynchronize()),
                        throwable -> getView().onError(throwable));
        addSubscription(updateSubscription);

        Subscription userSubscription = userModel.getUserReadyObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> getListData(historyMode),
                        throwable -> getView().onError(throwable));
        addSubscription(userSubscription);

        Subscription updateSubscriber = homeListModel.onUpdateStorageListModelSubscribe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(homeListModel::getListModel)
                .subscribe(listModel -> getView().onUpdateListModel(listModel));
        addSubscription(updateSubscriber);
    }

    @Override
    public void removeListModel(ListModel item) {
        Subscription dataSubscription = homeListModel.removeListModel(item)
                .subscribe(o -> {}, throwable -> getView().onError(throwable));

        addSubscription(dataSubscription);
    }

    @Override
    public void unarchiveListModel(ListModel item) {
        Subscription dataSubscription = homeListModel.updateListModel(item.toBuilder()
                .archived(false).build())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {}, throwable -> getView().onError(throwable));

        addSubscription(dataSubscription);
    }

    @Override
    public void updateModel(List<ListModel> items, String itemId) {
        Observable<Integer> positionItemObservable = Observable.from(items)
                .zipWith(Observable.range(0, items.size()), Indexed::new)
                .filter(indexed -> itemId.equals(indexed.value.itemId()))
                .first()
                .flatMap(indexed -> Observable.just(indexed.index));

        Subscription dataSubscription = homeListModel.getListModel(itemId)
                .zipWith(positionItemObservable, Indexed::new)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(indexed -> {
                    getView().updateItem(indexed.index, indexed.value);
                }, throwable -> getView().onError(throwable));

        addSubscription(dataSubscription);
    }

    @Override
    public void updateListModels(List<ListModel> items) {
        homeListModel.setListModels(items);
    }
}
