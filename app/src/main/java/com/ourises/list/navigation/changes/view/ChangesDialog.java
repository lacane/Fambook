package com.ourises.list.navigation.changes.view;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.ourises.list.R;

public class ChangesDialog extends DialogFragment {

    private Toolbar toolbar;

    public static ChangesDialog newInstance(String itemId) {
        ChangesDialog changesDialog = new ChangesDialog();
        Bundle args = new Bundle();
        args.putString("itemId", itemId);
        changesDialog.setArguments(args);
        return changesDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.changes_fragment_dialog, container);
        toolbar = (Toolbar) view.findViewById(R.id.changes_dialog_toolbar);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String itemId = getArguments().getString("itemId");

        toolbar.setTitle(getString(R.string.changes));
        toolbar.setNavigationIcon(R.drawable.ic_clear_container);
        toolbar.setNavigationOnClickListener(v -> dismiss());

        if (itemId != null) {
            ChangesFragment fragment = new ChangesFragmentBuilder(itemId).build();

            getChildFragmentManager().beginTransaction()
                    .replace(R.id.changes_fragment_dialog_container, fragment)
                    .commit();
        } else {
            dismiss();
        }
    }
}
