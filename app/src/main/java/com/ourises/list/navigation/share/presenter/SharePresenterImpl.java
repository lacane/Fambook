package com.ourises.list.navigation.share.presenter;

import android.content.Context;

import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.ourises.list.R;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.entities.User;
import com.ourises.list.common.model.HomeListModel;
import com.ourises.list.common.model.NetworkModel;
import com.ourises.list.common.model.UserModel;
import com.ourises.list.common.widget.Indexed;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItem;
import com.ourises.list.navigation.share.adapter.modelHeader.ShareHeaderItem;
import com.ourises.list.navigation.share.adapter.modelText.ShareTextItem;
import com.ourises.list.navigation.share.adapter.modelUser.ShareUserItem;
import com.ourises.list.navigation.share.view.ShareView;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SharePresenterImpl extends RxPresenter<ShareView> implements SharePresenter {
    private final UserModel userModel;
    private final HomeListModel homeListModel;
    private final NetworkModel networkModel;

    private List<User> sharingList = new ArrayList<>();
    private ListModel currentListModel;

    public SharePresenterImpl(UserModel userModel, HomeListModel homeListModel, NetworkModel networkModel) {
        this.userModel = userModel;
        this.homeListModel = homeListModel;
        this.networkModel = networkModel;
    }

    @Override
    public void loadData(Context context, String itemId) {
        Subscription dataSubscription = homeListModel.getListModel(itemId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(listModel -> {
                    currentListModel = listModel;

                    List<ShareCommonItem> items = new ArrayList<>();
                    items.add(ShareHeaderItem.create(listModel));

                    if (listModel != null && listModel.users() != null && !listModel.users().isEmpty()) {
                        String text = String.format(context.getString(R.string.shared_already_text),
                                listModel.users().size());
                        items.add(ShareTextItem.create(text));
                        items.addAll(ShareUserItem.createAlreadyListUsers(listModel.users()));
                    }

                    return items;
                })
                .flatMap(items -> Observable.zip(Observable.just(items), userModel.getUser(),
                        (items1, user) -> {
                            if (user != null && user.friends() != null && !user.friends().isEmpty()) {
                                String text = context.getString(R.string.shared_other_text);
                                items1.add(ShareTextItem.create(text));
                                List<User> friends = user.friends();
                                friends = removeDuplicates(friends, currentListModel.users());
                                items1.addAll(ShareUserItem.createOtherListUsers(friends));
                            }

                            return items1;
                        }))
                .subscribe(items -> {
                    getView().onLoadedItems(items);
                });

        addSubscription(dataSubscription);
    }

    private List<User> removeDuplicates(List<User> friends, List<User> users) {
        List<User> result = new ArrayList<>();

        for (User friend : friends) {
            boolean concurrence = false;
            for (User user : users) {
                if (user.id().equals(friend.id())) {
                    concurrence = true;
                }
            }

            if (!concurrence) {
                result.add(friend);
            }
        }

        return result;
    }

    @Override
    public void prepareSharingFriends(User user, boolean share) {
        if (share) {
            sharingList.add(user);
        } else {
            Subscription dataSubscription = Observable.from(sharingList)
                    .zipWith(Observable.range(0, sharingList.size()), Indexed::new)
                    .filter(indexed -> user.id().equals(indexed.value.id()))
                    .first()
                    .subscribe(indexed -> {
                        sharingList.remove(indexed.index);
                    });
            addSubscription(dataSubscription);
        }
    }

    @Override
    public void share(String listId) {
        if (sharingList.size() != 0) {
            Subscription dataSubscription = Observable.from(sharingList)
                    .flatMap(user -> homeListModel.shareToFriend(user.id(), listId))
                    .doOnCompleted(this::completeSharing)
                    .subscribe(o -> {}, throwable -> getView().onError(throwable));
            addSubscription(dataSubscription);
        } else {
            getView().onCompleteSharing();
        }
    }

    private void completeSharing() {
        sharingList.addAll(currentListModel.users());

        ListModel listModel = currentListModel.toBuilder().users(sharingList).build();
        Subscription dataSubscription = homeListModel.updateListModel(listModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(o -> getView().onCompleteSharing(), throwable -> getView().onError(throwable));

        addSubscription(dataSubscription);
    }
}
