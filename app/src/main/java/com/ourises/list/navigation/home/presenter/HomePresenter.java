package com.ourises.list.navigation.home.presenter;

import android.content.Context;

import com.alapshin.arctor.presenter.Presenter;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.navigation.home.view.HomeView;

import java.util.List;

public interface HomePresenter extends Presenter<HomeView> {
    void createListModel(Context context);

    void removeListModel(ListModel item);

    void archiveListModel(ListModel item);

    void updateModel(List<ListModel> items, String itemId);

    void updateListModels(List<ListModel> items);

    void checkForEditMode(List<ListModel> items);

    void synchronizeItem(ListModel listModel);

    void getListData(boolean historyMode);

    void prepareCallbacks();
}
