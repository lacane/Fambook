package com.ourises.list.navigation.changes.adapter;

import com.alapshin.genericrecyclerview.DefaultViewHolder;
import com.ourises.list.navigation.changes.model.ChangesModel;

public class ChangesViewHolder extends DefaultViewHolder<ChangesModel, ChangesItemView> {
    public ChangesViewHolder(ChangesItemView itemView) {
        super(itemView);
    }

    @Override
    public void onBindViewHolder(ChangesModel item) {
        super.onBindViewHolder(item);
        ChangesItemView changesItemView = (ChangesItemView) itemView;
        changesItemView.onBind(item);
    }
}
