package com.ourises.list.navigation.homeDescription.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.ourises.list.R;
import com.ourises.list.navigation.changes.view.ChangesActivity;
import com.ourises.list.navigation.main.model.ToolbarProvider;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeDescriptionActivity extends AppCompatActivity implements ToolbarProvider {

    public static final String EXTRA_LIST_MODEL = "listModel";
    private String itemId;

    @BindView(R.id.description_toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_item_description_activity);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        setTitle(getString(R.string.list));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_container);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        if (getIntent() == null) {
            finish();
        } else {
            itemId = getIntent().getStringExtra(EXTRA_LIST_MODEL);
            HomeDescriptionFragment fragment = new HomeDescriptionFragmentBuilder(itemId).build();
            fragment.setChangesListListener(newItemId -> {
                Intent intent = new Intent(this, ChangesActivity.class);
                intent.putExtra(ChangesActivity.EXTRA_CHANGES_MODEL, newItemId);
                startActivity(intent);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
                }
            });
            getSupportFragmentManager().beginTransaction().replace(R.id.description_container, fragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        Intent data = new Intent();
        data.putExtra(EXTRA_LIST_MODEL, itemId);
        setResult(Activity.RESULT_OK, data);

        super.onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }
}
