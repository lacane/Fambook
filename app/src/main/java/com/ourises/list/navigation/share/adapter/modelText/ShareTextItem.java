package com.ourises.list.navigation.share.adapter.modelText;

import com.google.auto.value.AutoValue;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItem;

@AutoValue
public abstract class ShareTextItem implements ShareCommonItem {
    public static final int TEXT_TYPE = 1;

    @Override
    public int id() {
        return 0;
    }

    @Override
    public int type() {
        return TEXT_TYPE;
    }

    public abstract String text();

    public static ShareTextItem create(String text) {
        return new AutoValue_ShareTextItem.Builder().text(text).build();
    }
    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder text(String text);

        public abstract ShareTextItem build();
    }
}
