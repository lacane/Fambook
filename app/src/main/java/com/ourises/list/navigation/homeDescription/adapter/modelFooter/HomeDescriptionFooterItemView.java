package com.ourises.list.navigation.homeDescription.adapter.modelFooter;

import android.content.Context;

import com.ourises.list.R;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItemView;

public class HomeDescriptionFooterItemView extends HomeDescriptionCommonItemView {
    public HomeDescriptionFooterItemView(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.home_description_item_footer;
    }
}
