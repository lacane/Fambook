package com.ourises.list.navigation.home.presenter;

import android.content.Context;

import com.alapshin.arctor.presenter.Presenter;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.navigation.home.view.HomeHistoryView;

import java.util.List;

public interface HomeHistoryPresenter extends Presenter<HomeHistoryView> {
    void createListModel(Context context);

    void removeListModel(ListModel item);

    void unarchiveListModel(ListModel item);

    void updateModel(List<ListModel> items, String itemId);

    void updateListModels(List<ListModel> items);

    void checkForEditMode(List<ListModel> items);

    void synchronizeItem(ListModel listModel);

    void getListData(boolean historyMode);

    void prepareCallbacks();
}
