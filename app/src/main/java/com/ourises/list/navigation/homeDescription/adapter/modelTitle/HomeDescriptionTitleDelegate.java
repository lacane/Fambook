package com.ourises.list.navigation.homeDescription.adapter.modelTitle;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.alapshin.genericrecyclerview.ViewHolderDelegate;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonViewHolder;

public class HomeDescriptionTitleDelegate
        implements ViewHolderDelegate<HomeDescriptionCommonItem, HomeDescriptionCommonViewHolder> {
    @Override
    public boolean isForViewType(@NonNull HomeDescriptionCommonItem homeDescriptionCommonItem) {
        return homeDescriptionCommonItem.type() == HomeDescriptionTitleItem.TITLE_MODEL_TYPE;
    }

    @NonNull
    @Override
    public HomeDescriptionCommonViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        return new HomeDescriptionTitleViewHolder(new HomeDescriptionTitleItemView(viewGroup.getContext()));
    }

    @Override
    public void onBindViewHolder(@NonNull HomeDescriptionCommonViewHolder holder, HomeDescriptionCommonItem item) {
        holder.onBindViewHolder(item);
    }

    @Override
    public Class<HomeDescriptionCommonViewHolder> getViewHolderType() {
        return HomeDescriptionCommonViewHolder.class;
    }
}
