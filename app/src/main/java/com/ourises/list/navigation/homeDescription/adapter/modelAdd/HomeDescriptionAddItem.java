package com.ourises.list.navigation.homeDescription.adapter.modelAdd;

import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;

public class HomeDescriptionAddItem implements HomeDescriptionCommonItem {
    public static final int ADD_MODEL_TYPE = 1;
    @Override
    public int id() {
        return 0;
    }

    @Override
    public int type() {
        return ADD_MODEL_TYPE;
    }
}
