package com.ourises.list.navigation.settings.presenter;

import android.content.Context;

import com.alapshin.arctor.presenter.Presenter;
import com.ourises.list.navigation.settings.view.SettingsView;

public interface SettingsPresenter extends Presenter<SettingsView> {
    void updateNotificationState(boolean checked);

    void logout(Context context);
}
