package com.ourises.list.navigation.auth.presenter;


import android.content.Context;
import android.net.Uri;

import com.alapshin.arctor.presenter.Presenter;
import com.ourises.list.navigation.auth.view.AuthFacebookView;

public interface AuthFacebookPresenter extends Presenter<AuthFacebookView> {
    void login(String token);

    void loginWithCode(Context context, String clientId, String redirectUri, String clientSecret, Uri url);
}
