package com.ourises.list.navigation.changes.view;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.ourises.list.R;
import com.ourises.list.navigation.main.model.ToolbarProvider;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangesActivity extends AppCompatActivity implements ToolbarProvider {
    public static final String EXTRA_CHANGES_MODEL = "listModel";
    @BindView(R.id.changes_toolbar) Toolbar toolbar;

    private String itemId;

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changes_activity);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        setTitle(getString(R.string.changes));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_container);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        if (getIntent() == null) {
            finish();
        } else {
            itemId = getIntent().getStringExtra(EXTRA_CHANGES_MODEL);
            ChangesFragment fragment = new ChangesFragmentBuilder(itemId).build();
            getSupportFragmentManager().beginTransaction().replace(R.id.changes_container, fragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
        }
    }
}
