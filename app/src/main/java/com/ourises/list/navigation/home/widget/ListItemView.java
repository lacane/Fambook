package com.ourises.list.navigation.home.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ourises.list.R;
import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.widget.util.DateUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListItemView extends RelativeLayout {

    @BindView(R.id.home_item_root) ViewGroup rootView;
    @BindView(R.id.home_item_click_view) View clickableVIew;
    @BindView(R.id.home_item_title) TextView titleView;
    @BindView(R.id.home_item_users) UsersView usersView;

    @BindView(R.id.home_item_date_text) TextView dateView;

    @BindView(R.id.home_item_progress) ListItemProgressView progressView;

    private ListModel item;
    private boolean isTablet;

    public ListItemView(Context context) {
        super(context);
        LayoutInflater.from(getContext()).inflate(R.layout.home_item_view, this);
        ButterKnife.bind(this);
        isTablet = getResources().getBoolean(R.bool.isTablet);
    }

    public void bind(ListModel item) {
        this.item = item;

        titleView.setText(item.name());
        usersView.setUsers(item.getUsers(), item.createdUserId());
        dateView.setText(DateUtil.getDateForList(item.createDate()));

        int completedCount = 0;
        for (ListItemModel itemModel : item.getItems()) {
            if (itemModel.status()) {
                completedCount++;
            }
        }
        progressView.setTotalCount(item.getItems().size());
        progressView.setCompletedCount(completedCount);

        setActivated(isTablet && item.isClicked());
    }

    public void setListener(HomeAdapter.OnSelectItemListener listener) {
        clickableVIew.setOnClickListener(v -> {
            if (!item.isEditMode()) {
                setActivated(isTablet);
                listener.onClickItem(item);
            } else {
                listener.onSelectItem(item);
            }
        });
        clickableVIew.setOnLongClickListener(v -> {
            listener.onSelectItem(item);
            return true;
        });
    }
}
