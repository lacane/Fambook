package com.ourises.list.navigation.auth.view;

import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.fambook.base.mvp.view.BaseMvpActivity;
import com.ourises.list.R;
import com.ourises.list.di.HasComponent;
import com.ourises.list.di.components.ApplicationComponent;
import com.ourises.list.di.components.LoginComponent;
import com.ourises.list.navigation.auth.presenter.AuthFacebookPresenter;

public class AuthFacebookActivity extends BaseMvpActivity<AuthFacebookView, AuthFacebookPresenter>
        implements AuthFacebookView {

    private LoginComponent component;

    private String clientSecret;
    private String clientId;
    private String redirectUri;

    private WebView webView;
    private WebViewClient webViewClient = new WebViewClient() {
        @Override
        public void onPageFinished(WebView view, String url) {
            if (url.startsWith(redirectUri)) {
                Log.d("AuthFacebookActivity", "facebook getUser success");

                getPresenter().loginWithCode(AuthFacebookActivity.this,
                        clientId,
                        redirectUri,
                        clientSecret,
                        Uri.parse(url));
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean isTablet = getResources().getBoolean(R.bool.isTablet);

        if (isTablet) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        webView = (WebView) findViewById(R.id.auth_web_view);

        clientSecret = getString(R.string.facebook_app_secret);
        clientId = getString(R.string.facebook_app_id);
        redirectUri = getString(R.string.facebook_app_redirect);

        String url = "https://www.facebook.com/v2.9/dialog/oauth?"
                + "scope=email,public_profile,user_friends"
                + "&client_id=" + clientId
                + "&redirect_uri=" + redirectUri;

        webView.loadUrl(url);
        webView.setWebViewClient(webViewClient);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.auth_facebook_activity;
    }

    @Override
    protected void injectDependencies() {
        if (component == null) {
            component = ((HasComponent<ApplicationComponent>) getApplication()).getComponent().loginComponent();
        }

        component.inject(this);
    }

    @Override
    public void onError(Throwable throwable) {
        if (throwable != null) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void successLoadingToken() {
        setResult(RESULT_OK);
        finish();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
        }
    }
}
