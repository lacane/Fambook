package com.ourises.list.navigation.home.view;

import com.alapshin.arctor.view.MvpView;
import com.ourises.list.common.entities.ListModel;

import java.util.List;

public interface HomeHistoryView extends MvpView {
    void onLoadedLists(List<ListModel> models);

    void onError(Throwable throwable);

    void onUpdateListModel(ListModel listModel);

    void addItem(ListModel listModel);

    void synchronizeListModel(ListModel newListModel, String oldItemId);

    void updateItem(int index, ListModel listModel);

    void setEditMode(boolean editMode, int selectedCount);

    void removeListModel(String itemId);
}
