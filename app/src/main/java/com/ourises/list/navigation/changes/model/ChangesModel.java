package com.ourises.list.navigation.changes.model;

import android.support.annotation.Nullable;

import com.alapshin.genericrecyclerview.Item;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_ChangesModel.Builder.class)
public abstract class ChangesModel implements Item {
    public static final int CHANGES_MODEL_TYPE = 0;

    @Override
    public int type() {
        return CHANGES_MODEL_TYPE;
    }

    @Override
    public int id() {
        return 0;
    }

    @Nullable
    @JsonProperty("action")
    public abstract ChangesAction action();

    @Nullable
    @JsonProperty("user_photo")
    public abstract String userPhoto();

    @Nullable
    @JsonProperty("updated_at")
    public abstract String updatedAt();

    @Nullable
    @JsonProperty("item_name")
    public abstract String itemName();

    @AutoValue.Builder
    public abstract static class Builder {
        @JsonProperty("action")
        public abstract Builder action(ChangesAction action);

        @JsonProperty("user_photo")
        public abstract Builder userPhoto(String userPhoto);

        @JsonProperty("updated_at")
        public abstract Builder updatedAt(String updatedAt);

        @JsonProperty("item_name")
        public abstract Builder itemName(String itemName);

        public abstract ChangesModel build();
    }
}
