package com.ourises.list.navigation.settings.view;

import com.alapshin.arctor.view.MvpView;

public interface SettingsView extends MvpView {
    void setNotificationState(boolean state);

    void onLogoutComplete();
}
