package com.ourises.list.navigation.homeDescription.model;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class SynchronizedListModel {

    public abstract String oldModelId();

    public abstract String newModelId();

    public static SynchronizedListModel create(String oldModelId, String newModelId) {
        return new AutoValue_SynchronizedListModel.Builder()
                .newModelId(newModelId)
                .oldModelId(oldModelId)
                .build();
    }

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder oldModelId(String oldModelId);

        public abstract Builder newModelId(String newModelId);

        public abstract SynchronizedListModel build();
    }
}
