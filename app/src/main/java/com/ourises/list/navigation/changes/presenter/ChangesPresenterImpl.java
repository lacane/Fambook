package com.ourises.list.navigation.changes.presenter;

import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.model.HomeListModel;
import com.ourises.list.navigation.changes.view.ChangesView;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ChangesPresenterImpl extends RxPresenter<ChangesView> implements ChangesPresenter {
    private final HomeListModel homeListModel;

    private ListModel currentListModel;
    public ChangesPresenterImpl(HomeListModel homeListModel) {
        this.homeListModel = homeListModel;
    }

    @Override
    public void getListModel(String itemId) {
        Subscription dataSubscription = homeListModel.getListModel(itemId)
                .compose(deliverLatest())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listModel -> {
                    currentListModel = listModel;
                    getView().onLoadedListModel(listModel);
                }, throwable -> getView().onError(throwable));
        addSubscription(dataSubscription);
    }

    @Override
    public void getChanges(String itemId) {
        Subscription dataSubscription = homeListModel.getChanges(itemId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(changesModels -> {
                    getView().onLoadedChanges(changesModels);
                    currentListModel = currentListModel.toBuilder().changes(changesModels).build();
                    homeListModel.updateListModelInStorage(currentListModel);
                }, throwable -> getView().onError(throwable));
        addSubscription(dataSubscription);
    }

    @Override
    public void createCallback() {
        Subscription dataSubscription = homeListModel.onUpdateModelSubscribe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(listModel -> listModel.itemId().equals(currentListModel.itemId()))
                .subscribe(listModel -> {
                    currentListModel = listModel;
                    getView().onLoadedListModel(listModel);
                });
        addSubscription(dataSubscription);
    }
}
