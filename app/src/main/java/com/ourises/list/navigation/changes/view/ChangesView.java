package com.ourises.list.navigation.changes.view;

import com.alapshin.arctor.view.MvpView;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.navigation.changes.model.ChangesModel;

import java.util.List;

public interface ChangesView extends MvpView {
    void onLoadedListModel(ListModel listModel);

    void onLoadedChanges(List<ChangesModel> changesModels);

    void onError(Throwable throwable);
}
