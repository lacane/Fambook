package com.ourises.list.navigation.homeDescription.adapter.modelAdd;

import android.content.Context;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.ourises.list.R;
import com.ourises.list.common.widget.EditTextImeAction;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItemView;
import com.ourises.list.common.widget.TintableImageView;

import butterknife.BindView;

public class HomeDescriptionAddItemView extends HomeDescriptionCommonItemView {

    @BindView(R.id.home_description_item_add_text_click) TextView textViewClick;
    @BindView(R.id.home_description_item_add_text) EditTextImeAction editText;
    @BindView(R.id.home_description_item_add_icon) TintableImageView iconView;

    public HomeDescriptionAddItemView(Context context) {
        super(context);

        editText.setVisibility(INVISIBLE);
        textViewClick.setVisibility(VISIBLE);

        editText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                onChangeItemContentListener.onClickOnAddItem(editText.getText().toString());
                editText.setText("");
            }

            return true;
        });

        editText.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                textViewClick.setVisibility(VISIBLE);
                editText.setVisibility(INVISIBLE);
            }
        });

        textViewClick.setOnClickListener(v -> {
            textViewClick.setVisibility(INVISIBLE);
            editText.setVisibility(VISIBLE);
            onChangeItemContentListener.onClickOnItem(null, editText);
        });
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.home_description_item_add;
    }
}
