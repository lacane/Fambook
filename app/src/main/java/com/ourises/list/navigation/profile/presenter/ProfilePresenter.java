package com.ourises.list.navigation.profile.presenter;

import com.alapshin.arctor.presenter.Presenter;
import com.ourises.list.navigation.profile.view.ProfileView;

public interface ProfilePresenter extends Presenter<ProfileView> {
    void getData();
}
