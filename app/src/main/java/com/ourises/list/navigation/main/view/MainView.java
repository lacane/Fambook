package com.ourises.list.navigation.main.view;

import android.support.v4.app.Fragment;

import com.alapshin.arctor.view.MvpView;
import com.ourises.list.common.entities.User;

public interface MainView extends MvpView {
    void setUser(User user);

    void onError(Throwable throwable);

    void offlineMode(boolean offlineMode);

    void startDescription(Fragment fragment, String itemId, int requestCode);

    void connectedError();
}
