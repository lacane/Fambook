package com.ourises.list.navigation.changes.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_ChangesModelForRequest.Builder.class)
public abstract class ChangesModelForRequest {
    @JsonProperty("id")
    public abstract Integer id();

    public static ChangesModelForRequest create(String itemId) {
        return new AutoValue_ChangesModelForRequest.Builder().id(Integer.parseInt(itemId)).build();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        @JsonProperty("id")
        public abstract Builder id(Integer id);

        public abstract ChangesModelForRequest build();
    }
}
