package com.ourises.list.navigation.share.adapter.modelHeader;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.alapshin.genericrecyclerview.ViewHolderDelegate;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItem;
import com.ourises.list.navigation.share.adapter.common.ShareCommonViewHolder;

public class ShareHeaderDelegate implements ViewHolderDelegate<ShareCommonItem, ShareCommonViewHolder> {
    @Override
    public boolean isForViewType(@NonNull ShareCommonItem item) {
        return item.type() == ShareHeaderItem.HEADER_TYPE;
    }

    @NonNull
    @Override
    public ShareCommonViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        return new ShareHeaderViewHolder(new ShareHeaderItemView(viewGroup.getContext()));
    }

    @Override
    public void onBindViewHolder(@NonNull ShareCommonViewHolder holder, ShareCommonItem item) {
        holder.onBindViewHolder(item);
    }

    @Override
    public Class<ShareCommonViewHolder> getViewHolderType() {
        return ShareCommonViewHolder.class;
    }
}
