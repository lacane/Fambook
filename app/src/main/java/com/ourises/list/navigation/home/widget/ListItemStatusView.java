package com.ourises.list.navigation.home.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ourises.list.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListItemStatusView extends RelativeLayout {

    @BindView(R.id.home_item_status_view_count) TextView countView;
    @BindView(R.id.home_item_status_view_name) TextView nameView;
    @BindView(R.id.home_item_status_view_line) View lineView;

    private float countTextSize;
    private float textSize;

    public enum StatusType {
        OVERDUE,
        COMPLETED
    }

    public ListItemStatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ListItemStatusView);
        countTextSize = typedArray.getDimensionPixelSize(R.styleable.ListItemStatusView_statusTextSizeCount, -1);
        textSize = typedArray.getDimensionPixelSize(R.styleable.ListItemStatusView_statusTextSize, -1);
        typedArray.recycle();

        LayoutInflater.from(context).inflate(R.layout.home_item_status_view, this);
        ButterKnife.bind(this);

        if (countTextSize != -1) {
            countView.setTextSize(TypedValue.COMPLEX_UNIT_PX, countTextSize);
        }
        if (textSize != -1) {
            nameView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        }
    }

    public void setStatusType(StatusType statusType) {
        switch (statusType) {
            case COMPLETED:
                nameView.setText(R.string.home_completed);
                lineView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.green));
                break;
            case OVERDUE:
            default:
                nameView.setText(R.string.home_overdue);
                lineView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                break;
        }
    }

    public void setTitle(String title) {
        nameView.setText(title);
    }

    public void setCount(int count) {
        countView.setText(String.valueOf(count));
    }
}
