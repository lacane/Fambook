package com.ourises.list.navigation.test;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ourises.list.R;
import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.entities.User;
import com.orhanobut.hawk.Hawk;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TestActivity extends AppCompatActivity {
    private final static String LIST = "list";

    @BindView(R.id.get_text)
    TextView getText;

    List<ListModel> listModels = new ArrayList<>();

    ObjectMapper mapper = new ObjectMapper();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity);
        ButterKnife.bind(this);

        listModels.add(ListModel.builder().name("one")
                .createDate(UUID.randomUUID().toString())
                .users(Arrays.asList(
                        User.builder()
                                .id(UUID.randomUUID().toString())
                                .name("one name")
                                .build(),
                        User.builder()
                                .id(UUID.randomUUID().toString())
                                .name("two name")
                                .build()
                ))
                .items(Arrays.asList(
                        ListItemModel.builder()
                                .itemId(UUID.randomUUID().toString())
                                .parentId(UUID.randomUUID().toString())
                                .name("one item")
                                .status(false)
                                .build(),
                        ListItemModel.builder()
                                .itemId(UUID.randomUUID().toString())
                                .parentId(UUID.randomUUID().toString())
                                .name("two item")
                                .status(false)
                                .build()
                ))
                .itemId(UUID.randomUUID().toString())
                .build()
        );
        listModels.add(ListModel.builder().name("two")
                .createDate(UUID.randomUUID().toString())
                .users(Arrays.asList(
                        User.builder()
                                .id(UUID.randomUUID().toString())
                                .name("one name")
                                .build(),
                        User.builder()
                                .id(UUID.randomUUID().toString())
                                .name("two name")
                                .build()
                ))
                .items(Arrays.asList(
                        ListItemModel.builder()
                                .itemId(UUID.randomUUID().toString())
                                .parentId(UUID.randomUUID().toString())
                                .name("one item")
                                .status(false)
                                .build(),
                        ListItemModel.builder()
                                .itemId(UUID.randomUUID().toString())
                                .parentId(UUID.randomUUID().toString())
                                .name("two item")
                                .status(false)
                                .build()
                ))
                .itemId(UUID.randomUUID().toString())
                .build()
        );
        listModels.add(ListModel.builder().name("three")
                .createDate(UUID.randomUUID().toString())
                .users(Arrays.asList(
                        User.builder()
                                .id(UUID.randomUUID().toString())
                                .name("one name")
                                .build(),
                        User.builder()
                                .id(UUID.randomUUID().toString())
                                .name("two name")
                                .build()
                ))
                .items(Arrays.asList(
                        ListItemModel.builder()
                                .itemId(UUID.randomUUID().toString())
                                .parentId(UUID.randomUUID().toString())
                                .name("one item")
                                .status(false)
                                .build(),
                        ListItemModel.builder()
                                .itemId(UUID.randomUUID().toString())
                                .parentId(UUID.randomUUID().toString())
                                .name("two item")
                                .status(false)
                                .build()
                ))
                .itemId(UUID.randomUUID().toString())
                .build()
        );
    }

    @OnClick(R.id.save_button)
    public void onSave(View view) {
        List<String> convertedItems = new ArrayList<>();

        for (ListModel model : listModels) {
            try {
                String item = mapper.writeValueAsString(model);
                convertedItems.add(item);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        Hawk.put(LIST, convertedItems);

        Toast.makeText(this, "Сохранено", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.get_button)
    public void onGet(View view) {
        List<String> newStringModels = Hawk.get(LIST);
        List<ListModel> newListModels = new ArrayList<>();

        if (newStringModels != null) {
            for (String model : newStringModels) {
                try {
                    ListModel listModel = mapper.readValue(model, ListModel.class);
                    newListModels.add(listModel);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            String text = "";
            for (ListModel model : newListModels) {
                text += model.name() + "\n";
            }
            getText.setText(text);
        } else {
            getText.setText("Данные не найдены");
        }
    }
}
