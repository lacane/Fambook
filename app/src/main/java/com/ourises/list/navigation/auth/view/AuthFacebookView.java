package com.ourises.list.navigation.auth.view;


import com.alapshin.arctor.view.MvpView;

public interface AuthFacebookView extends MvpView {
    void onError(Throwable throwable);

    void successLoadingToken();
}
