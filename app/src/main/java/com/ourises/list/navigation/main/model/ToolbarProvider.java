package com.ourises.list.navigation.main.model;

import android.support.v7.widget.Toolbar;

public interface ToolbarProvider {
    Toolbar getToolbar();
}
