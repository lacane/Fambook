package com.ourises.list.navigation.homeDescription.adapter.modelFooter;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.alapshin.genericrecyclerview.ViewHolderDelegate;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonViewHolder;

public class HomeDescriptionFooterDelegate
        implements ViewHolderDelegate<HomeDescriptionCommonItem, HomeDescriptionCommonViewHolder> {
    @Override
    public boolean isForViewType(@NonNull HomeDescriptionCommonItem item) {
        return item.type() == HomeDescriptionFooterItem.FOOTER_MODEL_TYPE;
    }

    @NonNull
    @Override
    public HomeDescriptionCommonViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        return new HomeDescriptionFooterViewHolder(new HomeDescriptionFooterItemView(viewGroup.getContext()));
    }

    @Override
    public void onBindViewHolder(@NonNull HomeDescriptionCommonViewHolder holder, HomeDescriptionCommonItem item) {
        holder.onBindViewHolder(item);
    }

    @Override
    public Class<HomeDescriptionCommonViewHolder> getViewHolderType() {
        return HomeDescriptionCommonViewHolder.class;
    }
}
