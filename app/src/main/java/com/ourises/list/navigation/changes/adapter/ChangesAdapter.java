package com.ourises.list.navigation.changes.adapter;

import com.alapshin.genericrecyclerview.DefaultAdapter;
import com.ourises.list.navigation.changes.model.ChangesModel;

public class ChangesAdapter extends DefaultAdapter<ChangesModel, ChangesViewHolder> {

    @Override
    public void onBindViewHolder(ChangesViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        holder.onBindViewHolder(itemProvider.getItem(position));
    }
}
