package com.ourises.list.navigation.home.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ourises.list.R;
import com.ourises.list.common.widget.TintableImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListItemProgressView extends RelativeLayout {
    private static final int DURATION = 400;

    @BindView(R.id.home_item_progress_progress) ProgressBar progressBar;
    @BindView(R.id.home_item_progress_completed) TextView completedCountView;
    @BindView(R.id.home_item_progress_total) TextView totalCountView;
    @BindView(R.id.home_item_progress_icon) TintableImageView doneIcon;

    private int totalCount = 0;
    private int completedCount = 0;

    public ListItemProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.home_item_progress_view, this);
        ButterKnife.bind(this);
    }

    public void setTotalCount(int total) {
        this.totalCount = total;
        totalCountView.setText(String.valueOf(total));
        progressBar.setMax(totalCount);
        checkForCompleted();
    }

    public void setCompletedCount(int completedCount) {
        this.completedCount = completedCount;
        completedCountView.setText(String.valueOf(completedCount));
        progressBar.setProgress(completedCount);
        checkForCompleted();
    }

    private void checkForCompleted() {
        if (totalCount != 0 && totalCount == completedCount) {
            doneIcon.setVisibility(VISIBLE);
            completedCountView.setVisibility(INVISIBLE);
            totalCountView.setVisibility(INVISIBLE);
        } else {
            doneIcon.setVisibility(INVISIBLE);
            completedCountView.setVisibility(VISIBLE);
            totalCountView.setVisibility(VISIBLE);
        }
    }
}
