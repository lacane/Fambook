package com.ourises.list.navigation.homeDescription.view;

import com.alapshin.arctor.view.MvpView;
import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.entities.ResponseModelId;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;

import java.util.List;

public interface HomeDescriptionView extends MvpView {
    void onLoadedListModel(ListModel listModel);

    void onError(Throwable throwable);

    void onLoadedListItems(List<HomeDescriptionCommonItem> items);

    void updateCheckedItem(int position, ListItemModel itemModel, boolean checked);

    void setEditMode(boolean editMode, int selectedCount);

    void addItem(int index, ListItemModel itemModel);

    void synchronizedItem(String itemId, ListItemModel itemModel);

    void onDeleteListModel();

    void close(boolean archived);

    void setStatusModel(int overdueSize, int completedSize);

    void checkForArchived(Boolean archived);

    void updateListModel(String newListModelId);
}
