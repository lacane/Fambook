package com.ourises.list.navigation.share.presenter;

import android.content.Context;

import com.alapshin.arctor.presenter.Presenter;
import com.ourises.list.common.entities.User;
import com.ourises.list.navigation.share.view.ShareView;

public interface SharePresenter extends Presenter<ShareView> {
    void loadData(Context context, String itemId);

    void prepareSharingFriends(User user, boolean share);

    void share(String listId);
}
