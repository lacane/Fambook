package com.ourises.list.navigation.homeDescription.adapter.modelUsers;

import android.content.Context;

import com.ourises.list.R;
import com.ourises.list.common.widget.TintableImageView;
import com.ourises.list.navigation.home.widget.UsersView;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItemView;

import butterknife.BindView;

public class HomeDescriptionUsersItemView extends HomeDescriptionCommonItemView {

    @BindView(R.id.home_description_item_users_view) UsersView usersView;
    @BindView(R.id.home_description_item_changes_button) TintableImageView changesButton;

    public HomeDescriptionUsersItemView(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.home_description_item_users;
    }

    public void bind(HomeDescriptionUsersItem usersItem) {
        usersView.withShareButton();
        usersView.setShareClickListener(v -> {
            onChangeItemContentListener.onClickShare();
        });
        usersView.setUsers(usersItem.getUsers(), usersItem.creatorUserId());
        changesButton.setOnClickListener(v -> onChangeItemContentListener.onClickOnChanges());
    }
}
