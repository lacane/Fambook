package com.ourises.list.navigation.share.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_ShareItemModel.Builder.class)
public abstract class ShareItemModel {
    @JsonProperty("note_id")
    public abstract String listId();

    @JsonProperty("user_id")
    public abstract String userId();

    public static ShareItemModel create(String listId, String userId) {
        return new AutoValue_ShareItemModel.Builder().listId(listId).userId(userId).build();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        @JsonProperty("note_id")
        public abstract Builder listId(String listId);

        @JsonProperty("user_id")
        public abstract Builder userId(String userId);

        public abstract ShareItemModel build();
    }
}
