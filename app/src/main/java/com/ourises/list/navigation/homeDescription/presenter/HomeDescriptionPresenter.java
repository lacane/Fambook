package com.ourises.list.navigation.homeDescription.presenter;

import com.alapshin.arctor.presenter.Presenter;
import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.common.widget.Indexed;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;
import com.ourises.list.navigation.homeDescription.view.HomeDescriptionView;

import java.util.List;

import rx.Observable;

public interface HomeDescriptionPresenter extends Presenter<HomeDescriptionView> {
    void getListModel(String itemId);

    void addListItemModel(List<HomeDescriptionCommonItem> items, String name);

    void synchronizeItem(ListItemModel itemModel);

    void checkItem(List<HomeDescriptionCommonItem> items, String itemId, boolean isChecked);

    void checkForEditMode(List<HomeDescriptionCommonItem> items);

    void updateItemsInStorage(List<HomeDescriptionCommonItem> items);

    void removeItemModel(HomeDescriptionCommonItem item);

    void updateItemModel(ListItemModel itemModel);

    void updateNameOfList(String nameOfList);

    Observable<Indexed<HomeDescriptionCommonItem>> sortedItems(List<HomeDescriptionCommonItem> items, int type);

    void checkStatusModel(List<HomeDescriptionCommonItem> items);

    void changeArchiveState();
}
