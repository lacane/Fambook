package com.ourises.list.navigation.homeDescription.adapter.modelFooter;

import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItemView;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonViewHolder;

public class HomeDescriptionFooterViewHolder extends HomeDescriptionCommonViewHolder {
    public HomeDescriptionFooterViewHolder(HomeDescriptionCommonItemView itemView) {
        super(itemView);
    }
}
