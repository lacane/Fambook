package com.ourises.list.navigation.share.adapter.modelUser;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.ourises.list.R;
import com.ourises.list.common.widget.TintableImageView;
import com.ourises.list.common.widget.avatar.AvatarView;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItemView;

import butterknife.BindView;

public class ShareUserItemView extends ShareCommonItemView {

    @BindView(R.id.share_item_user_avatar) AvatarView avatarView;
    @BindView(R.id.share_item_user_name) TextView usernameView;
    @BindView(R.id.share_item_user_check_box) TintableImageView checkBox;

    public ShareUserItemView(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.share_item_user;
    }

    public void onBind(ShareUserItem userItem) {
        checkBox.setOnClickListener(v -> {
            if (userItem.canChangeState()) {
                shareItemListener.onShare(userItem.user(), !userItem.shared());
                setCheckBoxState(!userItem.shared());
            }
        });
        avatarView.setType(AvatarView.Type.MEMBER);
        avatarView.setAvatar(userItem.user().photo());

        usernameView.setText(userItem.user().name());

        setCheckBoxState(userItem.shared());
    }

    private void setCheckBoxState(boolean state) {
        if (state) {
            checkBox.setSupportImageTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.green)));
        } else {
            checkBox.setSupportImageTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.gray_90)));
        }
    }
}
