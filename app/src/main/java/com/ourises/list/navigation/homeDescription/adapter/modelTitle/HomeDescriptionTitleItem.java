package com.ourises.list.navigation.homeDescription.adapter.modelTitle;

import com.google.auto.value.AutoValue;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;

@AutoValue
public abstract class HomeDescriptionTitleItem implements HomeDescriptionCommonItem  {
    static final int TITLE_MODEL_TYPE = 5;

    @Override
    public int id() {
        return 0;
    }

    @Override
    public int type() {
        return TITLE_MODEL_TYPE;
    }

    public abstract String title();

    public static HomeDescriptionTitleItem create(String title) {
        return new AutoValue_HomeDescriptionTitleItem.Builder().title(title).build();
    }

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder title(String title);

        public abstract HomeDescriptionTitleItem build();
    }
}
