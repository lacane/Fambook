package com.ourises.list.navigation.profile.presenter;

import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.ourises.list.common.model.UserModel;
import com.ourises.list.navigation.profile.view.ProfileView;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ProfilePresenterImpl extends RxPresenter<ProfileView> implements ProfilePresenter {
    private final UserModel userModel;

    public ProfilePresenterImpl(UserModel userModel) {
        this.userModel = userModel;
    }

    @Override
    public void getData() {
        getUser();
        getStatus();
    }

    private void getUser() {
        Subscription dataSubscription = userModel.getUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    getView().onLoadedUser(user);
                }, throwable -> getView().onError(throwable));

        addSubscription(dataSubscription);
    }

    private void getStatus() {
        Subscription dataSubscription = userModel.getListStatus()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(profileStatus -> {
                    getView().onStatusLoaded(profileStatus);
                }, throwable -> getView().onError(throwable));
        addSubscription(dataSubscription);
    }
}
