package com.ourises.list.navigation.profile.view;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.fambook.base.mvp.view.BaseMvpActivity;
import com.ourises.list.R;
import com.ourises.list.common.entities.User;
import com.ourises.list.common.widget.avatar.AvatarView;
import com.ourises.list.di.HasComponent;
import com.ourises.list.di.components.ApplicationComponent;
import com.ourises.list.di.components.ProfileComponent;
import com.ourises.list.navigation.home.widget.ListItemStatusView;
import com.ourises.list.navigation.profile.model.ProfileStatus;
import com.ourises.list.navigation.profile.presenter.ProfilePresenter;

import butterknife.BindView;

public class ProfileActivity extends BaseMvpActivity<ProfileView, ProfilePresenter>
        implements ProfileView {

    @BindView(R.id.profile_toolbar) Toolbar toolbar;

    @BindView(R.id.profile_image) AvatarView avatarView;
    @BindView(R.id.profile_name) TextView nameView;
    @BindView(R.id.profile_summary) TextView summaryView;

    @BindView(R.id.profile_in_progress) ListItemStatusView progressStatus;
    @BindView(R.id.profile_archived) ListItemStatusView archivedStatus;

    private ProfileComponent component;

    @Override
    protected int getLayoutRes() {
        return R.layout.profile_activity;
    }

    @Override
    protected void injectDependencies() {
        if (component == null) {
            component = ((HasComponent<ApplicationComponent>) getApplication()).getComponent().profileComponent();
        }
        component.inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean isTablet = getResources().getBoolean(R.bool.isTablet);

        if (isTablet && savedInstanceState == null) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        setSupportActionBar(toolbar);

        setTitle(getString(R.string.progile));
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_container);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        progressStatus.setStatusType(ListItemStatusView.StatusType.OVERDUE);
        progressStatus.setTitle(getString(R.string.in_progress));

        archivedStatus.setStatusType(ListItemStatusView.StatusType.COMPLETED);
        archivedStatus.setTitle(getString(R.string.archived));

        avatarView.setType(AvatarView.Type.CREATOR);
        avatarView.setShadowRadius(9);

        avatarView.post(() -> getPresenter().getData());
    }

    @Override
    public void onLoadedUser(User user) {
        if (user != null) {
            avatarView.setAvatar(user.photo());
            nameView.setText(user.name());
        }
    }

    @Override
    public void onError(Throwable throwable) {
        if (throwable != null) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void onStatusLoaded(ProfileStatus profileStatus) {
        progressStatus.setCount(profileStatus.inProgress());
        archivedStatus.setCount(profileStatus.archived());
    }
}
