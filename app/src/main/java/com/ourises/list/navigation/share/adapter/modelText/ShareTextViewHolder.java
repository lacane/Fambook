package com.ourises.list.navigation.share.adapter.modelText;

import com.ourises.list.navigation.share.adapter.common.ShareCommonItem;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItemView;
import com.ourises.list.navigation.share.adapter.common.ShareCommonViewHolder;

public class ShareTextViewHolder extends ShareCommonViewHolder {
    public ShareTextViewHolder(ShareCommonItemView itemView) {
        super(itemView);
    }

    @Override
    public void onBindViewHolder(ShareCommonItem item) {
        super.onBindViewHolder(item);

        ShareTextItem textItem = (ShareTextItem) item;
        ShareTextItemView textItemView = (ShareTextItemView) itemView;
        textItemView.onBind(textItem);
    }
}
