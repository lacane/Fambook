package com.ourises.list.navigation.home.widget;


import com.alapshin.genericrecyclerview.DefaultAdapter;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.widget.Indexed;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class HomeAdapter extends DefaultAdapter<ListModel, ListItemViewHolder> {

    private OnSelectItemListener onSelectItemListener;

    public void setOnSelectItemListener(OnSelectItemListener onSelectItemListener) {
        this.onSelectItemListener = onSelectItemListener;
    }

    @Override
    public void onBindViewHolder(ListItemViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        holder.setOnSelectItemListener(onSelectItemListener);
        holder.onBindViewHolder(itemProvider.getItem(position));
    }

    public void setEditMode(boolean editMode) {
        Observable.from(itemProvider.getItems())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .zipWith(Observable.range(0, itemProvider.getItemCount()), Indexed::new)
                .subscribe(indexed -> {
                    itemProvider.setItem(indexed.index, indexed.value.toBuilder().editMode(editMode).build());
                });
    }

    public interface OnSelectItemListener {
        void onClickItem(ListModel listModel);

        void onSelectItem(ListModel item);
    }
}
