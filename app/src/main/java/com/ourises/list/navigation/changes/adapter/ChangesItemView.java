package com.ourises.list.navigation.changes.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ourises.list.R;
import com.ourises.list.common.widget.avatar.AvatarView;
import com.ourises.list.common.widget.util.DateUtil;
import com.ourises.list.navigation.changes.model.ChangesModel;
import com.ourises.list.navigation.changes.widget.ChangesLabelView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangesItemView extends RelativeLayout {

    @BindView(R.id.changes_item_view_label) ChangesLabelView labelView;
    @BindView(R.id.changes_item_view_description) TextView descriptionView;
    @BindView(R.id.changes_item_view_date) TextView dateView;
    @BindView(R.id.changes_item_view_user) AvatarView avatarView;

    private static final Locale loc = Locale.getDefault();

    public ChangesItemView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.changes_item_view, this);
        ButterKnife.bind(this);
    }

    public void onBind(ChangesModel changesModel) {
        labelView.setAction(changesModel.action());
        avatarView.setAvatar(changesModel.userPhoto());
        dateView.setText(DateUtil.getDateForChangesList(changesModel.updatedAt()));

        determineAction(changesModel);
    }

    @SuppressWarnings("ConstantConditions")
    private void determineAction(ChangesModel changesModel) {
        String text = "";
        switch (changesModel.action()) {
            case SHARE:
                text = getContext().getString(R.string.changes_share);
                break;
            case DELETED:
                text = getContext().getString(R.string.changes_deleted);
                break;
            case ITEM_CHECKED:
                text = String.format(loc, getContext().getString(R.string.changes_checked), changesModel.itemName());
                break;
            case ITEM_UNCHECKED:
                text = String.format(loc, getContext().getString(R.string.changes_unchecked), changesModel.itemName());
                break;
            case ITEM_ADD:
                text = String.format(loc, getContext().getString(R.string.changes_add), changesModel.itemName());
                break;
            case ITEM_DELETE:
                text = String.format(loc, getContext().getString(R.string.changes_item_deleted), changesModel.itemName());
                break;
            case ARCHIVED:
                text = getContext().getString(R.string.changes_archived);
                break;
            case UNARCHIVED:
                text = getContext().getString(R.string.changes_unarchived);
                break;
            case ITEM_UPDATE:
                text = String.format(loc, getContext().getString(R.string.changes_item_updated),
                        changesModel.itemName());
                break;
            case LIST_UPDATE:
                text = String.format(loc, getContext().getString(R.string.changes_updated),
                        changesModel.itemName());
                break;
            default:break;
        }
        descriptionView.setText(text);
    }
}
