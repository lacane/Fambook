package com.ourises.list.navigation.homeDescription.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.alapshin.genericrecyclerview.ItemProvider;
import com.alapshin.genericrecyclerview.ViewHolderDelegateManager;
import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.common.widget.EditTextImeAction;
import com.ourises.list.common.widget.Indexed;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonViewHolder;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class HomeDescriptionAdapter extends RecyclerView.Adapter<HomeDescriptionCommonViewHolder> {

    private ViewHolderDelegateManager<HomeDescriptionCommonItem, HomeDescriptionCommonViewHolder> delegateManager;
    private ItemProvider<HomeDescriptionCommonItem> itemProvider;
    private OnChangeItemContentListener onChangeItemContentListener;

    @Override
    public HomeDescriptionCommonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return delegateManager.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(HomeDescriptionCommonViewHolder holder, int position) {
        holder.setOnChangeItemContentListener(onChangeItemContentListener);
        delegateManager.onBindViewHolder(holder, itemProvider.getItem(position));
    }

    @Override
    public int getItemViewType(int position) {
        return delegateManager.getItemViewType(itemProvider.getItem(position));
    }

    @Override
    public int getItemCount() {
        return itemProvider.getItemCount();
    }

    public void setDelegateManager(ViewHolderDelegateManager<HomeDescriptionCommonItem,
            HomeDescriptionCommonViewHolder> delegateManager) {
        this.delegateManager = delegateManager;
    }

    public void setItemProvider(ItemProvider<HomeDescriptionCommonItem> itemProvider) {
        this.itemProvider = itemProvider;
    }

    public void setOnChangeItemContentListener(OnChangeItemContentListener onChangeItemContentListener) {
        this.onChangeItemContentListener = onChangeItemContentListener;
    }

    public void setEditMode(boolean editMode) {
        Observable.from(itemProvider.getItems())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .zipWith(Observable.range(0, itemProvider.getItemCount()), Indexed::new)
                .filter(indexed -> indexed.value.type() == ListItemModel.LIST_MODEL_TYPE)
                .subscribe(indexed -> {
                    ListItemModel itemModel = (ListItemModel) indexed.value;

                    if (indexed.index < itemProvider.getItemCount()) {
                        itemProvider.setItem(indexed.index, itemModel.toBuilder().editMode(editMode).build());
                    }
                }, Throwable::printStackTrace);
    }

    public interface OnChangeItemContentListener {
        void onClickOnAddItem(String name);

        void onClickOnItem(String itemId, EditTextImeAction editText);

        void onClickOnCheckedItem(String itemId, boolean isChecked);

        void onClickOnRemoveItem(String itemId);

        void onClickOnSelectItem(String itemId);

        void onClickOnUpdateItem(String itemId, String name);

        void onClickShare();

        void onClickOnChanges();

        void onClickOnChangeTitle(String title);
    }
}
