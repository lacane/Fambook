package com.ourises.list.navigation.homeDescription.adapter.modelAdd;

import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItemView;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonViewHolder;

public class HomeDescriptionAddViewHolder extends HomeDescriptionCommonViewHolder {
    public HomeDescriptionAddViewHolder(HomeDescriptionCommonItemView itemView) {
        super(itemView);
    }
}
