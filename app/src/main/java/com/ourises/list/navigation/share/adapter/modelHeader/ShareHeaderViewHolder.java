package com.ourises.list.navigation.share.adapter.modelHeader;

import com.ourises.list.navigation.share.adapter.common.ShareCommonItem;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItemView;
import com.ourises.list.navigation.share.adapter.common.ShareCommonViewHolder;

public class ShareHeaderViewHolder extends ShareCommonViewHolder {
    public ShareHeaderViewHolder(ShareCommonItemView itemView) {
        super(itemView);
    }

    @Override
    public void onBindViewHolder(ShareCommonItem item) {
        super.onBindViewHolder(item);

        ShareHeaderItem headerItem = (ShareHeaderItem) item;
        ShareHeaderItemView headerItemView = (ShareHeaderItemView) itemView;
        headerItemView.onBind(headerItem);
    }
}
