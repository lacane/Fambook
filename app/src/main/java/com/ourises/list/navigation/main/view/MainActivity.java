package com.ourises.list.navigation.main.view;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.fambook.base.mvp.view.BaseMvpActivity;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.ourises.list.R;
import com.ourises.list.common.entities.User;
import com.ourises.list.common.service.SynchronizingService;
import com.ourises.list.common.widget.util.AnimUtils;
import com.ourises.list.di.HasComponent;
import com.ourises.list.di.components.ApplicationComponent;
import com.ourises.list.di.components.HomeComponent;
import com.ourises.list.navigation.changes.view.ChangesDialog;
import com.ourises.list.navigation.homeDescription.view.HomeDescriptionFragmentBuilder;
import com.ourises.list.navigation.main.drawer.DrawerHeaderView;
import com.ourises.list.navigation.main.drawer.Navigation;
import com.ourises.list.navigation.auth.view.AuthActivity;
import com.ourises.list.navigation.home.view.HomeFragment;
import com.ourises.list.navigation.home.view.HomeHistoryFragment;
import com.ourises.list.navigation.homeDescription.view.HomeDescriptionActivity;
import com.ourises.list.navigation.homeDescription.view.HomeDescriptionFragment;
import com.ourises.list.navigation.main.model.ToolbarProvider;
import com.ourises.list.navigation.main.presenter.MainPresenter;
import com.ourises.list.navigation.settings.view.SettingsFragment;
import com.ourises.list.navigation.share.view.ShareDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseMvpActivity<MainView, MainPresenter>
        implements MainView, HasComponent<HomeComponent>, ToolbarProvider {

    @BindView(R.id.main_toolbar) Toolbar toolbar;

    @BindView(R.id.main_root_view) ViewGroup rootView;
    @BindView(R.id.main_fragments_container) ViewGroup content;
    private ViewGroup detailContainer;

    private HomeComponent component;
    private boolean loadedUser = false;
    private MenuItem connectionMenuItem;

    private boolean isTablet = false;
    private DrawerHeaderView headerView;
    private int currentItemId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isTablet = getResources().getBoolean(R.bool.isTablet);

        if (isTablet && savedInstanceState == null) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            detailContainer = (ViewGroup) findViewById(R.id.main_detail_container);
        }

        setSupportActionBar(toolbar);
        toolbar.post(() -> getPresenter().connect());

        setupDrawer(savedInstanceState);
    }

    private void setupDrawer(Bundle savedInstanceState) {
        headerView = new DrawerHeaderView(this);

        Drawer drawer = new DrawerBuilder()
                .withSavedInstance(savedInstanceState)
                .withActivity(this)
                .withToolbar(toolbar)
                .withHeaderDivider(false)
                .withTranslucentStatusBar(true)
                .withDisplayBelowStatusBar(true)
                .withActionBarDrawerToggleAnimated(true)
                .withDrawerItems(createDrawerItems())
                .withHeader(headerView)
                .build();

        drawer.setOnDrawerItemClickListener((view, position, drawerItem) -> {
            long item = drawerItem.getIdentifier();
            displayView((int) item);
            return false;
        });
    }

    private PrimaryDrawerItem createPrimaryItem(int id, @StringRes int nameRes, @DrawableRes int iconRes) {
        return new PrimaryDrawerItem()
                .withIdentifier(id)
                .withName(nameRes)
                .withIcon(iconRes)
                .withSelectedIconColorRes(R.color.navigation_drawer_primary_selected_icon)
                .withSelectedTextColorRes(R.color.navigation_drawer_primary_selected_text)
                .withIconTintingEnabled(true);
    }

    private List<IDrawerItem> createDrawerItems() {
        List<IDrawerItem> items = new ArrayList<>();
        items.add(createPrimaryItem(Navigation.HOME,
                R.string.navigation_home, R.drawable.ic_home));

        items.add(createPrimaryItem(Navigation.HISTORY,
                R.string.navigation_history, R.drawable.ic_history));

        items.add(createPrimaryItem(Navigation.SETTINGS,
                R.string.navigation_settings, R.drawable.ic_settings));

        return items;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.main_activity;
    }

    @Override
    protected void injectDependencies() {
        getComponent().inject(this);
    }

    private void displayView(int itemId) {
        if (currentItemId != itemId) {
            currentItemId = itemId;
            Fragment fragment = null;
            switch (itemId) {
                case Navigation.HOME:
                    fragment = new HomeFragment();
                    break;
                case Navigation.HISTORY:
                    fragment = new HomeHistoryFragment();
                    break;
                case Navigation.SETTINGS:
                    fragment = new SettingsFragment();
                    break;
                default:
                    break;
            }

            if (fragment != null && !fragment.isVisible()) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_container, fragment)
                        .commit();
            }

            AnimUtils.hideFade(detailContainer);
        }
    }

    @Override
    public HomeComponent getComponent() {
        if (component == null)
            component = ((HasComponent<ApplicationComponent>) getApplication()).getComponent().homeComponent();
        return component;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public void setUser(User user) {
        if (user != null) {
            headerView.changeName(user.name());
            headerView.changeImage(user.photo());
        }

        if (!loadedUser) {
            loadedUser = true;
            displayView(Navigation.HOME);
        }
    }

    @Override
    public void onError(Throwable throwable) {
        if (throwable != null) {
            throwable.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        connectionMenuItem = menu.findItem(R.id.menu_main_connection);
        connectionMenuItem.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_main_connection:
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void offlineMode(boolean offlineMode) {
        if (offlineMode && connectionMenuItem != null) {
            connectionMenuItem.setVisible(true);
        } else if (connectionMenuItem != null) {
            connectionMenuItem.setVisible(false);
        }
    }

    @Override
    public void startDescription(Fragment fragment, String itemId, int requestCode) {
        if (!isTablet) {
            Intent descriptionIntent = new Intent(this, HomeDescriptionActivity.class);
            descriptionIntent.putExtra(HomeDescriptionActivity.EXTRA_LIST_MODEL, itemId);
            fragment.startActivityForResult(descriptionIntent, requestCode);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
            }
        } else {
            AnimUtils.showFade(detailContainer);
            HomeDescriptionFragment.OnClickActionListener changesListener = newItemId -> {
                ChangesDialog changesDialog = ChangesDialog.newInstance(newItemId);
                changesDialog.show(getSupportFragmentManager(), "ChangesDialog");
            };
            HomeDescriptionFragment.OnClickActionListener shareListener = newItemId -> {
                ShareDialog shareDialog = ShareDialog.newInstance(newItemId);
                shareDialog.show(getSupportFragmentManager(), "ShareDialog");
            };
            HomeDescriptionFragment descriptionFragment = new HomeDescriptionFragmentBuilder(itemId).build();
            descriptionFragment.setChangesListListener(changesListener);
            descriptionFragment.setShareListener(shareListener);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_detail_container, descriptionFragment)
                    .commit();
        }
    }

    @Override
    public void connectedError() {
        startActivity(new Intent(this, AuthActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        startService(new Intent(this, SynchronizingService.class));

        super.onDestroy();
    }
}
