package com.ourises.list.navigation.main.drawer;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ourises.list.R;
import com.ourises.list.common.widget.avatar.AvatarView;
import com.ourises.list.navigation.profile.view.ProfileActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DrawerHeaderView extends FrameLayout {
    @BindView(R.id.header_profile_container) ViewGroup userContainer;
    @BindView(R.id.drawer_header_name) TextView name;
    @BindView(R.id.header_profile_icon) AvatarView imageView;

    public DrawerHeaderView(Context context) {
        super(context);
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.drawer_header_view, this);
        ButterKnife.bind(this);

        imageView.setYShadow(0);

        userContainer.setOnClickListener(v -> {
            getContext().startActivity(new Intent(getContext(), ProfileActivity.class));
        });
    }

    public void changeName(String name) {
        this.name.setText(name);
    }

    public void changeImage(String image) {
        imageView.setType(AvatarView.Type.CREATOR);
        imageView.setAvatar(image);
    }
}
