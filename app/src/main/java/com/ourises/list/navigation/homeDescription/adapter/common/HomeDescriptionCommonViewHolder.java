package com.ourises.list.navigation.homeDescription.adapter.common;

import com.alapshin.genericrecyclerview.DefaultViewHolder;
import com.ourises.list.navigation.homeDescription.adapter.HomeDescriptionAdapter;

public class HomeDescriptionCommonViewHolder
        extends DefaultViewHolder<HomeDescriptionCommonItem, HomeDescriptionCommonItemView> {

    public HomeDescriptionAdapter.OnChangeItemContentListener onChangeItemContentListener;

    public void setOnChangeItemContentListener(HomeDescriptionAdapter.OnChangeItemContentListener onChangeItemContentListener) {
        this.onChangeItemContentListener = onChangeItemContentListener;
    }

    public HomeDescriptionCommonViewHolder(HomeDescriptionCommonItemView itemView) {
        super(itemView);
    }

    @Override
    public void onBindViewHolder(HomeDescriptionCommonItem item) {
        super.onBindViewHolder(item);
        getItemView().setOnChangeItemContentListener(onChangeItemContentListener);
    }

    public void setSelection(boolean selected) {
        getItemView().setSelected(selected);
    }
}
