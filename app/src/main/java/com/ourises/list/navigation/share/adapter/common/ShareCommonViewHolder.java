package com.ourises.list.navigation.share.adapter.common;

import com.alapshin.genericrecyclerview.DefaultViewHolder;
import com.ourises.list.navigation.share.adapter.ShareAdapter;

public class ShareCommonViewHolder extends DefaultViewHolder<ShareCommonItem, ShareCommonItemView> {

    public ShareCommonViewHolder(ShareCommonItemView itemView) {
        super(itemView);
    }

    @Override
    public void onBindViewHolder(ShareCommonItem item) {
        super.onBindViewHolder(item);
    }

    public void setShareItemListener(ShareAdapter.OnShareItemListener shareItemListener) {
        getItemView().setShareItemListener(shareItemListener);
    }
}
