package com.ourises.list.navigation.changes.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ourises.list.R;
import com.ourises.list.navigation.changes.model.ChangesAction;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangesLabelView extends FrameLayout {

    @BindView(R.id.changes_item_label_text)
    TextView labelTextView;

    public ChangesLabelView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.changes_item_label_view, this);
        ButterKnife.bind(this);
    }

    public void setAction(ChangesAction action) {
        switch (action) {
            case SHARE:
                labelTextView.setText(getContext().getString(R.string.changes_share_label));
                labelTextView.setBackgroundResource(R.drawable.changes_label_shared_background);
                break;
            case DELETED:
                labelTextView.setText(getContext().getString(R.string.changes_deleted_label));
                labelTextView.setBackgroundResource(R.drawable.changes_label_deleted_background);
                break;
            case ITEM_CHECKED:
                labelTextView.setText(getContext().getString(R.string.changes_checked_label));
                labelTextView.setBackgroundResource(R.drawable.changes_label_checked_background);
                break;
            case ITEM_UNCHECKED:
                labelTextView.setText(getContext().getString(R.string.changes_unchecked_label));
                labelTextView.setBackgroundResource(R.drawable.changes_label_unchecked_background);
                break;
            case ITEM_ADD:
                labelTextView.setText(getContext().getString(R.string.changes_add_label));
                labelTextView.setBackgroundResource(R.drawable.changes_label_added_background);
                break;
            case ITEM_DELETE:
                labelTextView.setText(getContext().getString(R.string.changes_deleted_label));
                labelTextView.setBackgroundResource(R.drawable.changes_label_deleted_background);
                break;
            case ARCHIVED:
                labelTextView.setText(getContext().getString(R.string.changes_archived_label));
                labelTextView.setBackgroundResource(R.drawable.changes_label_archived_background);
                break;
            case UNARCHIVED:
                labelTextView.setText(getContext().getString(R.string.changes_unarchived_label));
                labelTextView.setBackgroundResource(R.drawable.changes_label_unarchived_background);
                break;
            case ITEM_UPDATE:
                labelTextView.setText(getContext().getString(R.string.changes_updated_label));
                labelTextView.setBackgroundResource(R.drawable.changes_label_updated_background);
                break;
            case LIST_UPDATE:
                labelTextView.setText(getContext().getString(R.string.changes_updated_label));
                labelTextView.setBackgroundResource(R.drawable.changes_label_updated_background);
                break;
            default:break;

        }
    }
}
