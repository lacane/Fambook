package com.ourises.list.navigation.main.drawer;

public class Navigation {
    public static final int HOME = 0;
    public static final int CALENDAR = 1;
    public static final int DAILY_SCHEDULE = 2;
    public static final int HISTORY = 3;
    public static final int SETTINGS = 4;

}
