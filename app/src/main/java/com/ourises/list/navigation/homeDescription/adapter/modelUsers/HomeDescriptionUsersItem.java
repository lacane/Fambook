package com.ourises.list.navigation.homeDescription.adapter.modelUsers;

import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.ourises.list.common.entities.User;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;

import java.util.ArrayList;
import java.util.List;

@AutoValue
public abstract class HomeDescriptionUsersItem implements HomeDescriptionCommonItem {
    public static final int USERS_MODEL_TYPE = 6;

    @Override
    public int id() {
        return 0;
    }

    @Override
    public int type() {
        return USERS_MODEL_TYPE;
    }

    @Nullable
    public abstract List<User> users();

    @Nullable
    public abstract String creatorUserId();

    public List<User> getUsers() {
        return users() == null ? new ArrayList<>() : users();
    }

    public static HomeDescriptionUsersItem create(List<User> users, String creatorUserId) {
        return new AutoValue_HomeDescriptionUsersItem.Builder()
                .users(users)
                .creatorUserId(creatorUserId)
                .build();
    }

    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder users(@Nullable List<User> users);

        public abstract Builder creatorUserId(String creatorUserId);

        public abstract HomeDescriptionUsersItem build();
    }
}
