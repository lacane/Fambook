package com.ourises.list.navigation.share.adapter.modelUser;

import com.google.auto.value.AutoValue;
import com.ourises.list.common.entities.User;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItem;

import java.util.ArrayList;
import java.util.List;

@AutoValue
public abstract class ShareUserItem implements ShareCommonItem {
    public static final int USER_TYPE = 2;

    public enum UserType {
        SHARED_ALREADY,
        SHARED_OTHER
    }

    @Override
    public int id() {
        return 0;
    }

    @Override
    public int type() {
        return USER_TYPE;
    }

    public abstract User user();

    public abstract UserType userType();

    public abstract Boolean shared();

    public abstract Boolean canChangeState();

    public abstract Builder toBuilder();

    public static List<ShareUserItem> createAlreadyListUsers(List<User> users) {
        List<ShareUserItem> shareUserItems = new ArrayList<>();

        for (User user : users) {
            shareUserItems.add(
                    new AutoValue_ShareUserItem.Builder()
                            .user(user)
                            .userType(UserType.SHARED_ALREADY)
                            .shared(true)
                            .canChangeState(false)
                            .build()
            );
        }

        return shareUserItems;
    }

    public static List<ShareUserItem> createOtherListUsers(List<User> users) {
        List<ShareUserItem> shareUserItems = new ArrayList<>();

        for (User user : users) {
            shareUserItems.add(
                    new AutoValue_ShareUserItem.Builder()
                            .user(user)
                            .userType(UserType.SHARED_OTHER)
                            .shared(false)
                            .canChangeState(true)
                            .build()
            );

        }

        return shareUserItems;
    }

    public static ShareUserItem create(User user, UserType type, boolean shared) {
        return new AutoValue_ShareUserItem.Builder().user(user).userType(type).shared(shared).build();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder user(User user);

        public abstract Builder userType(UserType userType);

        public abstract Builder shared(Boolean shared);

        public abstract Builder canChangeState(Boolean canChangeState);

        public abstract ShareUserItem build();
    }
}
