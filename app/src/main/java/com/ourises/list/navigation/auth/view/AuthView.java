package com.ourises.list.navigation.auth.view;


import com.alapshin.arctor.view.MvpView;
import com.ourises.list.common.entities.User;

public interface AuthView extends MvpView {
    void onError(Throwable throwable);

    void successLoadingUser();

    void haveNotUser();

    void goToOffline(User user);

    void onErrorConnect();
}
