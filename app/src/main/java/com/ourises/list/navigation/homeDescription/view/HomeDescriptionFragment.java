package com.ourises.list.navigation.homeDescription.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.alapshin.genericrecyclerview.DefaultItemProvider;
import com.alapshin.genericrecyclerview.ItemProvider;
import com.alapshin.genericrecyclerview.ViewHolderDelegateManager;
import com.fambook.base.mvp.view.BaseMvpFragment;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.ourises.list.R;
import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.entities.ResponseModelId;
import com.ourises.list.common.widget.ArchiveButton;
import com.ourises.list.common.widget.EditTextImeAction;
import com.ourises.list.common.widget.Indexed;
import com.ourises.list.common.widget.recyclerView.DefaultAnimator;
import com.ourises.list.common.widget.util.InfoDialog;
import com.ourises.list.common.widget.util.SystemUtil;
import com.ourises.list.di.HasComponent;
import com.ourises.list.di.components.ApplicationComponent;
import com.ourises.list.di.components.HomeComponent;
import com.ourises.list.navigation.home.view.HomeFragment;
import com.ourises.list.navigation.homeDescription.adapter.HomeDescriptionAdapter;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonViewHolder;
import com.ourises.list.navigation.homeDescription.adapter.modelAdd.HomeDescriptionAddDelegate;
import com.ourises.list.navigation.homeDescription.adapter.modelAdd.HomeDescriptionAddItem;
import com.ourises.list.navigation.homeDescription.adapter.modelFooter.HomeDescriptionFooterDelegate;
import com.ourises.list.navigation.homeDescription.adapter.modelList.HomeDescriptionListDelegate;
import com.ourises.list.navigation.homeDescription.adapter.modelTitle.HomeDescriptionTitleDelegate;
import com.ourises.list.navigation.homeDescription.adapter.modelUsers.HomeDescriptionUsersDelegate;
import com.ourises.list.navigation.homeDescription.presenter.HomeDescriptionPresenter;
import com.ourises.list.navigation.homeDescription.widget.HomeDescriptionActionCallback;
import com.ourises.list.navigation.share.view.ShareActivity;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.util.List;

import butterknife.BindView;
import rx.Observable;

import static android.app.Activity.RESULT_OK;

@FragmentWithArgs
public class HomeDescriptionFragment extends BaseMvpFragment<HomeDescriptionView, HomeDescriptionPresenter>
        implements HomeDescriptionView, HomeDescriptionAdapter.OnChangeItemContentListener,
        KeyboardVisibilityEventListener {

    @Arg String itemId;
    private OnClickActionListener changesListListener;
    private OnClickActionListener shareListener;

    public static final int RC_SHARE_RESULT = 10;

    @BindView(R.id.home_item_description_recycler) RecyclerView recyclerView;
    @BindView(R.id.home_item_description_archive_button) ArchiveButton archiveButton;
    @BindView(R.id.home_item_description_root) ViewGroup rootView;

    private HomeComponent component;
    private boolean editMode = false;
    private boolean archived = false;
    private boolean isTablet = false;

    private ActionMode actionMode;
    private LinearLayoutManager layoutManager;
    private HomeDescriptionAdapter descriptionAdapter = new HomeDescriptionAdapter();
    private ItemProvider<HomeDescriptionCommonItem> itemProvider = new DefaultItemProvider<>();
    private ViewHolderDelegateManager<HomeDescriptionCommonItem, HomeDescriptionCommonViewHolder> delegateManager =
            new ViewHolderDelegateManager<>();
    private HomeDescriptionActionCallback.OnActionListener actionListener =
            new HomeDescriptionActionCallback.OnActionListener() {
                @Override
                public void onDestroyAction() {
                    setEditMode(false);
                }

                @Override
                public void onClickDeleteAction() {
                    getPresenter().sortedItems(itemProvider.getItems(), ListItemModel.LIST_MODEL_TYPE)
                            .filter(indexed -> ((ListItemModel) indexed.value).isSelected())
                            .toList()
                            .subscribe(indexedList -> {
                                for (int index = indexedList.size() - 1; index >= 0; index--) {
                                    Indexed<HomeDescriptionCommonItem> indexed = indexedList.get(index);
                                    itemProvider.removeItem(indexed.index);
                                    getPresenter().removeItemModel(indexed.value);
                                }

                                getPresenter().updateItemsInStorage(itemProvider.getItems());
                                actionMode.finish();
                            }, throwable -> onError(throwable));
                }

                @Override
                public void onClickSelectAction() {

                }
            };

    @Override
    protected int getLayoutRes() {
        return R.layout.home_item_description_fragment;
    }

    @Override
    protected void injectDependencies() {
        if (component == null) {
            component = ((HasComponent<ApplicationComponent>)
                    getActivity().getApplication()).getComponent().homeComponent();
        }

        component.inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isTablet = getResources().getBoolean(R.bool.isTablet);
        KeyboardVisibilityEvent.setEventListener(getActivity(), this);

        descriptionAdapter.setDelegateManager(delegateManager);
        descriptionAdapter.setOnChangeItemContentListener(this);
        delegateManager.addDelegate(new HomeDescriptionListDelegate());
        delegateManager.addDelegate(new HomeDescriptionTitleDelegate());
        delegateManager.addDelegate(new HomeDescriptionUsersDelegate());
        delegateManager.addDelegate(new HomeDescriptionAddDelegate());
        delegateManager.addDelegate(new HomeDescriptionFooterDelegate());

        itemProvider.setAdapter(descriptionAdapter);
        descriptionAdapter.setItemProvider(itemProvider);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(descriptionAdapter);

        recyclerView.setItemAnimator(new DefaultAnimator());
        getPresenter().getListModel(itemId);
    }

    public void setChangesListListener(OnClickActionListener listListener) {
        this.changesListListener = listListener;
    }

    public void setShareListener(OnClickActionListener shareListener) {
        this.shareListener = shareListener;
    }

    private void unselectItems() {
        for (int index = 0; index < itemProvider.getItems().size(); index++) {
            if (itemProvider.getItem(index).type() == ListItemModel.LIST_MODEL_TYPE) {
                ListItemModel listItemModel = (ListItemModel) itemProvider.getItem(index);
                listItemModel = listItemModel.toBuilder().editMode(false).selected(false).build();
                itemProvider.setItem(index, listItemModel);
            }
        }
    }

    @Override
    public void onLoadedListModel(ListModel listModel) {
        itemId = listModel.itemId();
        archived = listModel.archived();
        archiveButton.setArchivedState(listModel.archived());
        archiveButton.setOnClickListener(v -> close(true));
    }

    @Override
    public void onError(Throwable throwable) {
        if (throwable != null) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void onLoadedListItems(List<HomeDescriptionCommonItem> items) {
        itemProvider.setItems(items);
        descriptionAdapter.setEditMode(false);

        getPresenter().updateItemsInStorage(items);
    }

    @Override
    public void onDeleteListModel() {
        InfoDialog.show(getActivity(), getString(R.string.attention), getString(R.string.deleted_list_already),
                (dialog, which) -> close(false));
    }

    @Override
    public void close(boolean archived) {
        if (!isTablet) {
            Intent data = new Intent();
            data.putExtra(HomeDescriptionActivity.EXTRA_LIST_MODEL, itemId);

            if (!archived) {
                getActivity().setResult(RESULT_OK, data);
            } else {
                getActivity().setResult(HomeFragment.RESULT_ARCHIVED, data);
            }

            getActivity().finish();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getActivity().overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
            }
        } else {
            if (archived) {
                getPresenter().changeArchiveState();
            }

            rootView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void setStatusModel(int overdueSize, int completedSize) {
        if (overdueSize == 0 && completedSize > 0) {
            archiveButton.show();
        } else {
            archiveButton.hide();
        }
    }

    @Override
    public void checkForArchived(Boolean archived) {
        if (this.archived != archived) {
            String text = archived ?
                    getString(R.string.archive_list_already) : getString(R.string.unarchive_list_already);
            InfoDialog.show(getActivity(), getString(R.string.attention), text, (dialog, which) -> close(true));
        }
    }

    @Override
    public void updateListModel(String newListModelId) {
        recyclerView.postDelayed(() -> {
            getPresenter().getListModel(newListModelId);
        }, 100);
    }

    @Override
    public void updateCheckedItem(int position, ListItemModel itemModel, boolean isChecked) {
        itemProvider.removeItem(position);

        if (isChecked) {
            itemProvider.addItem(itemProvider.getItemCount() - 1, itemModel);

            getPresenter().updateItemModel(itemModel);
            getPresenter().updateItemsInStorage(itemProvider.getItems());
        } else {
            getPresenter().sortedItems(itemProvider.getItems(), HomeDescriptionAddItem.ADD_MODEL_TYPE)
                    .subscribe(indexed -> {
                        itemProvider.addItem(indexed.index, itemModel);

                        getPresenter().updateItemModel(itemModel);
                        getPresenter().updateItemsInStorage(itemProvider.getItems());
                    }, this::onError);
        }
    }

    @Override
    public void onClickOnAddItem(String name) {
        getPresenter().addListItemModel(itemProvider.getItems(), name);
    }

    @Override
    public void addItem(int position, ListItemModel itemModel) {
        itemProvider.addItem(position, itemModel);
        getPresenter().updateItemsInStorage(itemProvider.getItems());
    }

    @Override
    public void synchronizedItem(String itemId, ListItemModel itemModel) {
        getPresenter().sortedItems(itemProvider.getItems(), ListItemModel.LIST_MODEL_TYPE)
                .filter(indexed -> itemId.equals(((ListItemModel) indexed.value).itemId()))
                .subscribe(indexed -> {
                    itemProvider.setItem(indexed.index, itemModel.doSynchronize());
                    getPresenter().updateItemsInStorage(itemProvider.getItems());
                }, this::onError);
    }

    @Override
    public void onClickOnItem(String itemId, EditTextImeAction editText) {
        if (!editMode) {
            editText.setEnabled(true);
            editText.selectAll();
            editText.post(() -> SystemUtil.showKeyboard(getActivity(), editText));
        } else {
            editText.setEnabled(false);
            getPresenter().sortedItems(itemProvider.getItems(), ListItemModel.LIST_MODEL_TYPE)
                    .filter(indexed -> itemId.equals(((ListItemModel) indexed.value).itemId()))
                    .subscribe(indexed -> {
                        ListItemModel listItemModel = (ListItemModel) indexed.value;
                        listItemModel = listItemModel.toBuilder().selected(!listItemModel.isSelected()).build();
                        itemProvider.setItem(indexed.index, listItemModel);
                        getPresenter().checkForEditMode(itemProvider.getItems());
                    }, this::onError);
        }
    }

    @Override
    public void onClickOnCheckedItem(String itemId, boolean isChecked) {
        if (!editMode) {
            getPresenter().checkItem(itemProvider.getItems(), itemId, isChecked);
        }
    }

    @Override
    public void onClickOnRemoveItem(String itemId) {
        getPresenter().sortedItems(itemProvider.getItems(), ListItemModel.LIST_MODEL_TYPE)
                .filter(indexed -> itemId.equals(((ListItemModel) indexed.value).itemId()))
                .subscribe(indexed -> {
                    itemProvider.removeItem(indexed.index);

                    getPresenter().removeItemModel(indexed.value);
                    getPresenter().updateItemsInStorage(itemProvider.getItems());
                }, this::onError);
    }

    @Override
    public void onClickOnSelectItem(String itemId) {
        Observable.from(itemProvider.getItems())
                .zipWith(Observable.range(0, itemProvider.getItemCount()), Indexed::new)
                .filter(indexed -> indexed.value.type() == ListItemModel.LIST_MODEL_TYPE)
                .filter(indexed -> itemId.equals(((ListItemModel) indexed.value).itemId()))
                .subscribe(indexed -> {
                    ListItemModel listItemModel = (ListItemModel) indexed.value;
                    listItemModel = listItemModel.toBuilder().selected(!listItemModel.isSelected()).build();
                    itemProvider.setItem(indexed.index, listItemModel);
                    getPresenter().checkForEditMode(itemProvider.getItems());
                }, this::onError);
    }

    @Override
    public void onClickOnUpdateItem(String itemId, String name) {
        getPresenter().sortedItems(itemProvider.getItems(), ListItemModel.LIST_MODEL_TYPE)
                .filter(indexed -> itemId.equals(((ListItemModel) indexed.value).itemId()))
                .subscribe(indexed -> {
                    ListItemModel itemModel = (ListItemModel) indexed.value;
                    itemModel = itemModel.toBuilder().name(name).build();
                    itemProvider.setItem(indexed.index, itemModel);

                    getPresenter().updateItemModel(itemModel);
                    getPresenter().updateItemsInStorage(itemProvider.getItems());
                }, this::onError);
    }

    @Override
    public void onClickShare() {
        if (isTablet) {
            shareListener.onClick(itemId);
        } else {
            Intent shareIntent = new Intent(getActivity(), ShareActivity.class);
            shareIntent.putExtra(ShareActivity.SHARED_LIST_ID, itemId);
            startActivityForResult(shareIntent, RC_SHARE_RESULT);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getActivity().overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
            }
        }
    }

    @Override
    public void onClickOnChanges() {
        changesListListener.onClick(itemId);
    }

    @Override
    public void onClickOnChangeTitle(String title) {
        getPresenter().updateNameOfList(title);
    }

    private void setEditMode(boolean editMode) {
        setEditMode(editMode, 0);
    }

    @Override
    public void setEditMode(boolean editMode, int selectedCount) {
        if (editMode && !this.editMode) {
            this.editMode = true;
            editToolbarMode(true);
            Observable.from(itemProvider.getItems())
                    .zipWith(Observable.range(0, itemProvider.getItemCount()), Indexed::new)
                    .filter(indexed -> indexed.value.type() == HomeDescriptionAddItem.ADD_MODEL_TYPE)
                    .subscribe(indexed -> {
                        itemProvider.removeItem(indexed.index);
                        descriptionAdapter.setEditMode(true);
                    }, this::onError);
        } else if (this.editMode != editMode) {
            this.editMode = false;

            editToolbarMode(false);
            getPresenter().sortedItems(itemProvider.getItems(), ListItemModel.LIST_MODEL_TYPE)
                    .filter(indexed -> !((ListItemModel) indexed.value).status())
                    .toList()
                    .subscribe(indexedList -> {
                        itemProvider.addItem(indexedList.size() + 2, new HomeDescriptionAddItem());
                        unselectItems();
                    }, this::onError);
        }

        if (actionMode != null) {
            actionMode.setTitle(String.valueOf(selectedCount) + " " + getString(R.string.selected));
        }
    }

    private void editToolbarMode(boolean show) {
        if (show) {
            recyclerView.setNestedScrollingEnabled(false);
            actionMode = ((AppCompatActivity) getActivity())
                    .startSupportActionMode(new HomeDescriptionActionCallback(actionListener));
        } else {
            recyclerView.setNestedScrollingEnabled(true);
            if (actionMode != null) {
                actionMode.finish();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SHARE_RESULT && resultCode == RESULT_OK) {
            getPresenter().getListModel(itemId);
        }
    }

    @Override
    public void onVisibilityChanged(boolean showedKeyboard) {
        if (archiveButton != null) {
            archiveButton.changeStateForKeyboard(showedKeyboard);
        }
    }

    public interface OnClickActionListener {
        void onClick(String itemId);
    }
}
