package com.ourises.list.navigation.homeDescription.adapter.modelList;

import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItemView;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonViewHolder;

public class HomeDescriptionListViewHolder extends HomeDescriptionCommonViewHolder {
    public HomeDescriptionListViewHolder(HomeDescriptionCommonItemView itemView) {
        super(itemView);
    }

    @Override
    public void onBindViewHolder(HomeDescriptionCommonItem item) {
        super.onBindViewHolder(item);

        ListItemModel itemModel = (ListItemModel) item;
        HomeDescriptionListItemView listItemView = (HomeDescriptionListItemView) itemView;
        listItemView.setSelected(itemModel.isSelected());
        listItemView.bind(itemModel);
    }
}
