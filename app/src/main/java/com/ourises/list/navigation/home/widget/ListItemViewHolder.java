package com.ourises.list.navigation.home.widget;

import com.alapshin.genericrecyclerview.DefaultViewHolder;
import com.ourises.list.common.entities.ListModel;

public class ListItemViewHolder extends DefaultViewHolder<ListModel, ListItemView> {
    private HomeAdapter.OnSelectItemListener onSelectItemListener;

    public void setOnSelectItemListener(HomeAdapter.OnSelectItemListener onSelectItemListener) {
        this.onSelectItemListener = onSelectItemListener;
    }

    public ListItemViewHolder(ListItemView itemView) {
        super(itemView);
    }

    public void onBindViewHolder(ListModel item) {
        super.onBindViewHolder(item);

        ListItemView listItemView = (ListItemView) itemView;
        listItemView.setListener(onSelectItemListener);
        listItemView.setSelected(item.isSelected());
        listItemView.bind(item);
    }
}
