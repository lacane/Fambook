package com.ourises.list.navigation.auth.presenter;


import android.content.Context;
import android.net.Uri;

import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.ourises.list.common.model.OkHttpModel;
import com.ourises.list.common.model.StorageModel;
import com.ourises.list.navigation.auth.view.AuthFacebookView;

import java.util.HashMap;
import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AuthFacebookPresenterImpl extends RxPresenter<AuthFacebookView> implements AuthFacebookPresenter {
    private final StorageModel storageModel;
    private final OkHttpModel okHttpModel;

    public AuthFacebookPresenterImpl(StorageModel storageModel, OkHttpModel okHttpModel) {
        this.storageModel = storageModel;
        this.okHttpModel = okHttpModel;
    }

    @Override
    public void login(String token) {
        storageModel.putTokenSession(token);

        getView().successLoadingToken();
    }

    @Override
    public void loginWithCode(Context context, String clientId, String redirectUri, String clientSecret, Uri url) {
        Map<String, String> query = getQueryMap(url.getQuery());
        String code = query.get("code");

        Subscription dataSubscription2 = okHttpModel.getAccessToken(clientId, redirectUri, clientSecret, code)
                .compose(deliverLatest())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::login, throwable -> {
                    getView().onError(throwable);
                });

        addSubscription(dataSubscription2);
    }

    private Map<String, String> getQueryMap(String query) {
        if (query == null)
            return null;

        String[] params = query.split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String param : params) {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            map.put(name, value);
        }
        return map;
    }
}
