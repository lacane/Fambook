package com.ourises.list.navigation.share.view;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.ourises.list.R;

public class ShareDialog extends DialogFragment {

    public static ShareDialog newInstance(String itemId) {
        ShareDialog shareDialog = new ShareDialog();
        Bundle args = new Bundle();
        args.putString("itemId", itemId);
        shareDialog.setArguments(args);
        return shareDialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.share_fragment_dialog, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String itemId = getArguments().getString("itemId");

        if (itemId != null) {
            ShareFragment fragment = new ShareFragmentBuilder(itemId).build();
            fragment.setOnHideListener(okResult -> dismiss());

            getChildFragmentManager().beginTransaction()
                    .replace(R.id.share_fragment_dialog_container, fragment)
                    .commit();
        } else {
            dismiss();
        }
    }
}
