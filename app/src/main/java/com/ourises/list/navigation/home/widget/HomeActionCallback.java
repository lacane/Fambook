package com.ourises.list.navigation.home.widget;

import android.os.Build;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import com.ourises.list.R;

public class HomeActionCallback implements ActionMode.Callback {

    private OnActionListener onActionListener;
    private boolean historyMode;

    public HomeActionCallback(OnActionListener onActionListener, boolean historyMode) {
        this.onActionListener = onActionListener;
        this.historyMode = historyMode;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        MenuItem archiveItem = menu.findItem(R.id.menu_home_archive);
        MenuItem unarchiveItem = menu.findItem(R.id.menu_home_unarchive);

        if (Build.VERSION.SDK_INT < 11) {

            MenuItemCompat.setShowAsAction(archiveItem, MenuItemCompat.SHOW_AS_ACTION_NEVER);
            MenuItemCompat.setShowAsAction(unarchiveItem, MenuItemCompat.SHOW_AS_ACTION_NEVER);
            MenuItemCompat.setShowAsAction(menu.findItem(R.id.menu_home_delete), MenuItemCompat.SHOW_AS_ACTION_NEVER);
        } else {
            archiveItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            unarchiveItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            menu.findItem(R.id.menu_home_delete).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }

        if (historyMode) {
            archiveItem.setVisible(false);
            unarchiveItem.setVisible(true);
        } else {
            archiveItem.setVisible(true);
            unarchiveItem.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home_unarchive:
            case R.id.menu_home_archive:
                onActionListener.onChangeArchivedAction();
                break;
            case R.id.menu_home_delete:
                onActionListener.onClickDeleteAction();
                break;
            default:
                break;
        }
        return false;
    }


    @Override
    public void onDestroyActionMode(ActionMode mode) {
        onActionListener.onDestroyAction();
    }

    public interface OnActionListener {
        void onDestroyAction();

        void onClickDeleteAction();

        void onChangeArchivedAction();
    }
}
