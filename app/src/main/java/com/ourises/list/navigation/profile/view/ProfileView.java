package com.ourises.list.navigation.profile.view;

import com.alapshin.arctor.view.MvpView;
import com.ourises.list.common.entities.User;
import com.ourises.list.navigation.profile.model.ProfileStatus;

public interface ProfileView extends MvpView {
    void onLoadedUser(User user);

    void onError(Throwable throwable);

    void onStatusLoaded(ProfileStatus profileStatus);
}
