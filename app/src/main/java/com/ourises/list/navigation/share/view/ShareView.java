package com.ourises.list.navigation.share.view;

import com.alapshin.arctor.view.MvpView;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItem;

import java.util.List;

public interface ShareView extends MvpView {
    void onLoadedItems(List<ShareCommonItem> items);

    void onError(Throwable throwable);

    void onCompleteSharing();
}
