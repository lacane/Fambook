package com.ourises.list.navigation.home.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alapshin.genericrecyclerview.DefaultItemProvider;
import com.alapshin.genericrecyclerview.ItemProvider;
import com.alapshin.genericrecyclerview.ViewHolderDelegateManager;
import com.fambook.base.mvp.view.BaseMvpFragment;
import com.github.chenglei1986.statusbar.StatusBarColorManager;
import com.ourises.list.R;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.widget.Indexed;
import com.ourises.list.common.widget.recyclerView.DefaultAnimator;
import com.ourises.list.common.widget.util.AnimUtils;
import com.ourises.list.common.widget.util.InfoDialog;
import com.ourises.list.di.HasComponent;
import com.ourises.list.di.components.HomeComponent;
import com.ourises.list.navigation.home.presenter.HomePresenter;
import com.ourises.list.navigation.home.widget.HomeActionCallback;
import com.ourises.list.navigation.home.widget.HomeAdapter;
import com.ourises.list.navigation.home.widget.HomeItemDelegate;
import com.ourises.list.navigation.home.widget.ListItemViewHolder;
import com.ourises.list.navigation.homeDescription.view.HomeDescriptionActivity;
import com.ourises.list.navigation.main.model.ToolbarProvider;
import com.ourises.list.navigation.main.view.MainView;

import java.util.List;

import butterknife.BindView;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;

public class HomeFragment extends BaseMvpFragment<HomeView, HomePresenter>
        implements HomeView, HomeAdapter.OnSelectItemListener {

    public static final int RESULT_ARCHIVED = 100;

    public static final int RC_DESCRIPTION_ITEM = 101;

    @BindView(R.id.home_root_view) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.home_recycler_view) RecyclerView recyclerView;
    @BindView(R.id.home_item_description_fab) FloatingActionButton fab;

    @BindView(R.id.home_empty_message_container) ViewGroup emptyMessageContainer;
    @BindView(R.id.home_empty_message) TextView emptyMessage;

    private LinearLayoutManager layoutManager;
    private HomeAdapter adapter = new HomeAdapter();
    private ItemProvider<ListModel> itemProvider = new DefaultItemProvider<>();
    private ViewHolderDelegateManager<ListModel, ListItemViewHolder>
            delegateManager = new ViewHolderDelegateManager<>();

    private boolean editMode = false;
    private StatusBarColorManager statusBarColorManager;
    private Toolbar toolbar;
    private ActionMode actionMode;
    private HomeActionCallback.OnActionListener actionListener = new HomeActionCallback.OnActionListener() {
        @Override
        public void onDestroyAction() {
            setEditMode(false);
            fab.show();
        }

        @Override
        public void onClickDeleteAction() {
            Observable.from(itemProvider.getItems())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .zipWith(Observable.range(0, itemProvider.getItemCount()), Indexed::new)
                    .filter(indexed -> indexed.value.isSelected())
                    .toList()
                    .doOnNext(indexedList -> unselectItems())
                    .subscribe(indexedList -> {
                        for (int index = indexedList.size() - 1; index >= 0; index--) {
                            Indexed<ListModel> indexed = indexedList.get(index);
                            itemProvider.removeItem(indexed.index);
                            getPresenter().removeListModel(indexed.value);
                        }

                        getPresenter().updateListModels(itemProvider.getItems());
                        actionMode.finish();
                        checkForEmptyMessage();
                    });
        }

        @Override
        public void onChangeArchivedAction() {
            Observable.from(itemProvider.getItems())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .zipWith(Observable.range(0, itemProvider.getItemCount()), Indexed::new)
                    .filter(indexed -> indexed.value.isSelected())
                    .toList()
                    .doOnNext(indexedList -> unselectItems())
                    .subscribe(indexedList -> {
                        for (int index = indexedList.size() - 1; index >= 0; index--) {
                            Indexed<ListModel> indexed = indexedList.get(index);
                            itemProvider.removeItem(indexed.index);
                            getPresenter().archiveListModel(indexed.value);
                        }

                        actionMode.finish();
                        checkForEmptyMessage();
                    });
        }
    };

    @Override
    protected int getLayoutRes() {
        return R.layout.home_fragment;
    }

    @Override
    protected void injectDependencies() {
        ((HasComponent<HomeComponent>) getActivity()).getComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar = ((ToolbarProvider) getActivity()).getToolbar();

        statusBarColorManager = new StatusBarColorManager(getActivity());
        statusBarColorManager.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark), false, false);

        toolbar.setTitle(getString(R.string.my_lists));
        fab.setVisibility(View.VISIBLE);

        itemProvider.setAdapter(adapter);
        adapter.setItemProvider(itemProvider);
        adapter.setViewHolderDelegateManager(delegateManager);
        delegateManager.addDelegate(new HomeItemDelegate());

        adapter.setOnSelectItemListener(this);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setVisibility(View.INVISIBLE);
        recyclerView.setItemAnimator(new DefaultAnimator());

        fab.setOnClickListener(v -> getPresenter().createListModel(getActivity()));
        getPresenter().prepareCallbacks();
        getPresenter().getListData(false);
    }

    private void unselectItems() {
        List<ListModel> listModels = itemProvider.getItems();
        for (int index = 0; index < listModels.size(); index++) {
            ListModel listModel = listModels.get(index);
            listModel = listModel.toBuilder().selected(false).editMode(false).build();
            listModels.set(index, listModel);
        }

        itemProvider.setItems(listModels);
    }

    @Override
    public void onLoadedLists(List<ListModel> models) {
        itemProvider.setItems(models);
        unselectItems();
        recyclerView.post(() -> AnimUtils.showFade(recyclerView));

        checkForEmptyMessage();
    }

    private void checkForEmptyMessage() {
        if (itemProvider.getItemCount() == 0) {
            emptyMessage.setText(getString(R.string.home_empty_message));
            fab.show();

            emptyMessage.postDelayed(() -> AnimUtils.showFade(emptyMessageContainer), 1000);
        } else {
            emptyMessageContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onError(Throwable throwable) {
        if (throwable != null) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void onUpdateListModel(ListModel listModel) {
        if (listModel.archived()) {
            removeListModel(listModel.itemId());
            checkForEmptyMessage();
        } else {
            Observable<List<Indexed<ListModel>>> listObservable = Observable.from(itemProvider.getItems())
                    .zipWith(Observable.range(0, itemProvider.getItemCount()), Indexed::new)
                    .filter(indexed -> indexed.value.itemId().equals(listModel.itemId()))
                    .toList();

            listObservable.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(indexeds -> {
                        if (indexeds.isEmpty()) {
                            addItem(listModel, false);
                        } else {
                            itemProvider.setItem(indexeds.get(0).index, listModel);
                        }

                        getPresenter().updateListModels(itemProvider.getItems());
                        checkForEmptyMessage();
                    });
        }
    }

    @Override
    public void addItem(ListModel listModel) {
        addItem(listModel, true);
    }

    private void addItem(ListModel listModel, boolean openDescription) {
        itemProvider.addItem(0, listModel);
        checkForEmptyMessage();
        recyclerView.postDelayed(() -> {
            recyclerView.smoothScrollToPosition(0);
        }, 300);

        if (openDescription) {
            onClickItem(listModel);
        }
    }

    @Override
    public void synchronizeListModel(ListModel newListModel, String oldItemId) {
        Observable.from(itemProvider.getItems())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .zipWith(Observable.range(0, itemProvider.getItemCount()), Indexed::new)
                .filter(indexed -> oldItemId.equals(indexed.value.itemId()))
                .subscribe(indexed -> {
                    itemProvider.setItem(indexed.index, newListModel.toBuilder().synchronize(true).build());
                    getPresenter().updateListModels(itemProvider.getItems());
                });
    }

    @Override
    public void updateItem(int index, ListModel listModel) {
        itemProvider.setItem(index, listModel);
    }

    private void setEditMode(boolean editMode) {
        setEditMode(editMode, 0);
    }

    @Override
    public void setEditMode(boolean editMode, int selectedCount) {
        if (editMode && !this.editMode) {
            this.editMode = true;

            editToolbarMode(true);
            adapter.setEditMode(true);
            fab.hide();
        } else if (this.editMode != editMode) {
            this.editMode = false;

            editToolbarMode(false);
            unselectItems();
            fab.show();
        }

        if (actionMode != null) {
            actionMode.setTitle(String.valueOf(selectedCount) + " " + getString(R.string.selected));
        }
    }

    private void editToolbarMode(boolean show) {
        if (show) {
            actionMode = ((AppCompatActivity) getActivity())
                    .startSupportActionMode(new HomeActionCallback(actionListener, false));
        } else if (actionMode != null) {
            actionMode.finish();
        }
    }

    @Override
    public void removeListModel(String itemId) {
        Observable.from(itemProvider.getItems())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .zipWith(Observable.range(0, itemProvider.getItemCount()), Indexed::new)
                .filter(indexed -> indexed.value.itemId().equals(itemId))
                .subscribe(indexed -> {
                    if (indexed.index < itemProvider.getItemCount()) {
                        itemProvider.removeItem(indexed.index);
                        getPresenter().updateListModels(itemProvider.getItems());
                    }
                    checkForEmptyMessage();
                }, this::onError);
    }

    @Override
    public void onClickItem(ListModel clickedModel) {
        for (int index = 0; index < itemProvider.getItemCount(); index++) {
            ListModel listModel = itemProvider.getItem(index);
            if (clickedModel.itemId().equals(listModel.itemId()) && !listModel.isClicked()) {
                listModel = listModel.toBuilder().clicked(true).build();
                itemProvider.setItem(index, listModel);
            } else if (!clickedModel.itemId().equals(listModel.itemId()) && listModel.isClicked()) {
                listModel = listModel.toBuilder().clicked(false).build();
                itemProvider.setItem(index, listModel);
            }
        }
        ((MainView) getActivity()).startDescription(this, clickedModel.itemId(), RC_DESCRIPTION_ITEM);
    }

    @Override
    public void onSelectItem(ListModel item) {
        Observable.from(itemProvider.getItems())
                .zipWith(Observable.range(0, itemProvider.getItemCount()), Indexed::new)
                .filter(indexed -> item.itemId().equals(indexed.value.itemId()))
                .first()
                .subscribe(indexed -> {
                    ListModel listModel = itemProvider.getItem(indexed.index);
                    listModel = listModel.toBuilder().selected(!listModel.isSelected()).build();
                    itemProvider.setItem(indexed.index, listModel);
                    getPresenter().checkForEditMode(itemProvider.getItems());
                }, this::onError);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_DESCRIPTION_ITEM && resultCode == RESULT_OK && data != null) {
            String itemId = data.getStringExtra(HomeDescriptionActivity.EXTRA_LIST_MODEL);
            getPresenter().updateModel(itemProvider.getItems(), itemId);
        } else if (requestCode == RC_DESCRIPTION_ITEM && resultCode == RESULT_ARCHIVED && data != null) {
            InfoDialog.showSnackbar(coordinatorLayout, getString(R.string.archive_list_already));

            String itemId = data.getStringExtra(HomeDescriptionActivity.EXTRA_LIST_MODEL);
            Observable.from(itemProvider.getItems())
                    .zipWith(Observable.range(0, itemProvider.getItemCount()), Indexed::new)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .filter(indexed -> itemId.equals(indexed.value.itemId()))
                    .subscribe(indexed -> {
                        itemProvider.removeItem(indexed.index);
                        getPresenter().archiveListModel(indexed.value);
                        checkForEmptyMessage();
                    });
        }
    }
}
