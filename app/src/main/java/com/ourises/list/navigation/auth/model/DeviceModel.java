package com.ourises.list.navigation.auth.model;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_DeviceModel.Builder.class)
public abstract class DeviceModel {
    @Nullable
    @JsonProperty("token")
    public abstract String token();

    @JsonProperty("type")
    public abstract Integer type();

    public static DeviceModel create(String token) {
        return new AutoValue_DeviceModel.Builder().token(token).type(1).build();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        @JsonProperty("token")
        public abstract Builder token(@Nullable String token);

        @JsonProperty("type")
        public abstract Builder type(Integer type);

        public abstract DeviceModel build();
    }
}
