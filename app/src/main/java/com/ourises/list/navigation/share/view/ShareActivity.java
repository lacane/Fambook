package com.ourises.list.navigation.share.view;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.ourises.list.R;

public class ShareActivity extends AppCompatActivity {
    public static final String SHARED_LIST_ID = "listId";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_activity);

        String itemId = getIntent().getStringExtra(SHARED_LIST_ID);

        ShareFragment shareFragment = new ShareFragmentBuilder(itemId).build();
        shareFragment.setOnHideListener((okResult) -> {
            if (okResult) {
                setResult(RESULT_OK);
            }
            finish();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
            }
        });
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.share_container, shareFragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

}
