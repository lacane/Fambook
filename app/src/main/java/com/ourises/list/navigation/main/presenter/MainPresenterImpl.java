package com.ourises.list.navigation.main.presenter;

import android.util.Log;

import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.ourises.list.common.entities.Code;
import com.ourises.list.common.model.NetworkModel;
import com.ourises.list.common.model.UserModel;
import com.ourises.list.navigation.main.view.MainView;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainPresenterImpl extends RxPresenter<MainView> implements MainPresenter {
    private final UserModel userModel;
    private final NetworkModel networkModel;

    private boolean offlineMode = false;

    public MainPresenterImpl(NetworkModel networkModel, UserModel userModel) {
        this.userModel = userModel;
        this.networkModel = networkModel;
    }

    @Override
    public void connect() {
        boolean connected = networkModel.checkForNetwork();
        if (connected) {
            getUser();
        } else {
            getView().connectedError();
        }

        Subscription dataSubscription = networkModel.isOfflineMode()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(offlineMode -> {
                    if (offlineMode) {
                        getView().offlineMode(true);
                    }
                });
        addSubscription(dataSubscription);

        Subscription offlineSubscription = networkModel.onOfflineModeSubscribe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                    if (aBoolean) {
                        goToOffline();
                    } else {
                        goToOnline();
                    }
                    Log.w("MainPresenterImpl", "onOfflineModeSubscribe " + aBoolean);
                });

        addSubscription(offlineSubscription);

        Subscription errorSubscription = networkModel.onErrorModelSubscribe()
                .subscribe(errorModel -> {
                    if (errorModel.code() == Code.USER_NOT_FOUND) {
                        Log.w("MainPresenterImpl", errorModel.message());
                        login();
                    }
                }, throwable -> {
                    if (throwable != null) throwable.printStackTrace();
                });

        addSubscription(errorSubscription);
    }

    @Override
    public void login() {
        Subscription dataSubscription = userModel.getTokenSession()
                .flatMap(userModel::login)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    userModel.setUser(user);
                    getView().setUser(user);
                }, throwable -> getView().onError(throwable));
        addSubscription(dataSubscription);
    }

    private void goToOnline() {
        if (offlineMode) {
            offlineMode = false;
            getView().offlineMode(false);

            login();
        }
    }

    private void goToOffline() {
        if (!offlineMode) {
            offlineMode = true;
            getView().offlineMode(true);
        }

    }

    @Override
    public void getUser() {
        Subscription dataSubscription = userModel.getUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    getView().setUser(user);
                }, throwable -> {
                    getView().onError(throwable);
                });

        addSubscription(dataSubscription);
    }
}
