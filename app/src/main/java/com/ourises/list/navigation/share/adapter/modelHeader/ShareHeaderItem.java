package com.ourises.list.navigation.share.adapter.modelHeader;

import com.google.auto.value.AutoValue;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.entities.User;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItem;

@AutoValue
public abstract class ShareHeaderItem implements ShareCommonItem {
    public static final int HEADER_TYPE = 0;

    @Override
    public int id() {
        return 0;
    }

    @Override
    public int type() {
        return HEADER_TYPE;
    }

    public abstract User creatorUser();

    public abstract String listName();

    public static ShareHeaderItem create(ListModel listModel) {
        return new AutoValue_ShareHeaderItem.Builder()
                .creatorUser(listModel.users().get(0))
                .listName(listModel.name())
                .build();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder creatorUser(User creatorUser);

        public abstract Builder listName(String listName);

        public abstract ShareHeaderItem build();
    }
}
