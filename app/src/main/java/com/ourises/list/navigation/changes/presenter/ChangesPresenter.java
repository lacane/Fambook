package com.ourises.list.navigation.changes.presenter;

import com.alapshin.arctor.presenter.Presenter;
import com.ourises.list.navigation.changes.view.ChangesView;

public interface ChangesPresenter extends Presenter<ChangesView> {
    void getListModel(String itemId);

    void getChanges(String itemId);

    void createCallback();
}
