package com.ourises.list.navigation.share.adapter.common;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.ourises.list.navigation.share.adapter.ShareAdapter;

import butterknife.ButterKnife;

public class ShareCommonItemView extends RelativeLayout {

    protected ShareAdapter.OnShareItemListener shareItemListener;

    public ShareCommonItemView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(getLayoutRes(), this);
        ButterKnife.bind(this);
    }

    public void setShareItemListener(ShareAdapter.OnShareItemListener shareItemListener) {
        this.shareItemListener = shareItemListener;
    }

    @LayoutRes
    protected int getLayoutRes() {
        return 0;
    }
}
