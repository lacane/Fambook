package com.ourises.list.navigation.homeDescription.adapter.modelAdd;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.alapshin.genericrecyclerview.ViewHolderDelegate;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonViewHolder;

public class HomeDescriptionAddDelegate
        implements ViewHolderDelegate<HomeDescriptionCommonItem, HomeDescriptionCommonViewHolder> {

    @Override
    public boolean isForViewType(@NonNull HomeDescriptionCommonItem item) {
        return item.type() == HomeDescriptionAddItem.ADD_MODEL_TYPE;
    }

    @NonNull
    @Override
    public HomeDescriptionCommonViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        HomeDescriptionAddItemView itemView = new HomeDescriptionAddItemView(viewGroup.getContext());
        return new HomeDescriptionAddViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeDescriptionCommonViewHolder holder, HomeDescriptionCommonItem item) {
        holder.onBindViewHolder(item);
    }

    @Override
    public Class<HomeDescriptionCommonViewHolder> getViewHolderType() {
        return HomeDescriptionCommonViewHolder.class;
    }
}
