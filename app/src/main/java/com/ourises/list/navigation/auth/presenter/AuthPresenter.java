package com.ourises.list.navigation.auth.presenter;

import com.alapshin.arctor.presenter.Presenter;
import com.ourises.list.navigation.auth.view.AuthView;

public interface AuthPresenter extends Presenter<AuthView> {
    void disconnect();

    void getUser();

    void checkForNetwork();

    void connect();
}
