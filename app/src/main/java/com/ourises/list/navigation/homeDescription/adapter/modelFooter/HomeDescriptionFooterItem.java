package com.ourises.list.navigation.homeDescription.adapter.modelFooter;

import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;

public class HomeDescriptionFooterItem implements HomeDescriptionCommonItem {
    public static final int FOOTER_MODEL_TYPE = 3;
    @Override
    public int id() {
        return 0;
    }

    @Override
    public int type() {
        return FOOTER_MODEL_TYPE;
    }
}
