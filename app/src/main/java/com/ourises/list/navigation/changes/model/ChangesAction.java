package com.ourises.list.navigation.changes.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ChangesAction {
    DELETED(1),
    ITEM_ADD(2),
    ITEM_DELETE(3),
    SHARE(4),
    ITEM_CHECKED(5),
    ITEM_UNCHECKED(6),
    ARCHIVED(7),
    UNARCHIVED(8),
    ITEM_UPDATE(9),
    LIST_UPDATE(10);

    private final int action;

    ChangesAction(int action) {
        this.action = action;
    }

    @JsonCreator
    public static ChangesAction fromId(int id) {
        for (ChangesAction templateId: ChangesAction.values()) {
            if (templateId.getAction() == id) {
                return templateId;
            }
        }
        return ITEM_UPDATE;
    }

    @JsonValue
    public int getAction() {
        return this.action;
    }
}
