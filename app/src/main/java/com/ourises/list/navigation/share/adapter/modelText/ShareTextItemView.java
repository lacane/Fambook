package com.ourises.list.navigation.share.adapter.modelText;

import android.content.Context;
import android.widget.TextView;

import com.ourises.list.R;
import com.ourises.list.navigation.share.adapter.common.ShareCommonItemView;

import butterknife.BindView;

public class ShareTextItemView extends ShareCommonItemView {

    @BindView(R.id.share_item_text_view) TextView textView;

    public ShareTextItemView(Context context) {
        super(context);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.share_item_text;
    }

    public void onBind(ShareTextItem textItem) {
        textView.setText(textItem.text());
    }
}
