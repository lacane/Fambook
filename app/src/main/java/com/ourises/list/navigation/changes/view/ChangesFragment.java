package com.ourises.list.navigation.changes.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.alapshin.genericrecyclerview.DefaultItemProvider;
import com.alapshin.genericrecyclerview.ItemProvider;
import com.alapshin.genericrecyclerview.ViewHolderDelegateManager;
import com.fambook.base.mvp.view.BaseMvpFragment;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.ourises.list.R;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.widget.util.DateUtil;
import com.ourises.list.di.HasComponent;
import com.ourises.list.di.components.ApplicationComponent;
import com.ourises.list.di.components.ChangesComponent;
import com.ourises.list.navigation.changes.adapter.ChangesAdapter;
import com.ourises.list.navigation.changes.adapter.ChangesItemDelegate;
import com.ourises.list.navigation.changes.adapter.ChangesViewHolder;
import com.ourises.list.navigation.changes.model.ChangesModel;
import com.ourises.list.navigation.changes.presenter.ChangesPresenter;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import butterknife.BindView;

@FragmentWithArgs
public class ChangesFragment extends BaseMvpFragment<ChangesView, ChangesPresenter> implements ChangesView {

    @Arg String itemId;

    @BindView(R.id.changes_recycler) RecyclerView recyclerView;

    private LinearLayoutManager layoutManager;
    private ChangesAdapter adapter = new ChangesAdapter();
    private ItemProvider<ChangesModel> itemProvider = new DefaultItemProvider<>();
    private ViewHolderDelegateManager<ChangesModel, ChangesViewHolder>
            delegateManager = new ViewHolderDelegateManager<>();

    private ChangesComponent component;

    @Override
    protected int getLayoutRes() {
        return R.layout.changes_fragment;
    }

    @Override
    protected void injectDependencies() {
        if (component == null) {
            component = ((HasComponent<ApplicationComponent>)
                    getActivity().getApplication()).getComponent().changesComponent();
        }

        component.inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        itemProvider.setAdapter(adapter);
        adapter.setItemProvider(itemProvider);
        adapter.setViewHolderDelegateManager(delegateManager);
        delegateManager.addDelegate(new ChangesItemDelegate());

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        getPresenter().createCallback();
        getPresenter().getListModel(itemId);
    }

    @Override
    public void onLoadedListModel(ListModel listModel) {
        List<ChangesModel> changesModels = listModel.changes();
        onLoadedChanges(changesModels);

        getPresenter().getChanges(listModel.itemId());
    }

    @Override
    public void onLoadedChanges(List<ChangesModel> changesModels) {
        if (changesModels != null && changesModels.size() > 1) {
            itemProvider.setItems(changesModels);
        }
    }

    @Override
    public void onError(Throwable throwable) {
        if (throwable != null) {
            throwable.printStackTrace();
        }
    }
}
