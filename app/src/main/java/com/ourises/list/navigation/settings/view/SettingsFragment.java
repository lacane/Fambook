package com.ourises.list.navigation.settings.view;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fambook.base.mvp.view.BaseMvpFragment;
import com.ourises.list.R;
import com.ourises.list.di.HasComponent;
import com.ourises.list.di.components.ApplicationComponent;
import com.ourises.list.di.components.SettingsComponent;
import com.ourises.list.navigation.auth.view.AuthActivity;
import com.ourises.list.navigation.main.model.ToolbarProvider;
import com.ourises.list.navigation.settings.presenter.SettingsPresenter;

import butterknife.BindView;

public class SettingsFragment extends BaseMvpFragment<SettingsView, SettingsPresenter>
        implements SettingsView {

    @BindView(R.id.settings_notifications) View notificationContainer;
    @BindView(R.id.settings_notifications_switch) SwitchCompat switchNotification;

    @BindView(R.id.settings_logout) View logoutContainer;

    @BindView(R.id.settings_contact_button) Button contactButton;
    @BindView(R.id.settings_version) TextView versionView;

    private SettingsComponent component;

    @Override
    protected int getLayoutRes() {
        return R.layout.settings_fragment;
    }

    @Override
    protected void injectDependencies() {
        if (component == null) {
            component = ((HasComponent<ApplicationComponent>) getActivity().getApplication())
                    .getComponent().settingsComponent();
        }

        component.inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((ToolbarProvider) getActivity()).getToolbar().setTitle(R.string.navigation_settings);

        notificationContainer.setOnClickListener(v -> {
            setNotificationState(!switchNotification.isChecked());
            getPresenter().updateNotificationState(switchNotification.isChecked());
        });

        logoutContainer.setOnClickListener(v -> {
            getPresenter().logout(getActivity());
        });

        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            String versionName = pInfo.versionName;
            String text = String.format(getString(R.string.version), versionName);
            versionView.setText(text);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        contactButton.setOnClickListener(v -> {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", getString(R.string.support_email), null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.feedback_support));
            startActivity(Intent.createChooser(emailIntent, getString(R.string.send_email)));
        });
    }

    @Override
    public void setNotificationState(boolean state) {
        switchNotification.setOnCheckedChangeListener(null);
        switchNotification.setChecked(state);
        switchNotification.setOnCheckedChangeListener((buttonView, isChecked) -> {
            getPresenter().updateNotificationState(isChecked);
        });
    }

    @Override
    public void onLogoutComplete() {
        startActivity(new Intent(getActivity(), AuthActivity.class));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out);
        }
        getActivity().finish();
    }
}
