package com.ourises.list.navigation.homeDescription.adapter.common;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.ourises.list.navigation.homeDescription.adapter.HomeDescriptionAdapter;

import butterknife.ButterKnife;

public class HomeDescriptionCommonItemView extends RelativeLayout {
    protected HomeDescriptionAdapter.OnChangeItemContentListener onChangeItemContentListener;

    public HomeDescriptionCommonItemView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(getLayoutRes(), this);
        ButterKnife.bind(this);
    }

    @LayoutRes
    protected int getLayoutRes() {
        return 0;
    }

    public void setOnChangeItemContentListener(HomeDescriptionAdapter.OnChangeItemContentListener onChangeItemContentListener) {
        this.onChangeItemContentListener = onChangeItemContentListener;
    }
}
