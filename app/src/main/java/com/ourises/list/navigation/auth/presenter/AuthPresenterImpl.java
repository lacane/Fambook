package com.ourises.list.navigation.auth.presenter;

import com.alapshin.arctor.presenter.PresenterBundle;
import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.ourises.list.common.model.NetworkModel;
import com.ourises.list.common.model.UserModel;
import com.ourises.list.navigation.auth.view.AuthView;

import javax.annotation.Nullable;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AuthPresenterImpl extends RxPresenter<AuthView> implements AuthPresenter  {
    private final UserModel userModel;
    private final NetworkModel networkModel;
    private boolean needCheckingNetwork = true;

    public AuthPresenterImpl(UserModel userModel, NetworkModel networkModel) {
        this.userModel = userModel;
        this.networkModel = networkModel;
    }

    @Override
    public void onCreate(@Nullable PresenterBundle bundle) {
        super.onCreate(bundle);
    }

    @Override
    public void disconnect() {
        clearSubscriptions();
        networkModel.disconnect();
    }

    @Override
    public void getUser() {
        Subscription dataSubscription = userModel.getTokenSession()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(deliverLatest())
                .filter(token -> {
                    if (token == null) {
                        getView().haveNotUser();
                    }
                    return token != null;
                })
                .subscribe(this::login);

        addSubscription(dataSubscription);
    }

    private void login(String token) {
        Subscription dataSubscription = networkModel.isOfflineMode()
                .flatMap(offlineMode -> {
                    if (!offlineMode)
                        return userModel.login(token);
                    else
                        return Observable.just(null);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    if (user != null) {
                        userModel.setUser(user);
                    }
                    getView().successLoadingUser();
                }, Throwable::printStackTrace);
        addSubscription(dataSubscription);
    }

    @Override
    public void checkForNetwork() {
        if (needCheckingNetwork) {
            clearSubscriptions();
            Subscription dataSubscription = networkModel.isOfflineMode()
                    .subscribe(offlineMode -> {
                        if (offlineMode) {
                            getUser();
                        } else {
                            disconnect();
                            getView().onErrorConnect();
                        }
                    });
            addSubscription(dataSubscription);
        }
    }

    @Override
    public void connect() {
        Subscription offlineSubscription = networkModel.onOfflineModeSubscribe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(offlineMode -> {
                    if (!offlineMode) {
                        needCheckingNetwork = false;
                        getUser();
                    }
                });
        addSubscription(offlineSubscription);

        networkModel.connect();
    }
}
