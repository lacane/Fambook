package com.ourises.list.navigation.homeDescription.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.ourises.list.R;
import com.ourises.list.common.widget.TintableImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShareButton extends FrameLayout {

    @BindView(R.id.home_users_share_button) TintableImageView button;

    public ShareButton(Context context) {
        super(context);
        LayoutInflater.from(getContext()).inflate(R.layout.home_users_share_button, this);
        ButterKnife.bind(this);
    }

    public void setOnClickListener(OnClickListener clickListener) {
        button.setOnClickListener(clickListener);
    }
}
