package com.ourises.list.navigation.homeDescription.adapter.modelTitle;

import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItemView;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonViewHolder;

public class HomeDescriptionTitleViewHolder extends HomeDescriptionCommonViewHolder {
    public HomeDescriptionTitleViewHolder(HomeDescriptionCommonItemView itemView) {
        super(itemView);
    }

    @Override
    public void onBindViewHolder(HomeDescriptionCommonItem item) {
        super.onBindViewHolder(item);

        HomeDescriptionTitleItem titleItem = (HomeDescriptionTitleItem) item;
        HomeDescriptionTitleItemView titleItemView = (HomeDescriptionTitleItemView) itemView;
        titleItemView.onBind(titleItem);
    }
}
