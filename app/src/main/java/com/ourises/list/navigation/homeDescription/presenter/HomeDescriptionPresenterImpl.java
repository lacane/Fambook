package com.ourises.list.navigation.homeDescription.presenter;

import android.support.v4.util.Pair;

import com.alapshin.arctor.presenter.PresenterBundle;
import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.ourises.list.common.entities.ListItemModel;
import com.ourises.list.common.entities.ListModel;
import com.ourises.list.common.entities.User;
import com.ourises.list.common.model.HomeListModel;
import com.ourises.list.common.widget.Indexed;
import com.ourises.list.navigation.homeDescription.adapter.modelFooter.HomeDescriptionFooterItem;
import com.ourises.list.navigation.homeDescription.adapter.modelTitle.HomeDescriptionTitleItem;
import com.ourises.list.navigation.homeDescription.model.UpdateAction;
import com.ourises.list.navigation.homeDescription.view.HomeDescriptionView;
import com.ourises.list.navigation.homeDescription.adapter.common.HomeDescriptionCommonItem;
import com.ourises.list.navigation.homeDescription.adapter.modelAdd.HomeDescriptionAddItem;
import com.ourises.list.navigation.homeDescription.adapter.modelUsers.HomeDescriptionUsersItem;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class HomeDescriptionPresenterImpl extends RxPresenter<HomeDescriptionView>
        implements HomeDescriptionPresenter {

    private final HomeListModel homeListModel;

    private ListModel currentListModel;

    @Override
    public void onCreate(@Nullable PresenterBundle bundle) {
        super.onCreate(bundle);

        Subscription updateSubscription = homeListModel.onUpdateModelSubscribe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(listModel -> listModel.itemId().equals(currentListModel.itemId()))
                .subscribe(listModel -> {
                    getView().checkForArchived(listModel.archived());
                    getView().onLoadedListModel(listModel.doSynchronize());
                    setListItems(listModel.getItems(), listModel.users(), listModel.name());
                }, throwable -> getView().onError(throwable));
        addSubscription(updateSubscription);

        Subscription deleteSubscription = homeListModel.onDeleteModelSubscribe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(modelId -> modelId.id().equals(currentListModel.itemId()))
                .subscribe(modelId -> getView().onDeleteListModel(), throwable -> getView().onError(throwable));
        addSubscription(deleteSubscription);

        Subscription synchronizedListModel = homeListModel.onSynchronizedListModelSubscribe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(synchronizedListModel1 -> {
                    if (synchronizedListModel1.oldModelId().equals(currentListModel.itemId())) {
                        getView().updateListModel(synchronizedListModel1.newModelId());
                    }
                }, throwable -> getView().onError(throwable));
        addSubscription(synchronizedListModel);

        Subscription updateActionModel = homeListModel.onUpdateActionSubscribe()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(updateAction -> updateAction.getItemId().equals(currentListModel.itemId()))
                .subscribe(updateAction -> {
                    if (updateAction.getActionType() == UpdateAction.ActionType.DELETED) {
                        getView().close(false);
                    } else if (updateAction.getActionType() == UpdateAction.ActionType.ARCHIVED_STATE) {
                        getView().close(false);
                    } else if (updateAction.getActionType() == UpdateAction.ActionType.UPDATE) {
                        getListModel(currentListModel.itemId());
                    }
                }, throwable -> getView().onError(throwable));
        addSubscription(updateActionModel);
    }

    public HomeDescriptionPresenterImpl(HomeListModel homeListModel) {
        this.homeListModel = homeListModel;
    }

    @Override
    public void getListModel(String itemId) {
        Subscription dataSubscription = homeListModel.getListModel(itemId)
                .compose(deliverLatest())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listModel -> {
                    this.currentListModel = listModel;
                    getView().onLoadedListModel(listModel);
                    setListItems(listModel.getItems(), listModel.users(), listModel.name());
                }, throwable -> getView().onError(throwable));

        addSubscription(dataSubscription);
    }

    private void setListItems(List<ListItemModel> items, List<User> users, String title) {
        Observable<List<ListItemModel>> uncheckedListObservable = Observable.from(items)
                .cast(ListItemModel.class)
                .filter(listItemModel -> listItemModel != null)
                .filter(listItemModel -> !listItemModel.status())
                .toList();

        Observable<List<ListItemModel>> checkedListObservable = Observable.from(items)
                .cast(ListItemModel.class)
                .filter(listItemModel -> listItemModel != null)
                .filter(ListItemModel::status)
                .toList();

        Subscription dataSubscription = Observable.zip(uncheckedListObservable, checkedListObservable,
                (listItemModels, listItemModels2) -> {
                    List<HomeDescriptionCommonItem> itemList = new ArrayList<HomeDescriptionCommonItem>();
                    itemList.add(HomeDescriptionUsersItem.create(users, currentListModel.createdUserId()));
                    itemList.add(HomeDescriptionTitleItem.create(title));
                    itemList.addAll(listItemModels);
                    itemList.add(new HomeDescriptionAddItem());
                    itemList.addAll(listItemModels2);
                    itemList.add(new HomeDescriptionFooterItem());
                    return itemList;
                }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(itemList -> getView().onLoadedListItems(itemList),
                        throwable -> getView().onError(throwable));

        addSubscription(dataSubscription);
    }

    @Override
    public void addListItemModel(List<HomeDescriptionCommonItem> items, String name) {
        Subscription dataSubscription = sortedItems(items, HomeDescriptionAddItem.ADD_MODEL_TYPE)
                .subscribe(indexed -> {
                    ListItemModel itemModel = ListItemModel.create(name, currentListModel.itemId());
                    getView().addItem(indexed.index, itemModel);

                    synchronizeItem(itemModel);
                });

        addSubscription(dataSubscription);
    }

    @Override
    public void synchronizeItem(ListItemModel itemModel) {
        Subscription dataSubscription = homeListModel.synchronizeListItemModel(itemModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(newItemModel -> {
                    newItemModel = newItemModel.doSynchronize();
                    getView().synchronizedItem(itemModel.itemId(), newItemModel);
                }, throwable -> {
                    getView().onError(throwable);
                });

        addSubscription(dataSubscription);
    }

    @Override
    public void checkItem(List<HomeDescriptionCommonItem> items, String itemId, boolean isChecked) {
        Observable<Integer> positionItemObservable = sortedItems(items, ListItemModel.LIST_MODEL_TYPE)
                .filter(indexed -> itemId.equals(((ListItemModel)indexed.value).itemId()))
                .map(indexed -> indexed.index);

        Observable<Indexed<HomeDescriptionCommonItem>> updateItemObservable =
                sortedItems(items, ListItemModel.LIST_MODEL_TYPE)
                        .filter(indexed -> itemId.equals(((ListItemModel)indexed.value).itemId()));

        Subscription dataSubscription = Observable.zip(positionItemObservable, updateItemObservable,
                (integer, indexed) -> {
                    indexed.setIndex(integer);
                    return indexed;
                })
                .subscribe(indexed -> {
                    ListItemModel newItem = ((ListItemModel) indexed.value).toBuilder().status(isChecked).build();
                    getView().updateCheckedItem(indexed.index, newItem, isChecked);
                }, throwable -> getView().onError(throwable));

        addSubscription(dataSubscription);
    }

    @Override
    public void checkStatusModel(List<HomeDescriptionCommonItem> items) {
        Observable<List<ListItemModel>> uncheckedListObservable = Observable.from(items)
                .filter(item -> item.type() == ListItemModel.LIST_MODEL_TYPE)
                .cast(ListItemModel.class)
                .filter(listItemModel -> listItemModel != null)
                .filter(listItemModel -> !listItemModel.status())
                .toList();

        Observable<List<ListItemModel>> checkedListObservable = Observable.from(items)
                .filter(item -> item.type() == ListItemModel.LIST_MODEL_TYPE)
                .cast(ListItemModel.class)
                .filter(listItemModel -> listItemModel != null)
                .filter(ListItemModel::status)
                .toList();

        Subscription dataSubscription = Observable.zip(uncheckedListObservable, checkedListObservable,
                (listItemModels, listItemModels2) -> new Pair<>(listItemModels.size(), listItemModels2.size()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pair -> {
                    getView().setStatusModel(pair.first, pair.second);
                }, throwable -> getView().onError(throwable));

        addSubscription(dataSubscription);
    }

    @Override
    public void changeArchiveState() {
        currentListModel = currentListModel.toBuilder().archived(!currentListModel.archived()).build();

        getView().onLoadedListModel(currentListModel);
        Subscription dataSubscription = homeListModel.updateListModel(currentListModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {}, throwable -> getView().onError(throwable));
        addSubscription(dataSubscription);
    }

    @Override
    public void checkForEditMode(List<HomeDescriptionCommonItem> items) {
        int selectedCount = 0;
        for (int index = 0; index < items.size(); index++) {
            if (items.get(index).type() == ListItemModel.LIST_MODEL_TYPE) {
                if (((ListItemModel)items.get(index)).isSelected()) {
                    selectedCount++;
                }
            }
        }

        getView().setEditMode(selectedCount > 0, selectedCount);
    }


    @Override
    public void updateItemsInStorage(List<HomeDescriptionCommonItem> items) {
        checkStatusModel(items);

        Subscription dataSubscription = Observable.from(items)
                .filter(item -> item.type() == ListItemModel.LIST_MODEL_TYPE)
                .cast(ListItemModel.class)
                .toList()
                .subscribe(listItemModels -> {
                    homeListModel.updateListItemsInStorage(currentListModel.itemId(), listItemModels);
                }, throwable -> getView().onError(throwable));

        addSubscription(dataSubscription);
    }

    @Override
    public void removeItemModel(HomeDescriptionCommonItem item) {
        Subscription dataSubscription = homeListModel.removeListItemModel((ListItemModel) item)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {}, throwable -> getView().onError(throwable));
        addSubscription(dataSubscription);
    }

    @Override
    public void updateItemModel(ListItemModel itemModel) {
        Subscription dataSubscription = homeListModel.updateListItemModel(itemModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {}, throwable -> getView().onError(throwable));
        addSubscription(dataSubscription);
    }

    @Override
    public void updateNameOfList(String nameOfList) {
        currentListModel = currentListModel.toBuilder().name(nameOfList).build();

        Subscription dataSubscription = homeListModel.updateListModel(currentListModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {}, throwable -> getView().onError(throwable));
        addSubscription(dataSubscription);
    }

    @Override
    public Observable<Indexed<HomeDescriptionCommonItem>> sortedItems(List<HomeDescriptionCommonItem> items, int type) {
        return Observable.from(items)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .zipWith(Observable.range(0, items.size()), Indexed::new)
                .filter(indexed -> indexed.value.type() == type);
    }
}
