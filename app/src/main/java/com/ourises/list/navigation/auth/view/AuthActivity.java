package com.ourises.list.navigation.auth.view;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.fambook.base.mvp.view.BaseMvpActivity;
import com.ourises.list.R;
import com.ourises.list.common.entities.User;
import com.ourises.list.common.service.SynchronizingService;
import com.ourises.list.di.HasComponent;
import com.ourises.list.di.components.ApplicationComponent;
import com.ourises.list.di.components.LoginComponent;
import com.ourises.list.navigation.auth.presenter.AuthPresenter;
import com.ourises.list.navigation.main.view.MainActivity;

import butterknife.BindView;

public class AuthActivity extends BaseMvpActivity<AuthView, AuthPresenter> implements AuthView {

    private final static int RC_AUTH_FACEBOOK = 1;

    @BindView(R.id.auth_facebook)
    Button facebookButton;
    @BindView(R.id.auth_root_view)
    ViewGroup rootView;

    private LoginComponent component;
    private boolean successfullyLoadedUser = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean isTablet = getResources().getBoolean(R.bool.isTablet);

        if (isTablet) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (SynchronizingService.class.getName().equals(service.service.getClassName())) {
                stopService(new Intent(this, SynchronizingService.class));
            }
        }

        facebookButton.setOnClickListener(this::authFacebook);
        facebookButton.setVisibility(View.INVISIBLE);

        facebookButton.post(() -> {
            getPresenter().connect();

            facebookButton.postDelayed(() -> getPresenter().checkForNetwork(), 2000);
        });
    }

    private void showView(View view, int delay) {
        view.postDelayed(() -> {
            view.setVisibility(View.VISIBLE);
            view.setAlpha(0.0f);
            view.animate().alpha(1.0f).setDuration(400).start();
        }, delay);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.auth_activity;
    }

    @Override
    protected void injectDependencies() {
        if (component == null)
            component = ((HasComponent<ApplicationComponent>) getApplication()).getComponent().loginComponent();
        component.inject(this);
    }

    private void authFacebook(View view) {
        startActivityForResult(new Intent(this, AuthFacebookActivity.class), RC_AUTH_FACEBOOK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
        }
    }

    @Override
    public void onError(Throwable throwable) {
        if (throwable != null) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void successLoadingUser() {
        if (!successfullyLoadedUser) {
            successfullyLoadedUser = true;
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
            }
            finish();
        }
    }

    @Override
    public void haveNotUser() {
        if (facebookButton.getVisibility() != View.VISIBLE) {
            showView(facebookButton, 1800);
        }
    }

    @Override
    public void goToOffline(User user) {
        successLoadingUser();
    }

    @Override
    public void onErrorConnect() {
        facebookButton.postDelayed(() -> getPresenter().connect(), 100);
    }

    @Override
    protected void onDestroy() {
        if (!successfullyLoadedUser) {
            getPresenter().disconnect();
        }
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_AUTH_FACEBOOK && resultCode == RESULT_OK) {
            facebookButton.setVisibility(View.GONE);
            getPresenter().getUser();
        }
    }
}
