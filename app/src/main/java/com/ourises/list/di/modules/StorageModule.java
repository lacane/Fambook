package com.ourises.list.di.modules;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ourises.list.common.model.StorageModel;
import com.ourises.list.common.model.StorageModelImpl;
import com.ourises.list.di.components.ApplicationComponent;
import com.ourises.list.di.scopes.ScopeIn;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {
    @Provides
    @ScopeIn(ApplicationComponent.class)
    static StorageModel provideStorageModel(ObjectMapper objectMapper) {
        return new StorageModelImpl(objectMapper);
    }
}
