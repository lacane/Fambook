package com.ourises.list.di.modules;

import com.ourises.list.common.model.UserModel;
import com.ourises.list.di.components.ProfileComponent;
import com.ourises.list.di.scopes.ScopeIn;
import com.ourises.list.navigation.profile.presenter.ProfilePresenter;
import com.ourises.list.navigation.profile.presenter.ProfilePresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ProfileModule {
    @Provides
    @ScopeIn(ProfileComponent.class)
    static ProfilePresenter provideProfilePresenter(UserModel userModel) {
        return new ProfilePresenterImpl(userModel);
    }
}
