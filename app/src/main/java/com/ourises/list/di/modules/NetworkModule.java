package com.ourises.list.di.modules;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ourises.list.common.model.NetworkModel;
import com.ourises.list.common.model.NetworkModelImpl;
import com.ourises.list.common.model.OkHttpModel;
import com.ourises.list.common.model.OkHttpModelImpl;
import com.ourises.list.common.model.StorageModel;
import com.ourises.list.common.model.socket.SocketModel;
import com.ourises.list.common.model.socket.SocketModelImpl;
import com.ourises.list.di.components.ApplicationComponent;
import com.ourises.list.di.scopes.ScopeIn;

import java.net.URI;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import io.socket.client.IO;
import io.socket.client.Socket;
import okhttp3.OkHttpClient;

@Module
public class NetworkModule {
    @Provides
    @ScopeIn(ApplicationComponent.class)
    static Socket provideSocket() {
        URI uri = URI.create("https://ourises.com/");
        return IO.socket(uri);
    }

    @Provides
    @ScopeIn(ApplicationComponent.class)
    static SocketModel provideSocketModel(Socket socket, ObjectMapper objectMapper) {
        return new SocketModelImpl(socket, objectMapper);
    }

    @Provides
    @ScopeIn(ApplicationComponent.class)
    static OkHttpModel provideOkHttpModel(OkHttpClient okHttpClient) {
        return new OkHttpModelImpl(okHttpClient);
    }

    @Provides
    @ScopeIn(ApplicationComponent.class)
    static OkHttpClient provideApiHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS);
        return builder.build();
    }

    @Provides
    @ScopeIn(ApplicationComponent.class)
    static NetworkModel provideNetworkModel(SocketModel socketModel, StorageModel storageModel) {
        return new NetworkModelImpl(socketModel, storageModel);
    }
}
