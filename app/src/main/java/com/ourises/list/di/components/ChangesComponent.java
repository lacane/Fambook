package com.ourises.list.di.components;

import com.ourises.list.di.modules.ChangesModule;
import com.ourises.list.di.scopes.ScopeIn;
import com.ourises.list.navigation.changes.view.ChangesFragment;

import dagger.Subcomponent;

@ScopeIn(ChangesComponent.class)
@Subcomponent(modules = ChangesModule.class)
public interface ChangesComponent {
    void inject(ChangesFragment fragment);
}
