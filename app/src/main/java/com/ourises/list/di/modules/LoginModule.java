package com.ourises.list.di.modules;

import com.ourises.list.common.model.NetworkModel;
import com.ourises.list.common.model.OkHttpModel;
import com.ourises.list.common.model.StorageModel;
import com.ourises.list.common.model.UserModel;
import com.ourises.list.di.components.LoginComponent;
import com.ourises.list.di.scopes.ScopeIn;
import com.ourises.list.navigation.auth.presenter.AuthFacebookPresenter;
import com.ourises.list.navigation.auth.presenter.AuthFacebookPresenterImpl;
import com.ourises.list.navigation.auth.presenter.AuthPresenter;
import com.ourises.list.navigation.auth.presenter.AuthPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class LoginModule {
    @Provides
    @ScopeIn(LoginComponent.class)
    static AuthPresenter provideAuthPresenter(UserModel userModel, NetworkModel networkModel) {
        return new AuthPresenterImpl(userModel, networkModel);
    }

    @Provides
    @ScopeIn(LoginComponent.class)
    static AuthFacebookPresenter provideAuthFacebookPresenter(StorageModel storageModel,
                                                              OkHttpModel okHttpModel) {
        return new AuthFacebookPresenterImpl(storageModel, okHttpModel);
    }
}
