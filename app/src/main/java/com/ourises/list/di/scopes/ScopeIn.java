package com.ourises.list.di.scopes;

import javax.inject.Scope;

@Scope
public @interface ScopeIn {
    Class<?> value();
}
