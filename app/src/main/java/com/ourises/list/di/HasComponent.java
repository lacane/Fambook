package com.ourises.list.di;

public interface HasComponent<C> {
    C getComponent();
}