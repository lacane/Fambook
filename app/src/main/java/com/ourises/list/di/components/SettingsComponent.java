package com.ourises.list.di.components;

import com.ourises.list.di.modules.SettingsModule;
import com.ourises.list.di.scopes.ScopeIn;
import com.ourises.list.navigation.settings.view.SettingsFragment;

import dagger.Subcomponent;

@ScopeIn(SettingsComponent.class)
@Subcomponent(modules = SettingsModule.class)
public interface SettingsComponent {
    void inject(SettingsFragment fragment);
}
