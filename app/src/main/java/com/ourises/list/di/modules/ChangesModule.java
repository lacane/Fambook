package com.ourises.list.di.modules;

import com.ourises.list.common.model.HomeListModel;
import com.ourises.list.di.components.ChangesComponent;
import com.ourises.list.di.scopes.ScopeIn;
import com.ourises.list.navigation.changes.presenter.ChangesPresenter;
import com.ourises.list.navigation.changes.presenter.ChangesPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ChangesModule {
    @Provides
    @ScopeIn(ChangesComponent.class)
    static ChangesPresenter provideChangesPresenter(HomeListModel homeListModel) {
        return new ChangesPresenterImpl(homeListModel);
    }
}
