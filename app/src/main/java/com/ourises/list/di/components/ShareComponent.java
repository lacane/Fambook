package com.ourises.list.di.components;

import com.ourises.list.di.modules.ShareModule;
import com.ourises.list.di.scopes.ScopeIn;
import com.ourises.list.navigation.share.view.ShareFragment;

import dagger.Subcomponent;

@ScopeIn(ShareComponent.class)
@Subcomponent(modules = ShareModule.class)
public interface ShareComponent {
    void inject(ShareFragment activity);
}
