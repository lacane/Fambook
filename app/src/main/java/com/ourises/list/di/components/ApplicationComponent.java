package com.ourises.list.di.components;

import com.ourises.list.application.FMApplication;
import com.ourises.list.application.FMApplicationImpl;
import com.ourises.list.common.service.SynchronizingService;
import com.ourises.list.di.modules.ApplicationModule;
import com.ourises.list.di.modules.JsonModule;
import com.ourises.list.di.modules.NetworkModule;
import com.ourises.list.di.modules.SettingsModule;
import com.ourises.list.di.modules.StorageModule;
import com.ourises.list.di.modules.UserModule;
import com.ourises.list.di.scopes.ScopeIn;

import dagger.Component;

@ScopeIn(ApplicationComponent.class)
@Component(modules = {
        ApplicationModule.class,
        NetworkModule.class,
        StorageModule.class,
        UserModule.class,
        JsonModule.class,
        SettingsModule.class
})
public interface ApplicationComponent {
    final class Builder {
        public static ApplicationComponent build(FMApplicationImpl app) {
            return DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(app))
                    .build();
        }

        private Builder() {
        }
    }

    ShareComponent shareComponent();
    LoginComponent loginComponent();
    HomeComponent homeComponent();
    SettingsComponent settingsComponent();
    ProfileComponent profileComponent();
    ChangesComponent changesComponent();

    void inject(FMApplication application);
    void inject(SynchronizingService service);
}
