package com.ourises.list.di.modules;


import com.ourises.list.common.model.HomeListModel;
import com.ourises.list.common.model.NetworkModel;
import com.ourises.list.common.model.UserModel;
import com.ourises.list.di.components.HomeComponent;
import com.ourises.list.di.scopes.ScopeIn;
import com.ourises.list.navigation.home.presenter.HomeHistoryPresenter;
import com.ourises.list.navigation.home.presenter.HomeHistoryPresenterImpl;
import com.ourises.list.navigation.home.presenter.HomePresenter;
import com.ourises.list.navigation.home.presenter.HomePresenterImpl;
import com.ourises.list.navigation.homeDescription.presenter.HomeDescriptionPresenter;
import com.ourises.list.navigation.homeDescription.presenter.HomeDescriptionPresenterImpl;
import com.ourises.list.navigation.main.presenter.MainPresenter;
import com.ourises.list.navigation.main.presenter.MainPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class HomeModule {
    @Provides
    @ScopeIn(HomeComponent.class)
    static HomePresenter provideHomePresenter(HomeListModel homeListModel, NetworkModel networkModel,
                                              UserModel userModel) {
        return new HomePresenterImpl(homeListModel, networkModel, userModel);
    }

    @Provides
    @ScopeIn(HomeComponent.class)
    static HomeHistoryPresenter provideHomeHistoryPresenter(HomeListModel homeListModel, NetworkModel networkModel,
                                                     UserModel userModel) {
        return new HomeHistoryPresenterImpl(homeListModel, networkModel, userModel);
    }

    @Provides
    @ScopeIn(HomeComponent.class)
    static MainPresenter provideMainPresenter(NetworkModel networkModel, UserModel userModel) {
        return new MainPresenterImpl(networkModel, userModel);
    }

    @Provides
    @ScopeIn(HomeComponent.class)
    static HomeDescriptionPresenter provideHomeItemDescriptionPresenter(HomeListModel homeListModel) {
        return new HomeDescriptionPresenterImpl(homeListModel);
    }
}
