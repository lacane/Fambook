package com.ourises.list.di.modules;

import com.ourises.list.common.model.HomeListModel;
import com.ourises.list.common.model.HomeListModelImpl;
import com.ourises.list.common.model.NetworkModel;
import com.ourises.list.common.model.StorageModel;
import com.ourises.list.common.model.UserModel;
import com.ourises.list.common.model.UserModelImpl;
import com.ourises.list.common.model.socket.SocketModel;
import com.ourises.list.di.components.ApplicationComponent;
import com.ourises.list.di.scopes.ScopeIn;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class UserModule {
    @Provides
    @ScopeIn(ApplicationComponent.class)
    static UserModel provideUserModel(NetworkModel networkModel, SocketModel socketModel, StorageModel storageModel) {
        return new UserModelImpl(networkModel, socketModel, storageModel);
    }

    @Provides
    @ScopeIn(ApplicationComponent.class)
    static HomeListModel provideHomeListModel(StorageModel storageModel, SocketModel socketModel,
                                              NetworkModel networkModel) {
        return new HomeListModelImpl(storageModel, socketModel, networkModel);
    }
}
