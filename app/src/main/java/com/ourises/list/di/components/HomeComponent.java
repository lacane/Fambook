package com.ourises.list.di.components;

import com.ourises.list.di.modules.HomeModule;
import com.ourises.list.di.scopes.ScopeIn;
import com.ourises.list.navigation.home.view.HomeFragment;
import com.ourises.list.navigation.home.view.HomeHistoryFragment;
import com.ourises.list.navigation.home.widget.ListItemView;
import com.ourises.list.navigation.homeDescription.view.HomeDescriptionActivity;
import com.ourises.list.navigation.homeDescription.view.HomeDescriptionFragment;
import com.ourises.list.navigation.main.view.MainActivity;

import dagger.Subcomponent;

@ScopeIn(HomeComponent.class)
@Subcomponent(modules = HomeModule.class)
public interface HomeComponent {
    void inject(HomeFragment fragment);

    void inject(HomeHistoryFragment fragment);

    void inject(MainActivity activity);

    void inject(ListItemView listItemView);

    void inject(HomeDescriptionActivity activity);
    void inject(HomeDescriptionFragment fragment);
}
