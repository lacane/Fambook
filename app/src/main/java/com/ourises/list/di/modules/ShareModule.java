package com.ourises.list.di.modules;

import com.ourises.list.common.model.HomeListModel;
import com.ourises.list.common.model.NetworkModel;
import com.ourises.list.common.model.UserModel;
import com.ourises.list.di.components.ShareComponent;
import com.ourises.list.di.scopes.ScopeIn;
import com.ourises.list.navigation.share.presenter.SharePresenter;
import com.ourises.list.navigation.share.presenter.SharePresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ShareModule {
    @Provides
    @ScopeIn(ShareComponent.class)
    static SharePresenter provideSharePresenter(UserModel userModel, HomeListModel homeListModel,
                                                NetworkModel networkModel) {
        return new SharePresenterImpl(userModel, homeListModel, networkModel);
    }
}
