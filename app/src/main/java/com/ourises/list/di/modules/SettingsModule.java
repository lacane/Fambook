package com.ourises.list.di.modules;

import com.ourises.list.common.model.SettingsModel;
import com.ourises.list.common.model.SettingsModelImpl;
import com.ourises.list.common.model.StorageModel;
import com.ourises.list.common.model.UserModel;
import com.ourises.list.di.components.SettingsComponent;
import com.ourises.list.di.scopes.ScopeIn;
import com.ourises.list.navigation.settings.presenter.SettingsPresenter;
import com.ourises.list.navigation.settings.presenter.SettingsPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SettingsModule {
    @Provides
    @ScopeIn(SettingsComponent.class)
    static SettingsModel provideSettingsModel(StorageModel storageModel, UserModel userModel) {
        return new SettingsModelImpl(storageModel, userModel);
    }

    @Provides
    @ScopeIn(SettingsComponent.class)
    static SettingsPresenter provideSettingsPresenter(SettingsModel settingsModel) {
        return new SettingsPresenterImpl(settingsModel);
    }
}
