package com.ourises.list.di.modules;


import android.app.Application;
import android.content.Context;

import com.ourises.list.di.components.ApplicationComponent;
import com.ourises.list.di.scopes.ScopeIn;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private final Application app;

    public ApplicationModule(Application app) {
        this.app = app;
    }

    @Provides
    @ScopeIn(ApplicationComponent.class)
    Application provideApplication() {
        return this.app;
    }

    @Provides
    @ScopeIn(ApplicationComponent.class)
    Context provideApplicationContext() {
        return this.app;
    }
}
