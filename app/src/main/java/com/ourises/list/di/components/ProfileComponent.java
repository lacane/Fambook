package com.ourises.list.di.components;

import com.ourises.list.di.modules.ProfileModule;
import com.ourises.list.di.scopes.ScopeIn;
import com.ourises.list.navigation.profile.view.ProfileActivity;

import dagger.Subcomponent;

@ScopeIn(ProfileComponent.class)
@Subcomponent(modules = ProfileModule.class)
public interface ProfileComponent {
    void inject(ProfileActivity activity);
}
