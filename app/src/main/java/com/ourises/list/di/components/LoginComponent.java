package com.ourises.list.di.components;

import com.ourises.list.di.modules.LoginModule;
import com.ourises.list.di.scopes.ScopeIn;
import com.ourises.list.navigation.auth.view.AuthActivity;
import com.ourises.list.navigation.auth.view.AuthFacebookActivity;

import dagger.Subcomponent;

@ScopeIn(LoginComponent.class)
@Subcomponent(modules = LoginModule.class)
public interface LoginComponent {
    void inject(AuthActivity activity);

    void inject(AuthFacebookActivity activity);
}
