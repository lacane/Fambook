package com.ourises.list.application;

import android.content.Context;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.ourises.list.common.model.NetworkModel;
import com.ourises.list.di.components.ApplicationComponent;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.NoEncryption;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;

public class FMApplicationImpl extends FMApplication {

    private ApplicationComponent component;
    static FMApplicationImpl instance;

    @Inject
    NetworkModel networkModel;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setupDagger();
        instance = this;
        Hawk.init(this)
                .setEncryption(new NoEncryption())
                .build();
        Fabric.with(this, new Crashlytics());
    }

    @Override
    protected void setupDagger() {
        if (component == null) {
            component = ApplicationComponent.Builder.build(this);
        }
        component.inject(this);
    }

    public static ApplicationComponent component() {
        return instance.getComponent();
    }

    @Override
    public ApplicationComponent getComponent() {
        return component;
    }
}
