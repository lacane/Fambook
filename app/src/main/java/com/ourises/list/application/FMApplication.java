package com.ourises.list.application;


import android.app.Application;

import com.ourises.list.di.HasComponent;
import com.ourises.list.di.components.ApplicationComponent;

public abstract class FMApplication extends Application
        implements HasComponent<ApplicationComponent> {
    protected abstract void setupDagger();
}
