package com.ourises.list.application;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.NoEncryption;
import com.ourises.list.R;
import com.ourises.list.navigation.auth.view.AuthActivity;

import static com.ourises.list.common.model.StorageModelImpl.SETTINGS_NOTIFICATIONS;

public class HYFcmListenerService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        boolean enableNotifications = isEnableNotifications();
        if (enableNotifications
                && remoteMessage.getData() != null
                && remoteMessage.getNotification() != null
                && remoteMessage.getNotification().getBody() != null
                && !remoteMessage.getNotification().getBody().equals("")
                && remoteMessage.getNotification().getTitle() != null
                && !remoteMessage.getNotification().getTitle().equals("")) {

            NotificationManager notificationManager =
                    (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancelAll();

            new Handler(Looper.getMainLooper()).postDelayed(() -> {
                sendNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
            }, 1000);

        }
    }

    private void sendNotification(String title, String body) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setTicker(body)
                .setContentTitle(title)
                .setContentText(body)
                .setSmallIcon(R.drawable.ic_stat_name);

        Intent resultIntent = new Intent(this, AuthActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(AuthActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);

        Notification notification = new NotificationCompat.BigTextStyle(mBuilder)
                .setBigContentTitle(title)
                .bigText(body).build();

        notification.defaults = Notification.DEFAULT_ALL;
        notification.flags = Notification.FLAG_AUTO_CANCEL;

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify("tag", 0, notification);
    }

    private boolean isEnableNotifications() {
        Hawk.init(this)
                .setEncryption(new NoEncryption())
                .build();

        return Hawk.get(SETTINGS_NOTIFICATIONS, true);
    }
}
