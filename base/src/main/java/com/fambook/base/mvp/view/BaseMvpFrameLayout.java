package com.fambook.base.mvp.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import com.alapshin.arctor.presenter.Presenter;
import com.alapshin.arctor.view.MvpFrameLayout;
import com.alapshin.arctor.view.MvpView;

import butterknife.ButterKnife;

public abstract class BaseMvpFrameLayout<V extends MvpView, P extends Presenter<V>> extends MvpFrameLayout<V, P> {
    public BaseMvpFrameLayout(Context context) {
        super(context);
        init(context);
    }

    public BaseMvpFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BaseMvpFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BaseMvpFrameLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        injectDependencies();
        LayoutInflater.from(context).inflate(getLayoutRes(), this);
        ButterKnife.bind(this);
    }

    @LayoutRes
    protected abstract int getLayoutRes();
    protected abstract void injectDependencies();
}
