package com.fambook.base.mvp.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDelegate;

import com.alapshin.arctor.presenter.Presenter;
import com.alapshin.arctor.view.MvpActivity;
import com.alapshin.arctor.view.MvpView;
import com.fambook.base.di.ComponentCache;
import com.fambook.base.di.ComponentCacheDelegate;
import com.trello.rxlifecycle.LifecycleProvider;
import com.trello.rxlifecycle.LifecycleTransformer;
import com.trello.rxlifecycle.RxLifecycle;
import com.trello.rxlifecycle.android.ActivityEvent;

import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseMvpActivity<V extends MvpView, P extends Presenter<V>>
        extends MvpActivity<V, P> implements LifecycleProvider<ActivityEvent>, ComponentCache {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private final ComponentCacheDelegate componentCacheDelegate = new ComponentCacheDelegate();
    private final BehaviorSubject<ActivityEvent> lifecycleSubject = BehaviorSubject.create();

    @Override
    public final Observable<ActivityEvent> lifecycle() {
        return lifecycleSubject.asObservable();
    }

    @NonNull
    @Override
    public final <T> LifecycleTransformer<T> bindToLifecycle() {
        return RxLifecycle.bind(lifecycleSubject);
    }

    @Override
    public final <T> LifecycleTransformer<T> bindUntilEvent(@NonNull ActivityEvent event) {
        return RxLifecycle.bindUntilEvent(lifecycleSubject, event);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    @CallSuper
    protected void onCreate(Bundle savedInstanceState) {
        injectDependencies();
        // {@link ComponentCacheDelegate#onCreate} should be called before {@link Activity#onCreate}
        componentCacheDelegate.onCreate(savedInstanceState, getLastCustomNonConfigurationInstance());
        super.onCreate(savedInstanceState);

        setContentView(getLayoutRes());
        lifecycleSubject.onNext(ActivityEvent.CREATE);
    }

    @Override
    @CallSuper
    protected void onStart() {
        super.onStart();
        lifecycleSubject.onNext(ActivityEvent.START);
    }

    @Override
    @CallSuper
    protected void onResume() {
        super.onResume();
        lifecycleSubject.onNext(ActivityEvent.RESUME);
    }

    @Override
    @CallSuper
    protected void onPause() {
        lifecycleSubject.onNext(ActivityEvent.PAUSE);
        super.onPause();
    }

    @Override
    @CallSuper
    protected void onStop() {
        lifecycleSubject.onNext(ActivityEvent.STOP);
        super.onStop();
    }

    @Override
    @CallSuper
    protected void onDestroy() {
        lifecycleSubject.onNext(ActivityEvent.DESTROY);
        super.onDestroy();
    }

    @Override
    @CallSuper
    public void onContentChanged() {
        // {@link ButterKnife#bind} should be called before {@link Activity#onContentChanged}
        ButterKnife.bind(this);
        super.onContentChanged();
    }

    @Override
    @CallSuper
    public Object onRetainCustomNonConfigurationInstance() {
        return componentCacheDelegate.onRetainCustomNonConfigurationInstance();
    }

    @Override
    public long generateComponentId() {
        return componentCacheDelegate.generateId();
    }

    @Override
    public <C> C getComponent(long index) {
        return componentCacheDelegate.getComponent(index);
    }

    @Override
    public <C> void setComponent(long index, C component) {
        componentCacheDelegate.setComponent(0, component);
    }

    @LayoutRes
    protected abstract int getLayoutRes();
    protected abstract void injectDependencies();
}
