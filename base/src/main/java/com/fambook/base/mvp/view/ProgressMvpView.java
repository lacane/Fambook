package com.fambook.base.mvp.view;


import com.alapshin.arctor.view.MvpView;

public interface ProgressMvpView extends MvpView {
    void onProgress();
}
