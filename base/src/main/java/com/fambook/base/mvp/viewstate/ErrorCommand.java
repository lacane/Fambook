package com.fambook.base.mvp.viewstate;


import android.support.annotation.NonNull;

import com.alapshin.arctor.viewstate.ViewStateCommand;
import com.fambook.base.mvp.view.ErrorMvpView;
import com.google.auto.value.AutoValue;

/**
 * {@link ViewStateCommand} implementation to show error message
 * @param <V> view type
 */
@AutoValue
public abstract class ErrorCommand<V extends ErrorMvpView> implements ViewStateCommand<V> {
    public static final int COMMAND_TYPE = 1;

    @Override
    public int type() {
        return COMMAND_TYPE;
    }

    public abstract Throwable error();

    @Override
    public void execute(V v) {
        v.onError(error());
    }

    @NonNull
    public static <V extends ErrorMvpView> ErrorCommand<V> create(Throwable error) {
        return new AutoValue_ErrorCommand<V>(error);
    }
}
